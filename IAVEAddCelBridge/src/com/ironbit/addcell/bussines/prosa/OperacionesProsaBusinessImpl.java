/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ironbit.addcell.bussines.prosa;

import com.addcel.prosa.ProsaResponse;
import com.addcel.utils.ConstantesProsa;
import com.addcel.utils.U;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author efren
 */
public class OperacionesProsaBusinessImpl implements OperacionesProsaBusiness {
    private static final Logger logger = LoggerFactory.getLogger(OperacionesProsaBusinessImpl.class);
    private Gson gson = new Gson();

    @Override
    public ProsaResponse authorize(String afiliacion, String card, String vigencia, String cvv2, String monto, String nombre, String moneda) {
       
        ProsaResponse respuesta = new ProsaResponse();
        try {
            String data = "card=" + URLEncoder.encode(card, "UTF-8")
                    + "&vigencia=" + URLEncoder.encode(vigencia, "UTF-8")
                    + "&cvv2=" + URLEncoder.encode(cvv2, "UTF-8")
                    + "&monto=" + URLEncoder.encode(monto, "UTF-8")
                    + "&nombre=" + URLEncoder.encode(nombre, "UTF-8")
                    + "&afiliacion=" + URLEncoder.encode(afiliacion, "UTF-8")
                    + "&moneda=" + URLEncoder.encode(moneda, "UTF-8");
            
            //Envío de datos
            URL url = new URL(ConstantesProsa.URL_AUTH_PROSA);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr;
            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            logger.debug("Solicitando autorización PROSA...");
            logger.debug("data = " + data);

            // Recepción de respuesta
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            
            while ((line = rd.readLine()) != null) {
                logger.debug("Respuesta PROSA: " + line);
                respuesta = gson.fromJson(line, ProsaResponse.class);
            }
            wr.close();
            rd.close();
        } catch (UnsupportedEncodingException e) {
            logger.error("Ocurrio un error: {}", e);
        } catch (MalformedURLException e) {
            logger.error("Ocurrio un error: {}", e);
        } catch (IOException e) {
            logger.error("Ocurrio un error: {}", e);
        }
        
        return respuesta;
    }

    @Override
    public ProsaResponse devolution(String transactionId) {
        
        ProsaResponse respuesta = new ProsaResponse();
        try {
            String data = "transactionIdn=" + URLEncoder.encode(transactionId, "UTF-8");
            
            //Envío de datos
            
            URL url = new URL(ConstantesProsa.URL_REVERSO_PROSA);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            logger.debug("Solicitando reverso PROSA...");
            logger.debug(data);

            // Recepción de respuesta
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
	        respuesta = gson.fromJson(line, ProsaResponse.class);
            }
            wr.close();
            rd.close();
        } catch (UnsupportedEncodingException e) {
            logger.error("Ocurrio un error: {}", e);
        } catch (MalformedURLException e) {
            logger.error("Ocurrio un error: {}", e);
        } catch (IOException e) {
            logger.error("Ocurrio un error: {}", e);
        }
        
        return respuesta;
    }

    @Override
    public ProsaResponse authorizeByUserId(String idUsuario, String pass, String afiliacion, String cvv2, String monto, String propina) {
        U log = new U();
        ProsaResponse respuesta = new ProsaResponse();
        try {
            String data = "user=" + URLEncoder.encode(idUsuario, "UTF-8")
                    + "&afiliacion=" + URLEncoder.encode(afiliacion, "UTF-8")
                    + "&lada=" + URLEncoder.encode(cvv2, "UTF-8")
                    + "&monto=" + URLEncoder.encode(monto + propina, "UTF-8");
            
            //Envío de datos
            logger.debug(data);
            URL url = new URL(ConstantesProsa.URL_AUTH_PROSA_BYUSR);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr;
            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();
            logger.debug("Solicitando autorización PROSA por usuario " + idUsuario);
            logger.debug(data);

            // Recepción de respuesta
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
	        respuesta = gson.fromJson(line, ProsaResponse.class);
            }
            wr.close();
            rd.close();
        } catch (UnsupportedEncodingException e) {
            logger.error("Ocurrio un error: {}", e);
        } catch (MalformedURLException e) {
            logger.error("Ocurrio un error: {}", e);
        } catch (IOException e) {
            logger.error("Ocurrio un error: {}", e);
        }
        return respuesta;
    }
    
}
