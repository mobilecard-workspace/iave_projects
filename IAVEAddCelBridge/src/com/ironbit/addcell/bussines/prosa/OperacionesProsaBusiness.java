/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ironbit.addcell.bussines.prosa;

import com.addcel.prosa.ProsaResponse;

/**
 *
 * @author efren
 */
public interface OperacionesProsaBusiness {

    /**
     * M&eacute;todo que solicita la autorizaci&oacute;n de un cobro a traves de PROSA
     * @param afiliacion n&uacute;mero de afiliaci&oacute;n de la cuenta a la que se realizar&aacute; el pago
     * @param card 16 digitos de la tarjeta
     * @param vigencia Vigencia de la tarjeta
     * @param cvv2 n&uacutemero de seguridad de la tarjeta
     * @param monto monto a pagar
     * @param nombre Titular de la tarjeta
     * @param moneda moneda en la que se realizar&aacute; la transacci&oacute;n, si se manda cadena vacia
     * el servicio de PROSA interpreta que la transacci&oacute;n se realiza en pesos mexicanos
     * @return Respuesta de PROSA a la solicitud del cobro
     */
    ProsaResponse authorize(String afiliacion, String card, String vigencia,
            String cvv2, String monto, String nombre, String moneda);

    /**
     * M&eacute;todo que solicita la autorizaci&oacute;n de un cobro a traves de PROSA usando las credenciales de un usuario registrado en MobileCard
     * @param idUsuario identificador del usuario
     * @param pass password del usuario
     * @param afiliacion n&uacute;mero de afiliaci&oacute;n de la cuenta a la que se realizar&aacute; el pago
     * @param cvv2 n&uacutemero de seguridad de la tarjeta
     * @param monto monto a pagar
     * @param propina propina
     * @return Respuesta emitida por prosa
     */
    ProsaResponse authorizeByUserId(String idUsuario, String pass, String afiliacion, 
            String cvv2, String monto, String propina);
    
    /**
     * M&eacute;todo que solicita el reverso de un cargo realizado a la tarjeta de un cliente
     * @param transactionId el n&uacute;mero de transacci&oacute; con el que se realiz&oacute; la operaci&oacue;n que se desea reversar
     * @return Respuesta emitida por prosa
     */
    ProsaResponse devolution(String transactionId);
}
