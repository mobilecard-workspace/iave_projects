/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ironbit.addcell.bussines.emida;

import com.ironbit.addcell.bean.Bitacora;
import com.ironbit.addcell.bean.Compra;
import com.ironbit.addcell.bean.Producto;
import com.ironbit.addcell.bean.RespuestaVentaTAE;

/**
 * Interfaz que define las operaciones sobre los servicios que ofrece el proveedor EMIDA
 * @author Ing. Efr&eacute;n Reyes Torres
 */
public interface OperacionesEmidaBusiness {
    
    /**
     * M&eacute;todo que solicita una compra de tiempo aire de los carriers que soporta EMIDA
     * @param bitacora Bit&aacute;cora asociada a la transacción
     * @param response Respuesta de la transacci&oacute;n con PROSA
     * @param compra Datos de la compra de tiempo aire
     * @param Cargo Monto a comprar
     * @param Comision Comisi&oacute;n por la transacci&oacute;n
     * @param usuario Usuario de mobile card que realiza la compra
     * @param CodeError 
     * @param DescError
     * @param Resbanco
     * @param estatus
     * @return 
     */
    public RespuestaVentaTAE realizarCompraTAEmida(Compra compra, Bitacora bitacora, Producto producto);
}
