/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ironbit.addcell.bussines.emida;

import com.addcel.utils.ConstantesEmida;
import com.addcel.utils.U;
import com.google.gson.Gson;
import com.ironbit.addcell.bean.Bitacora;
import com.ironbit.addcell.bean.Compra;
import com.ironbit.addcell.bean.Producto;
import com.ironbit.addcell.bean.RespuestaVentaTAE;
import com.ironbit.addcell.emida.entity.CompraRespuesta;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
//import com.ironbit.addcell.emida.bussines.VentaTiempoAireEmida;
//import com.ironbit.addcell.emida.entity.CompraRespuesta;

/**
 * Clase que implementa las operaciones sobre los servicios que ofrece el proveedor EMIDA
 * @author Ing. Efr&eacute;n Reyes Torres
 */
public class OperacionesEmidaBusinessImpl implements OperacionesEmidaBusiness {

//    VentaTiempoAireEmida serviciosEmida = new VentaTiempoAireEmida();
    @Override
    public RespuestaVentaTAE realizarCompraTAEmida(Compra compra, Bitacora bitacora, Producto producto) {
        RespuestaVentaTAE respuesta = new RespuestaVentaTAE();
        CompraRespuesta respuestaEmida = new CompraRespuesta();
        U log = new U();
        log.Display("realizarCompraTAEmida");

        try {
            String data = "tel=" + URLEncoder.encode(compra.getTelefono(), "UTF-8")
                    + "&prod=" + URLEncoder.encode(compra.getProducto(), "UTF-8")
                    + "&monto=" + URLEncoder.encode(String.valueOf(producto.getMonto()), "UTF-8")
                    + "&site=" + URLEncoder.encode(ConstantesEmida.SITE_ID, "UTF-8")
                    + "&clerk=" + URLEncoder.encode(ConstantesEmida.CLERK_CODE, "UTF-8")
                    + "&invoice=" + URLEncoder.encode(String.valueOf(bitacora.getId()), "UTF-8");

            System.out.println(ConstantesEmida.URL_TAE);
            
            
            System.out.println(data);
            //Envío de datos
            URL url = new URL(ConstantesEmida.URL_TAE);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(data);
            wr.flush();

            // Recepción de respuesta
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            Gson gson = new Gson();
            while ((line = rd.readLine()) != null) {
                respuestaEmida  = new CompraRespuesta();
	        respuestaEmida = gson.fromJson(line, CompraRespuesta.class);
                log.Display("Respuesta WS Emida" + line);
            }
            wr.close();
            rd.close();
            
            respuesta.setAmount(producto.getMonto());
            respuesta.setResponsetelcel(respuestaEmida.getResponseMessage());
            respuesta.setReferenceid(respuestaEmida.getInvoiceNo());
            respuesta.setCode(respuestaEmida.getResponseCode());
            respuesta.setPhone(compra.getTelefono());
            respuesta.setTerminal(ConstantesEmida.SITE_ID);
            respuesta.setResponsetime(respuestaEmida.getTransactionDateTime());
            respuesta.setError("");
            respuesta.setNointentos("");
            respuesta.setCompany("");
            respuesta.setStore("");
            respuesta.setRegion("");
            respuesta.setTransactionno(respuestaEmida.getInvoiceNo());
            
            //DATOS PARA BITACORA
            respuesta.setTelcelid("NA");
            respuesta.setTransactionno(respuestaEmida.getInvoiceNo());
            
            if (respuestaEmida.getResponseCode().equals("00")) {
                respuesta.setCode("5");
                respuesta.setStatus("5");
            } else {
                respuesta.setCode("-1");
                respuesta.setStatus("-1");
                respuesta.setError("ERRORTAE");
            }
            
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return respuesta;

    }
}
