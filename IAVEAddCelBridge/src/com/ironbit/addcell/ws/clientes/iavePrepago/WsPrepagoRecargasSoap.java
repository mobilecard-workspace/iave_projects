/**
 * WsPrepagoRecargasSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.ironbit.addcell.ws.clientes.iavePrepago;

public interface WsPrepagoRecargasSoap extends java.rmi.Remote {
    public java.lang.String reload(java.lang.String sXML) throws java.rmi.RemoteException;
    public java.lang.String reverseReload(java.lang.String sXML) throws java.rmi.RemoteException;
}
