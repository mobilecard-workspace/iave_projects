package com.ironbit.addcell.integration;

import com.addcel.mx.antad.servicios.model.vo.OzekimessageoutVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioTagsVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;

import org.springframework.beans.factory.annotation.Autowired;

import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.ironbit.addcell.bean.Usuario;

import java.net.URLEncoder;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author - JCDP
 */
public class UsuarioDAO {
    
    private static final Logger logger = LoggerFactory.getLogger(UsuarioDAO.class);
    
    @Autowired
	private AntadServiciosMapper mapper;
    
    public void UsuarioDAO() {
    }

    public String random() {
        Random rand = new Random();
        int x = rand.nextInt(99999999);
        return "" + x;
    }
    
    /*
     * Transformacion del metodo getUsuario con la nueva arquitectura de mybatis
     * Autor : JCDP
     * Fecha : 18/07/2016
     * 
     * */
    public UsuarioVO getUsuario(UsuarioVO usuario) throws Exception {
        UsuarioVO userRep = null;
        String wPwd = "";
        try {
        	userRep = mapper.getUsuario(usuario.getUsr_login());

            logger.debug("NUEVA CADENA USUARIO ESTATUS 1: " + userRep.getId_usr_status());
            if (userRep != null && userRep.getId_usr_status() == 99) {
                wPwd = usuario.getPasswordS();
            } else {
                wPwd = usuario.getUsr_pwd();
            }
            
            if(userRep != null && userRep.getUsr_pwd() != null && userRep.getUsr_pwd().equals(wPwd)){
                usuario = userRep;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo metodo getUsuario(): {}",ex);
        } 
        return usuario;
    }
    /*
     * Transformacion del metodo existeUsuario con la nueva arquitectura de mybatis
     * Autor : JCDP
     * Fecha : 18/07/2016
     * 
     * */
    public boolean existeUsuario(UsuarioVO usuario) throws Exception {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
        	user = mapper.getUsuario(usuario.getUsr_login());
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo metodo existeUsuario() en clase UsuarioDAO: {}",ex);
        } finally {
            //closeConPrepStResSet(statement, resultSet);
        }
        return flag;
    }
    
    
    /**
	 * Nueva clase newValidateUsuario que sustituye a la clase mostrada arriba llamada validateUsuario. 
	 * se construye con la nueva arquitectura de persistencia mybatis.
	 * @author: JCDP
	 * @version: 12/07/2016
	 */
	public boolean newValidateUsuario(UsuarioVO usuario) throws ClassNotFoundException, SQLException {
        boolean flag = false;
        UsuarioVO vusuario = null;
        try {
            vusuario = mapper.validaUsuario(usuario.getUsr_login(), usuario.getUsr_pwd());
            if (vusuario != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en método newValidateUsuario(UsuarioVO) : {}",ex);
        } finally {
            //closeConPrepStResSet(statement, resultSet);
        }
        return flag;
    }
	
	/*
	 * metodo para actualizar password de usuario 
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	//public boolean updatePassword(long usuario, String password, String sMail, String u, String p) throws SQLException, ClassNotFoundException {
	public boolean updatePassword(UsuarioVO usuario) throws Exception {
        boolean flag = false;
        PreparedStatement statement = null;
        try {
        	mapper.updateUsuarios(usuario);
        	flag = true;
            new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_RECOVERYPWD", "@ASUNTO_RECOVERYPWD", usuario.getUsr_login(), usuario.getUsr_pwd(), "", "", "", "");
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo metodo updatePassword() : {}",ex);
        } 
        return flag;
    }
	/*
	 * metodo para que valida si existe un usuario con un mail existente 
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	public boolean ExisteMail(String eMail) throws Exception {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.getUsuarioByEmail(eMail);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo método existeMail () dentro de clase UsuarioDAO: {}",ex);
        } finally {
            //closeConPrepStResSet(statement, resultSet);
        }
        return flag;
    }
	/*
	 * metodo para que valida si existe un usuario con un mail y un usr_login existente 
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean ExisteMailUp(String eMail, String idusuario) throws Exception {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.existeMailUp(eMail, idusuario);
            if ( user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en metodo ExisteMailUp clase UsuarioDAO: {}",ex);
        }
        return flag;
    }
	/*
	 * metodo para que valida si existe un usuario con un numero de tarjeta y un usr_login existente 
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean ExisteTUp(String T, String idusuario) throws Exception {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
        	user = mapper.existeTUp(T, idusuario);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error: {}",ex);
        } 
        return flag;
    }
	/*
	 * metodo para que valida si existe un usuario con un numero de tarjeta existente 
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	public boolean ExisteT(String T) throws Exception {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.getUsuarioByT(T);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo metodo ExisteT en clase UsuarioDAO: {}",ex);
        } finally {
            //closeConPrepStResSet(statement, resultSet);
        }
        return flag;
    }
	/*
	 * metodo para que valida si existe un usuario con un imei existente
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	public boolean ExisteIMEI(String imei) throws ClassNotFoundException, SQLException {
		boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.getUsuarioByImei(imei);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo metodo ExisteIMEI en clase UsuarioDAO: {}",ex);
        } finally {
            //closeConPrepStResSet(statement, resultSet);
        }
        return flag;
    }
	/*
	 * metodo para que valida si existe un usuario con telefono existente
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	public boolean ExisteTelefono(String telefono) throws Exception {
		boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.getUsuarioByTelefono(telefono);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            logger.error("Ocurrio un error en nuevo metodo ExisteTelefono en clase UsuarioDAO: {}",ex);
        } finally {
            //closeConPrepStResSet(statement, resultSet);
        }
        return flag;
    }
	/*
	 * metodo para agregar un nuevo usuario
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	 public boolean add(UsuarioVO usuario, String p) throws ClassNotFoundException, SQLException {
	        PreparedStatement statement = null;
	        boolean flag = false;
	        try {
	            String wPwd = "99" + random();
	         
	            usuario.setId_usuario(new java.util.Date().getTime());
	            usuario.setUsr_login(usuario.getUsr_login());
	            if(usuario.getId_usr_status() != 1000){
	                usuario.setUsr_pwd(crypto.Crypto.sha1(wPwd));
	            }else{
	                String kk= ParametroDAO.parsePass(wPwd);
	                usuario.setUsr_pwd(crypto.Crypto.aesEncrypt(kk,  wPwd ));
	            }
	            
	            usuario.setUsr_telefono(usuario.getUsr_telefono());
	            usuario.setUsr_nombre(usuario.getUsr_nombre());
	            usuario.setUsr_direccion(usuario.getUsr_direccion());
	            usuario.setUsr_tdc_numero(usuario.getUsr_tdc_numero());
	            usuario.setUsr_tdc_vigencia(usuario.getUsr_tdc_vigencia());
	            usuario.setId_banco(usuario.getId_banco());
	            usuario.setId_tipo_tarjeta(usuario.getId_tipo_tarjeta());
	            usuario.setId_proveedor(usuario.getId_proveedor());
	            if (usuario.getId_usr_status() != 1000) { 
	                usuario.setId_usr_status(99);
	            } else {
	            	usuario.setId_usr_status(1); //Deja activado el usuario con los datos por default
	            }
	            if(usuario.getUsr_fecha_nac() != null && !"".equals(usuario.getUsr_fecha_nac())){
	            	usuario.setUsr_fecha_nac((Date.valueOf(""+usuario.getUsr_fecha_nac())));
	            }else{
	            	usuario.setUsr_fecha_nac(null);
	            }
	            
	            usuario.setUsr_apellido(usuario.getUsr_apellido());
	            usuario.seteMail(usuario.geteMail());
	            usuario.setImei(usuario.getImei());
	            usuario.setTipo(usuario.getTipo());
	            usuario.setSoftware(usuario.getSoftware());
	            usuario.setModelo(usuario.getModelo());
	            usuario.setWkey(usuario.getWkey());
	            usuario.setTelefono_original(usuario.getTelefono_original());
	            usuario.setUsr_materno(usuario.getUsr_materno());
	            usuario.setUsr_sexo(usuario.getUsr_sexo());
	            usuario.setUsr_tel_casa(usuario.getUsr_tel_casa());
	            usuario.setUsr_tel_oficina(usuario.getUsr_tel_oficina());
	            usuario.setUsr_id_estado(usuario.getUsr_id_estado());
	            usuario.setUsr_ciudad(usuario.getUsr_ciudad());
	            usuario.setUsr_calle(usuario.getUsr_calle());
	            usuario.setUsr_num_ext(usuario.getUsr_num_ext());
	            usuario.setUsr_num_interior(usuario.getUsr_num_interior());
	            usuario.setUsr_colonia(usuario.getUsr_colonia());
	            usuario.setUsr_cp(usuario.getUsr_cp());
	            usuario.setUsr_dom_amex(usuario.getUsr_dom_amex());
	            usuario.setUsr_terminos(usuario.getUsr_terminos());

	            mapper.insertUsuarios(usuario);
	   
	            String mensaje = null;
	            try{
	                if(usuario.getTipo_cliente() !=16){
	                    mensaje = URLEncoder.encode("Hola, bienvenido a MobileCard " + usuario.getUsr_nombre() + " tu usuario es: " + usuario.getUsr_login() + "  tu contraseña es: " + wPwd,"UTF-8");
	                } else {
	                    mensaje = URLEncoder.encode("Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contrasena son:\n Usuario: " + usuario.getUsr_login() + "\n Contrasena: " + wPwd,"UTF-8");
	                }                    
	            }catch(Exception e){
	                logger.debug("ERROR - generar cadena de SMS: " + e.getMessage());
	                //e.printStackTrace();
	                if(usuario.getTipo_cliente() !=16){
	                    mensaje="Hola, bienvenido a MobileCard " + usuario.getUsr_nombre() + " tu usuario es: " + usuario.getUsr_login() + "  tu contraseña es: " + wPwd;
	                } else {
	                    mensaje = "Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contraseña son:\n Usuario: " + usuario.getUsr_login() + "\n Contraseña: " + wPwd;
	                }
	            }

	            logger.debug("Fin del Registro.");
	            if (usuario.getTipo().toUpperCase().equals("IPAD") || usuario.getTipo().toUpperCase().equals("IPOD")) {
	                if(usuario.getTipo_cliente()!=16){
	                    new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_REGISTRO", "@ASUNTO_REGISTRO", usuario.getUsr_login(), wPwd, "", "", "", "");
	                } else {
	                    new ParametroDAO().EnviaMailBGDF(usuario.getUsr_login(), wPwd,usuario.geteMail());
	                }
	            } else {
	                /*          INICIO DE LA MODIFICACION
	                 * SE IMPLEMENTO EL METODO DE EnviarSMS QUE UTILIZA
	                 * EL ADDCELBATCH PARA VERIFICAR EN LA DB LOS SMS QUE 
	                 * NO SE HAN ENVIADO. SE COMENTO EL METODO ANTERIO DE
	                 * setSMS QUE ESTABA EN .NET (YA NO SE UTILIZA)
	                 * 
	                 * 25/07/13
	                 * SE AGREGO EL ENVIO DE EMAIL A TODOS LOS DISPOSITOVOS 
	                 * DESPUES DEL REGISTRO, PRIMERO ENVIA EL SMS Y LUEGO EL 
	                 * MAIL EN CASO DE LOS QUE NO SON IPAD NI IPOD.
	                 */
	                //setSMS(usuario.getNombre(), usuario.getTelefono(), Mensaje);
	                logger.debug("Inicia envio de SMS...");
	                if (new UsuarioDAO().EnviarSMS(usuario.getUsr_telefono(), mensaje )) {
	                    logger.debug("Se registro el envio del mensaje con exito para el numero " + usuario.getUsr_telefono() + " usuario " + usuario.getUsr_nombre());
	                } else {
	                    logger.debug("No se pudo registrar el envio del mensaje para el numero " + usuario.getUsr_telefono() + " usuario " + usuario.getUsr_nombre());
	                }
	                if(usuario.getTipo_cliente()!=16){
	                    new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_REGISTRO", "@ASUNTO_REGISTRO", usuario.getUsr_login(), wPwd, "", "", "", "");
	                } else {
	                    new ParametroDAO().EnviaMailBGDF(usuario.getUsr_login(), wPwd,usuario.geteMail());
	                }
	                /*
	                 *          FIN DE LOS CAMBIOS
	                 */
	            }

	            flag = true;
	            if (!usuario.getEtiqueta().equals("") && !usuario.getNumero().equals("") && usuario.getDv() != 0) {
	                UsuarioTagsVO usuariotag = new UsuarioTagsVO();
	                UsuarioVO x = new UsuarioVO();
	                x.setUsr_login(usuario.getUsr_login());
	                x.setUsr_pwd(usuario.getUsr_pwd());

	                logger.debug("Cadena usuario.getLogin() " + usuario.getUsr_login());
	                logger.debug("Cadena x.getLogin() " + x.getUsr_login());

	                x = mapper.getUsuario(x.getUsr_login());
	                logger.debug("Cadena x.getLogin() 2 " + x.getUsr_login());
	                logger.debug("Cadena x.getUsuario() 2 " + x.getId_usuario());

	                usuariotag.setId_usuario(x.getId_usuario());
	                usuariotag.setEtiqueta(usuario.getEtiqueta());
	                usuariotag.setId_tiporecargatag(usuario.getIdtiporecargatag());
	                usuariotag.setTag(usuario.getNumero());
	                usuariotag.setDv(usuario.getDv());

	                /*if (new UsuarioDAO().addTag(usuariotag)) {
	                    logger.debug("Se registro el usuario TAG con EXITO: "  +x.getUsuario() );
	                } else {
	                    logger.debug("No se pudo registrar el usuario TAG: "  +x.getUsuario() );
	                }*/
	                try
	                {
	                	mapper.insertUsuarioTags(usuariotag);
	                	logger.debug("Se registro el usuario TAG con EXITO: "  +x.getId_usuario() );
	                } catch(Exception ex){
	                	logger.debug("No se pudo registrar el usuario TAG: "  +x.getId_usuario() );
	                }
	                
	            }
	        } catch (Exception ex) {
	            logger.error("Ocurrio un error en metodo add de clase UsuarioDAO: {}",ex);
	        }
	        logger.debug("Resgitro Status: " + flag);
	        return flag;
	    }
	 /*               INICIO DEL CAMBIO
	     * SE AGREGO ESTE METODO PARA EL ENVIO DE MENSAJES DE TEXTO
	     * FUNCIONA CON LA APLICACION DE ADDCELBATCH
	     * TODOS LOS REGISTROS QUE LLEGUEN A LA TABLA ozekimessageout
	     * SE ESTAN VERIFICANDO EN BUSQUEDA DEL STATUS "Send" SI TIENE
	     * ESTE ESTADO EL ADDCELBATCH LO ENCUENTRA Y LO ENVIA CAMBIANDO EL 
	     * STATUS A "transmitted Y REGISTRANDO SU SALIDA EN smsOut
	     */
	    public boolean EnviarSMS(String numero, String mensaje) throws Exception {
	    	OzekimessageoutVO msg = new OzekimessageoutVO();
	        boolean flag = false;
	        PreparedStatement ps = null;
	        msg.setReceiver(numero);
	        msg.setMsg(mensaje);
	        msg.setOperator("MobileCard");
	        msg.setMsgtype("SMS:TEXT:GSM7BIT");
	        msg.setSender("MobileCard");
	        msg.setReference("MobileCard - APP");
	        msg.setStatus("Send");
	        msg.setSenttime(new Date(new java.util.Date().getTime()));
	        try {
	            mapper.insertOzekimessageout(msg);
	            flag = true;
	        } catch (Exception ex) {
	            logger.error("Ocurrio un error en metodo EnviarSMS dentro de clase UsuarioDAO: {}",ex);
	        } finally {
	            //closeConPreparedSt(ps);
	        }
	        return flag;
	    }
	    /*
	     *      FIN DEL CAMBIO
	     */
	    public boolean update(UsuarioVO usuario) throws Exception {
	        boolean flag = false;
	        try {
	        	if (new UsuarioDAO().UsuarioActivo(usuario)) {
	                mapper.updateUsuarios(usuario);
	                flag = true;
	            } else {
	                flag = false;
	            }
	        } catch (Exception ex) {
	            logger.error("Ocurrio un error en metodo update(): {}",ex);
	        } 
	        return flag;
	    }
	/*
	 * metodo para consultar si un usuario se encuentra activo
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean UsuarioActivo(UsuarioVO usuario) throws Exception {
    boolean flag = false;
    UsuarioVO user = new UsuarioVO();
    try {
        user = mapper.usuarioActivo(usuario.getUsr_login());
        if (user != null) {
            flag = true;
        }
    } catch (Exception ex) {
        logger.error("Ocurrio un error en metodo UsuarioActivo en clase UsuarioDAO: {}",ex);
    }
    return flag;
}

}
