/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ironbit.addcell.entity.amex;

/**
 *
 * @author JCDP
 */
public class RespuestaAmex {
    private String transaction;
    private String code;
    private String dsc;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }
}
