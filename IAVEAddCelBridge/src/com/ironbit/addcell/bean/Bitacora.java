/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ironbit.addcell.bean;


/**
 *
 * @author -
 */
public class Bitacora {
    private long id;
    private long usuario;
    private long proveedor;
    private long producto;
    private String fecha;
    private String concepto;
    private float cargo;
    private String no_autorizacion;
    private int codigo_error;
    private int car_id;
    private int status;
    private String ticket;
    private String imei;
    private String destino;
    private String tarjeta_compra;
    private String tipo;
    private String software;
    private String modelo;
    private String key;
    private long pase;

    public long getPase() {
        return pase;
    }

    public void setPase(long pase) {
        this.pase = pase;
    }
        
    /**
     * @return the usuario
     */
    public long getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(long usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the proveedor
     */
    public long getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(long proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the producto
     */
    public long getProducto() {
        return producto;
    }

    /**
     * @param producto the producto to set
     */
    public void setProducto(long producto) {
        this.producto = producto;
    }

    /**
     * @return the fecha
     */
    public String  getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the concepto
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concepto the concepto to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the cargo
     */
    public float getCargo() {
        return cargo;
    }

    /**
     * @param cargo the cargo to set
     */
    public void setCargo(float cargo) {
        this.cargo = cargo;
    }

    /**
     * @return the no_autorizacion
     */
    public String getNo_autorizacion() {
        return no_autorizacion;
    }

    /**
     * @param no_autorizacion the no_autorizacion to set
     */
    public void setNo_autorizacion(String no_autorizacion) {
        this.no_autorizacion = no_autorizacion;
    }

    /**
     * @return the codigo_error
     */
    public int getCodigo_error() {
        return codigo_error;
    }

    /**
     * @param codigo_error the codigo_error to set
     */
    public void setCodigo_error(int codigo_error) {
        this.codigo_error = codigo_error;
    }

    /**
     * @return the car_id
     */
    public int getCar_id() {
        return car_id;
    }

    /**
     * @param car_id the car_id to set
     */
    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the ticket
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * @param ticket the ticket to set
     */
    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

        /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    } 

    
    /**
     * @param destino the destino to set
     */
    public void setDestino(String destino) {
        this.destino = destino;
    } 
    /**
     * @return the destino
     */
    public String getDestino() {
        return destino;
    }

    
    /**
     * @param destino the destino to set
     */
    public void setTarjeta_Compra(String tarjeta_compra) {
        this.tarjeta_compra = tarjeta_compra;
    } 
    /**
     * @return the destino
     */
    public String getTarjeta_Compra() {
        return tarjeta_compra;
    }

       public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
       
         
}
