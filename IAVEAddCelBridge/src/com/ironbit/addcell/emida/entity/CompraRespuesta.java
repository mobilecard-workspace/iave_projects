/**
 * 
 */
package com.ironbit.addcell.emida.entity;

/**
 * @author marco
 *
 */
public class CompraRespuesta {
	private String version;
	private String invoiceNo;
	private String responseCode;
	private String pin;
	private String controlNo;
	private String carrierControlNo;
	private String customerServiceNo;
	private String transactionDateTime;
	private String resultCode;
	private String responseMessage;
	private String transactionId;
	
	public String getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}
	public String getControlNo() {
		return controlNo;
	}
	public void setControlNo(String controlNo) {
		this.controlNo = controlNo;
	}
	public String getCarrierControlNo() {
		return carrierControlNo;
	}
	public void setCarrierControlNo(String carrierControlNo) {
		this.carrierControlNo = carrierControlNo;
	}
	public String getCustomerServiceNo() {
		return customerServiceNo;
	}
	public void setCustomerServiceNo(String customerServiceNo) {
		this.customerServiceNo = customerServiceNo;
	}
	public String getTransactionDateTime() {
		return transactionDateTime;
	}
	public void setTransactionDateTime(String transactionDateTime) {
		this.transactionDateTime = transactionDateTime;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	/**
	 * @return the resultCode
	 */
	public String getResultCode() {
		return resultCode;
	}
	/**
	 * @param resultCode the resultCode to set
	 */
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	
}
