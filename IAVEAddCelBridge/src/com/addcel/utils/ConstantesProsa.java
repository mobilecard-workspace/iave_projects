/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.utils;

/**
 *
 * @author efren
 */
public class ConstantesProsa {
    
    //URL'S
    public static final String URL_AUTH_PROSA = "http://localhost:8080/ProsaWeb/ProsaAuth";
    public static final String URL_AUTH_PROSA_BYUSR = "http://localhost:8080/ProsaWeb/ProsaAuthByUser";
    public static final String URL_REVERSO_PROSA = "http://localhost:8080/ProsaWeb/ProsaRev";
    
    //public static final String URL_AUTH_PROSA = "http://50.57.192.214:8080/ProsaWeb/ProsaAuth";
    //public static final String URL_AUTH_PROSA_BYUSR = "http://50.57.192.214:8080/ProsaWeb/ProsaAuthByUser";
    //public static final String URL_REVERSO_PROSA = "http://50.57.192.214:8080/ProsaWeb/ProsaRev";
    
    //CODIGOS MONEDA
    public static final String USD = "840";
    public static final String MXP = "484";
}
