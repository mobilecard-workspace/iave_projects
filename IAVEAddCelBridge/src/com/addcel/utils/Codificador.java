/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Desarrollo
 */
public class Codificador {

    public String getEncoded(String stringToHash) {
        U log = new U();

        final char[] HEXADECIMAL = { '0', '1', '2', '3',
    	        '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] bytes = md.digest(stringToHash.getBytes());
            StringBuilder sb = new StringBuilder(2 * bytes.length);
            for (int i = 0; i < bytes.length; i++) {
                int low = (int)(bytes[i] & 0x0f);
                int high = (int)((bytes[i] & 0xf0) >> 4);
                sb.append(HEXADECIMAL[high]);
                sb.append(HEXADECIMAL[low]);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            log.Display(Codificador.class.getName() + ex.getMessage());
            return "";
        }
    }
}
