package com.addcel.mx.antad.servicios.controller;

import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_CATALOGOS_SERVICIOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_CATALOGOS_PARAMETROS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_INICIO_CONSULTA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_SALDO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_PAGO_SERVICIOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.TESTING_SERVICES_WEB;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraiaveVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraprosaVO;
import com.addcel.mx.antad.servicios.model.vo.CatCarriesVO;
import com.addcel.mx.antad.servicios.model.vo.EstadosVO;
import com.addcel.mx.antad.servicios.model.vo.ProductoVO;
import com.addcel.mx.antad.servicios.model.vo.ProveedorVO;
import com.addcel.mx.antad.servicios.model.vo.TBancoVO;
import com.addcel.mx.antad.servicios.model.vo.TTipoTarjetaVO;
import com.addcel.mx.antad.servicios.model.vo.TransactionProcomVO;
import com.addcel.mx.antad.servicios.model.vo.Usuario;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.services.AntadServiciosService;
import com.addcel.prosa.ProsaResponse;
import com.addcel.utils.U;
import com.google.gson.Gson;
import com.ironbit.addcell.bean.CompraIAVE;
import com.ironbit.addcell.bean.CompraResponce;
import com.ironbit.addcell.bean.Contenedor;
import com.ironbit.addcell.bean.Producto;
import com.ironbit.addcell.bean.RespuestaIAVE;
import com.ironbit.addcell.bussines.amex.PagoAmex;
import com.ironbit.addcell.bussines.amex.PagoAmexImpl;
import com.ironbit.addcell.entity.amex.RespuestaAmex;
import com.ironbit.addcell.integration.ParametroDAO;
import com.ironbit.addcell.ws.clientes.iavePrepago.WsPrepagoRecargasSoapProxy;
import com.google.gson.reflect.TypeToken;
import com.ironbit.addcell.bean.Respuesta;
import com.ironbit.addcell.bean.UserPasswordUpdate;
import com.ironbit.addcell.ws.clientes.iavePrepago.WsPrepagoRecargasSoapProxy;








import crypto.Crypto;

import java.util.Calendar;
import java.util.List;

@Controller
public class AntadServiciosController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AntadServiciosController.class);
	
	private static final String PATH_CONSULTA_PARAMETROS= "/consultaCatalogoParametros";
	
	private static final String PATH_CONSULTA_USUARIO= "/consultaUsuarioPorId";
	
	private static final String PATH_VALIDA_LOGIN_USUARIO= "/validaLoginUsuario";
	
	private static final String PATH_CONSULTA_PRODUCTO_CLAVE= "/consultaProductoClave";
	
	private static final String PATH_CONSULTA_PROVEEDOR_CLAVE= "/consultaProveedorClave";
	
	private static final String PATH_CONSULTA_COMISION_CLAVE_PROVEEDOR = "/consultaComisionClaveProveedor";
	
	private static final String PATH_ACTUALIZA_BITACORA_IAVE= "/actualizaBitacoraIave";
	
	private static final String PATH_ACTUALIZA_BITACORA= "/actualizaBitacora";

	private static final String PATH_CONSULTA_SERVICIOS= "/consultaCatalogoServiciosAntad";
	
	// INI pDiaz
	private static final String PATH_CONSULTA_ENDPOINTS= "/consultaEndPoints";
	// FIN pDiaz
	
	private static final String PATH_REALIZA_PAGO= "/procesaPagoAntad";
	
	private static final String PATH_EJECUTACOMPRA_IAVE3DS= "/adc_purchase_iave3Ds";
        
    private static final String PATH_INSERTA_USUARIO= "/adc_userInsert";
        
    private static final String PATH_USUARIO_LOGIN= "/adc_userLogin";
        
    private static final String PATH_USUARIO_PASSWORD_UPDATE= "/adc_userPasswordUpdate";
        
    private static final String PATH_USUARIO_UPDATE = "/adc_userUpdate";
        
    private static final String PATH_GET_ESTADOS = "/adc_getEstados";
        
    private static final String PATH_GETUSER_DATA = "adc_getUserData";
    
    private static final String PATH_GETTIPOS_TARJETAS = "adc_getCardType";
    
    private static final String PATH_GET_BANCOS = "adc_getBanks";
    
    private static final String PATH_GET_PROVIDERS= "adc_getProviders";
    
    private static final String PATH_GET_PRODUCTS= "adc_getProducts";
	
	private static final String PATH_REALIZA_CONSULTA= "/procesaConsultaSaldosAntad";
	
	private static final String PATH_TEST = "/test";
	
	private static final String VIEW_HOME = "home";
	
	private static final String REQ_PARAM_JSON = "json";
	
	private static final String REQ_PARAM_COMPRA = "compra";
	
	private static final String REQ_PARAM_TRANSACTION = "transactionProcomVO";
	
	private static final String TelefonoServicio="5525963513";
	
	public static String setSMS(String Telefono){
        return Crypto.aesEncrypt(parsePass(TelefonoServicio),  Telefono);    
    }
	
	public static String getSMS(String Telefono){
        return Crypto.aesDecrypt(parsePass(TelefonoServicio),  Telefono);    
    }
	
	public AntadServiciosController() {

	}
	
	@Autowired
	private AntadServiciosService antadService;
	
	public static String parsePass(String pass){
        int len = pass.length();
        String key = "";

        for (int i =0; i < 32 /len; i++){
              key += pass;
        }

        int carry = 0;
        while (key.length() < 32){
              key += pass.charAt(carry);
              carry++;
        }
        return key;
    }
	
	@RequestMapping(value = PATH_TEST, method=RequestMethod.GET)
	public ModelAndView home() {	
		LOGGER.info(TESTING_SERVICES_WEB);
		ModelAndView mav = new ModelAndView(VIEW_HOME);
		return mav;	
	}
	
	@RequestMapping(value = PATH_CONSULTA_SERVICIOS, method=RequestMethod.POST)
	public @ResponseBody String consultaServicios(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_CONSULTA_CATALOGOS_SERVICIOS);
		return antadService.consultaServicios(jsonEnc);
	}

	@RequestMapping(value = PATH_CONSULTA_PARAMETROS, method=RequestMethod.GET)
	public @ResponseBody String consultaParametros(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_CONSULTA_CATALOGOS_PARAMETROS);
		return antadService.consultaParametros(jsonEnc);
	}
	
	@RequestMapping(value = PATH_CONSULTA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String consultaUsuarioById(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return antadService.consultaUsuarioById(jsonEnc).getUsr_login();
	}
	
	@RequestMapping(value = PATH_VALIDA_LOGIN_USUARIO, method=RequestMethod.POST)
	public @ResponseBody UsuarioVO validaLoginUsuario(@RequestParam(REQ_PARAM_JSON) String jsonEnc, @RequestParam(REQ_PARAM_JSON) String pwd) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		LOGGER.info("usuario: "+jsonEnc);
		LOGGER.info("password: "+pwd);
		return antadService.validaUsuario(jsonEnc,pwd);
	}
	
	@RequestMapping(value = PATH_CONSULTA_PRODUCTO_CLAVE, method=RequestMethod.POST)
	public @ResponseBody String consultaProducto(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return antadService.consultaProducto(jsonEnc).getNombre();
	}
	
	@RequestMapping(value = PATH_CONSULTA_PROVEEDOR_CLAVE, method=RequestMethod.POST)
	public @ResponseBody String consultaProveedor(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return antadService.consultaProveedor(jsonEnc).getPrv_nombre_completo();
	}
	
	@RequestMapping(value = PATH_CONSULTA_COMISION_CLAVE_PROVEEDOR , method=RequestMethod.POST)
	public @ResponseBody String consultaComision(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		return Double.toString(antadService.consultaComision(jsonEnc));
	}
	
	@RequestMapping(value = PATH_ACTUALIZA_BITACORA, method=RequestMethod.POST)
	public @ResponseBody String actualizaBitacora(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		BitacoraVO bitacora = new BitacoraVO();
		bitacora.setId_bitacora(362);
		bitacora.setBit_concepto("Compra TA Telcel 5534918527 20.00 modificado JCDP");
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		antadService.actualizaBitacora(bitacora);
		return "Bitacora actualizada";
	}
	
	@RequestMapping(value = PATH_ACTUALIZA_BITACORA_IAVE, method=RequestMethod.POST)
	public @ResponseBody String actualizaBitacoraIave(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_INICIO_CONSULTA_USUARIO);
		//antadService.actualizaBitacoraIave(BitacoraiaveVO);
		return "BitacoraIAVE Actualizada";
	}
	
	@RequestMapping(value = PATH_CONSULTA_ENDPOINTS, method=RequestMethod.POST)
	public @ResponseBody String consultaEndPointsService(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info("pDiaz --------> "+LOG_PROCESO+LOG_PROCESO_CONSULTA_CATALOGOS_SERVICIOS);
		return antadService.consultaEndPointsService(jsonEnc);
	}

	
	@RequestMapping(value = PATH_REALIZA_PAGO, method=RequestMethod.POST)
	public @ResponseBody String realizaPago(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_PAGO_SERVICIOS);
		return antadService.realizaPago(jsonEnc);
	}
	
	@RequestMapping(value = PATH_REALIZA_CONSULTA, method=RequestMethod.POST)
	public @ResponseBody String realizaConsulta(@RequestParam(REQ_PARAM_JSON) String jsonEnc) {
		LOGGER.info(LOG_PROCESO+LOG_PROCESO_CONSULTA_SALDO);
		return antadService.realizaConsulta(jsonEnc);
	}
	
	/*nuevo Request mapping con la nueva arquitectura de mybatis y Spring MVC*/
	@RequestMapping(value = PATH_EJECUTACOMPRA_IAVE3DS, method=RequestMethod.POST)
	public @ResponseBody String ejecutaCompra(@RequestParam(REQ_PARAM_JSON)  String jsonEnc, @RequestParam(REQ_PARAM_TRANSACTION) String transaction) {
		String res = ""; 
		new com.addcel.utils.U().Display("Inicio RECARGA IAVE... " );
		String json = null;
		String key = "1234567890ABCDEF0123456789ABCDEF";
		String respuesta = null;

		CompraIAVE compraiave  = null;
		CompraResponce compraResponce = null;
		TransactionProcomVO transactionProcomVO = null;
		LOGGER.info("jsonEnc contiene : " + jsonEnc );
		
		new com.addcel.utils.U().Display("Despues de Inicio RECARGA IAVE" );
		if(jsonEnc == null ) {
			new com.addcel.utils.U().Display("parameter json null " );
		}
		if(jsonEnc.equals("")){
		    json = (String) jsonEnc.toString();
		    new com.addcel.utils.U().Display("Despues de Inicio RECARGA IAVE Dentro de If" );
		}else{
		    json = (String) jsonEnc.toString();
		    new com.addcel.utils.U().Display("Despues de Inicio RECARGA IAVE Dentro de else" );
		}
		new com.addcel.utils.U().Display("CADENA PurchaseIave: "  + json);
		Gson gson = new Gson();
		try{
		    String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
		    key = Cad[0];
		    json = Cad[1];
		    Type collectionType = new TypeToken<CompraIAVE>(){}.getType();
		    compraiave = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
		}catch(Exception e){	
		    compraResponce = new CompraResponce();
		    compraResponce.setMensaje("password incorrecto");
		    e.printStackTrace();
		}
		transactionProcomVO = gson.fromJson(transaction,TransactionProcomVO.class);
		if((compraiave != null)&&(transactionProcomVO != null)){
			new com.addcel.utils.U().Display("CADENA idBitacora: "  + transactionProcomVO.getEmOrderID());
			new com.addcel.utils.U().Display("key : " + key );
			new com.addcel.utils.U().Display("Password : " + compraiave.getPassword() );
			/*INICIO DE NUEVO PROCESO CON LA NUEVA ARQUITECTURA MYBATIS SPRING MVC */
			
			UsuarioVO usuario = new UsuarioVO();
			//usuario.setPassword(compra.getPassword());
			usuario.setUsr_pwd(compraiave.getPassword());
			//usuario.setLogin(compra.getLogin());
			usuario.setUsr_login(compraiave.getLogin());
			
			double MontoCentavos = 0.0D;
			String folio = "";

			String CodeError = "";
			String DescError = "";
            
			ProsaResponse Resbanco = new ProsaResponse();
			Integer idBitacora = Integer.parseInt(transactionProcomVO.getEmOrderID());
			LOGGER.info("Dentro de ejecutaCompraIAVE3Ds");
			LOGGER.info("transactionProcomVO: "+transactionProcomVO);
			LOGGER.info("getIdAplicacion: "+ compraiave.getIdAplicacion());
			
			if ((compraiave.getIdAplicacion() > 0) || antadService.validaUsuario(usuario.getUsr_login(),Crypto.aesEncrypt(key, usuario.getUsr_pwd())) != null ) { 
				try
				{
				LOGGER.info("/*************************** ENTRO AL IF CORRECTAMENTE !!!!!! ********************************/");
				System.out.println("/*************************** ENTRO AL IF CORRECTAMENTE !!!!!! ********************************/");
				String ActivaTrans = antadService.consultaParametros("@ACTIVA_TRANS_IAVE");
				LOGGER.info("ActivaTrans : "+ActivaTrans);
				String Comision = antadService.consultaParametros("@MONTO_COMISION");
				LOGGER.info("Comision : "+Comision);
				String idcom = null;
				String idgrp = null;
				String MontoCobrar = "";
				String response = null;
				if (compraiave.getIdAplicacion() > 0) {
					usuario = new UsuarioVO();
					usuario.setId_usuario(compraiave.getIdUsuario());
					usuario.setUsr_nombre(compraiave.getNombre());
					usuario.setUsr_apellido(compraiave.getApellido());
					usuario.setUsr_materno(compraiave.getMaterno());
					usuario.seteMail(compraiave.getEmail());
					usuario.setId_tipo_tarjeta(compraiave.getTipoTarjeta());
					usuario.setUsr_tdc_numero(compraiave.getTarjetaBanco());
					usuario.setUsr_tdc_vigencia(compraiave.getVigencia());
					usuario.setId_usr_status(1);
				} else {
					usuario = antadService.obtenerUsuario(usuario);
				}
				LOGGER.debug(
						"Inicia IAVE validacion Reglas negocio, idUsuario: " + usuario.getId_usuario() + ",  Tarjeta: "
								+ AntadServiciosController.setSMS(usuario.getUsr_tdc_numero()) + ",  Compra TAG: " + compraiave.getTarejeta());

				compraResponce = validaReglasNegocio(usuario.getId_usuario(), AntadServiciosController.setSMS(usuario.getUsr_tdc_numero()),
						0,antadService);
				
				if (compraResponce != null) {
					LOGGER.debug("No se satisfacen las Reglas Negocio: "+ compraResponce);
					//return compraResponce;
				}else{
					LOGGER.debug("Se satisfacen las Reglas Negocio");
				}
				compraResponce = new CompraResponce();
				compraResponce.setMensaje("password incorrecto");
				BitacoraVO bitacora = new BitacoraVO();

				bitacora.setTipo(compraiave.getTipo());
				bitacora.setSoftware(compraiave.getSoftware());
				bitacora.setModelo(compraiave.getModelo());
				bitacora.setWkey(compraiave.getKey());
				bitacora.setId_usuario(usuario.getId_usuario());
				
				LOGGER.debug("sets bitacora" + bitacora);
				
				ProductoVO producto = antadService.consultaProducto(compraiave.getProducto());
				
				LOGGER.debug("producto" + producto.getPro_clave());
				
				if (!esTuTag(compraiave.getProducto(),antadService)) {
					idcom = antadService.consultaParametros("@ID_COM_IAVE");
					idgrp = antadService.consultaParametros("@ID_GRP_IAVE");
				} else {
					idcom = antadService.consultaParametros("@ID_COM_IAVE_TUTAG");
					idgrp = antadService.consultaParametros("@ID_GRP_IAVE_TUTAG");
				}
				double Res = antadService.consultaComision(producto.getId_proveedor().toString());
                MontoCobrar = Double.toString(Double.parseDouble(Float.toString(producto.getPro_monto())) + Res);
                Comision = "" + Res;
                bitacora.setId_producto(Long.parseLong(producto.getPro_clave()));
				bitacora.setBit_card_id(Integer.parseInt(producto.getPro_clave()));
				//logger.debug("MPX PROVEEDOR: " + producto.getProveedor());
				LOGGER.debug("MPX PROVEEDOR: " + producto.getId_proveedor());
				//JCDP
				LOGGER.debug("MPX COMISION: " + Comision);
				//JCDP
				//logger.debug("MPX MONTO: " + producto.getMonto());
				LOGGER.debug("MPX MONTO: " + producto.getPro_monto());
				//JCDP
				LOGGER.debug("MPX MONTOCOBRAR: " + MontoCobrar);
				BigDecimal b = new BigDecimal(producto.getPro_monto());
				bitacora.setBit_cargo(b);
				bitacora.setId_proveedor(Integer.parseInt(Long.toString(producto.getId_proveedor())));
				ProveedorVO proveedor = antadService.consultaProveedor(Long.toString(bitacora.getId_proveedor()));
				bitacora.setBit_concepto("Compra IAVE " + proveedor.getPrv_nombre_comercio() + " " + compraiave.getTarejeta() + " "
						+ producto.getPro_monto() + " Comision: " + Comision);
				bitacora.setImei(compraiave.getImei());
				bitacora.setDestino(compraiave.getTarejeta());
				bitacora.setTarjeta_compra(usuario.getUsr_tdc_numero());
				bitacora.setId_bitacora(idBitacora);
				antadService.actualizaBitacora(bitacora);
				if (compraiave.getTarejeta().equals("")) {
					bitacora.setBit_codigo_error(820);
					antadService.actualizaBitacora(bitacora);
					compraResponce.setResultado(0);
					compraResponce.setFolio("820");
					compraResponce.setMensaje("Digite o seleccione el TAG IAVE para poder realizar la recarga.");
                                        respuesta = gson.toJson(compraResponce);
					json = Crypto.aesEncrypt(key, respuesta);
					return json;
				}
				if (compraiave.getTarejeta().length() <= 4) {
					bitacora.setBit_codigo_error(820);
					antadService.actualizaBitacora(bitacora);
					compraResponce.setResultado(0);
					compraResponce.setFolio("820");
					compraResponce.setMensaje("Digite o seleccione el TAG IAVE para poder realizar la recarga.");
                                        respuesta = gson.toJson(compraResponce);
					json = Crypto.aesEncrypt(key, respuesta);
					return json;
				}
				if (compraiave.getIdAplicacion() == 0) {
					if (antadService.validaT(ParametroDAO.setSMS(usuario.getUsr_tdc_numero()))) {
						bitacora.setBit_codigo_error(999);
						antadService.actualizaBitacora(bitacora);
						antadService.actualizaUsuario(usuario);
						antadService.BuscaIm(compraiave.getImei());
						compraResponce.setResultado(0);
						compraResponce.setFolio("999");
						compraResponce.setMensaje(antadService.consultaParametros("@MENSAJE_999"));
                                                respuesta = gson.toJson(compraResponce);
                                                json = Crypto.aesEncrypt(key, respuesta);
						return json;
					}
					if (antadService.ValidaIM(compraiave.getImei())) {
						bitacora.setBit_codigo_error(998);
						antadService.actualizaBitacora(bitacora);
                        UsuarioVO usu = new UsuarioVO();
                        usu = antadService.consultaUsuarioById(usuario.getUsr_login());
                        usu.setId_usr_status(0);
                        antadService.actualizaUsuario(usu);
                        antadService.BTar(usuario.getUsr_tdc_numero());
						compraResponce.setResultado(0);
						compraResponce.setFolio("998");
						compraResponce.setMensaje(antadService.consultaParametros("@MENSAJE_998"));
						LOGGER.info("antes del return ");
                                                respuesta = gson.toJson(compraResponce);
                                                json = Crypto.aesEncrypt(key, respuesta);
						return json;
					}
				}
				LOGGER.info("ya no llego aqui");
				if (usuario.getId_usr_status() == 0) {
					bitacora.setBit_codigo_error(997);
					antadService.actualizaBitacora(bitacora);
					compraResponce.setResultado(0);
					compraResponce.setFolio("997");
					compraResponce.setMensaje(antadService.consultaParametros("@MENSAJE_997"));
                                        respuesta = gson.toJson(compraResponce);
                                        json = Crypto.aesEncrypt(key, respuesta);
					return json;
				}
				MontoCentavos = Double.parseDouble(Float.toString(producto.getPro_monto()));
				folio = antadService.getFolioIAVE();
				
				String secuence = antadService.getFolioBancoIAVE();
				String Cobro = "" + (Double.parseDouble(Float.toString(producto.getPro_monto())) + Double.parseDouble(Comision));
				//rellenar datos para bitacoraProsa
				LOGGER.info("Tipo tarjeta : " + compraiave.getTipoTarjeta());
				if (compraiave.getTipoTarjeta() != 3) { //para visa y master card		
					
					
					Resbanco.setAutorizacion(transactionProcomVO.getEmAuth());
					Resbanco.setBancoAdquirente("HSBC");
					Resbanco.setBancoEmisor("OTROS");
					Resbanco.setClaveOperacion("0");										
					Resbanco.setDireccionComercio("PASEO DE LOS TAMARINDOS 400 TORRE A P26");					
					Resbanco.setFechaHora(transactionProcomVO.getFecha());
					Resbanco.setImporte(transactionProcomVO.getEmTotal());
					
					if(compraiave.getTipoTarjeta() == 1){ 
						Resbanco.setMarca("VISA");
					}	
					else
						if(compraiave.getTipoTarjeta() == 2){
							Resbanco.setMarca("MASTER CARD");
						}
					
					Resbanco.setMoneda(transactionProcomVO.getMoneda());
					Resbanco.setNombreComercio("I+D MEXICO");
					Resbanco.setNumeroCaja(transactionProcomVO.getEmTerm());
					Resbanco.setProducto(transactionProcomVO.getProducto());
					Resbanco.setTransactionId(transactionProcomVO.getEmOrderID());
					
				} else { //si es amex
					/*
					* *******************************************************************************
					* */
					PagoAmex amex = new PagoAmexImpl();
					
					RespuestaAmex respAmex = new RespuestaAmex();
					respAmex.setTransaction(idBitacora.toString());
					respAmex.setCode("000");
					respAmex.setDsc("Approved");
					
					amex.realizarPagoTelepeaje(usuario.getUsr_tdc_numero(), usuario.getUsr_tdc_vigencia() ,
							Cobro, compraiave.getCvv2().replace("/", " ").trim(), usuario.getUsr_direccion(), usuario.getUsr_cp(),
							"IAVE");
					
					Resbanco.setAutorizacion(respAmex.getCode());
					Resbanco.setTransactionId(respAmex.getTransaction());
					Resbanco.setDescripcionRechazo(respAmex.getDsc());

					Resbanco.setBancoAdquirente("American Express");
					Resbanco.setBancoEmisor("American Express");
					Resbanco.setClaveOperacion("0");
					Resbanco.setDireccionComercio("AV P DE LA REFORMA NO 2654 P8");
					Resbanco.setError(respAmex.getDsc());
					Resbanco.setFechaHora(new Date().toString());
					Resbanco.setImporte(Cobro);
					Resbanco.setMarca("AMEX");
					Resbanco.setMoneda("MXN");
					Resbanco.setNombreComercio("ADDCEL");
					Resbanco.setNumeroCaja("99");
					Resbanco.setProducto("CREDITO");
					Resbanco.setIsAuthorized(false);
					if (respAmex.getCode().equals("000")) {
						Resbanco.setIsAuthorized(true);
					}
					/*
					* *******************************************************************************
					* */
				}
				LOGGER.debug("CADENA IAVE DESPUES BANCO IsAuthorized : " + Resbanco.isIsAuthorized());
				LOGGER.debug("CADENA IAVE DESPUES BANCO Error : " + Resbanco.getError());
				LOGGER.debug("CADENA IAVE DESPUES BANCO ClaveRechazo : " + Resbanco.getClaveRechazo());
				LOGGER.debug("CADENA IAVE DESPUES BANCO DescripcionRechazo : " + Resbanco.getDescripcionRechazo());
				
				//insertar bitacoraIAVE
				/*new BitacoraDAO().addBitacoraIAVE(bitacora.getId(), bitacora.getUsuario(),
						parametroDAO.getParametro("@POSNAME"), parametroDAO.getParametro("@POSPWD"),
						parametroDAO.getParametro("@USERPWD"), Resbanco, secuence, Comision, compra.getCx(),
						compra.getCy(), compra.getImei(), compra.getTarejeta(), "");*/
				//insertar bitacoraIAVE
				/*Nuevo insert para nueva arquitectura con mybatis */
				BitacoraiaveVO bitvo = new BitacoraiaveVO();
				bitvo.setId_bitacora(bitacora.getId_bitacora());
				bitvo.setId_usuario(bitacora.getId_usuario());
				bitvo.setPosname(antadService.consultaParametros("@POSNAME"));
				bitvo.setPospwd(antadService.consultaParametros("@POSPWD"));
				bitvo.setUserpwd(antadService.consultaParametros("@USERPWD"));
				bitvo.setAfiliacion(Resbanco.getAfiliacion());
				bitvo.setAutorizacion(Resbanco.getAutorizacion());
				bitvo.setBancoAdquirente(Resbanco.getBancoAdquirente());
				bitvo.setBancoEmisor(Resbanco.getBancoEmisor());
				bitvo.setClaveOperacion(Resbanco.getClaveOperacion());
				bitvo.setClaveVenta("");
				bitvo.setDescripcion("");
				bitvo.setDireccionComercio(Resbanco.getDireccionComercio());
				bitvo.setErrCode("");
				bitvo.setFechaHora(Resbanco.getFechaHora());
				bitvo.setImporte(Resbanco.getImporte());
				bitvo.setMarca(Resbanco.getMarca());
				bitvo.setMoneda(Resbanco.getMoneda());
				bitvo.setNombreComercio(Resbanco.getNombreComercio());
				bitvo.setNumeroCaja(Resbanco.getNumeroCaja());
				bitvo.setProducto(Resbanco.getProducto());
				bitvo.setTrack2("");
				bitvo.setSequence(secuence);
				bitvo.setComision(Comision);
				bitvo.setCx(compraiave.getCx());
				bitvo.setCy(compraiave.getCy());
				bitvo.setImei(compraiave.getImei());
				bitvo.setTag(compraiave.getTarejeta());
				bitvo.setCliente("");
				bitvo.setTransactionid(Resbanco.getTransactionId());
				antadService.insertaBitacoraIave(bitvo);
				LOGGER.info("folio, compra.getTarejeta(), PasoMonto, compra.getPin(),idcom, idgrp, ActivaTrans");
				LOGGER.info(folio+","+compraiave.getTarejeta()+","+"" + MontoCentavos+","+compraiave.getPin()+","+idcom+","+idgrp+","+ActivaTrans);
				
				LOGGER.info("transactionProcomVO : " + transactionProcomVO.getEmResponse());
				
				if (transactionProcomVO.getEmResponse().equalsIgnoreCase("approved")) {
					String PasoMonto = "" + MontoCentavos;

					response = "";

					response = executePurchaseIAVERecarga(folio, compraiave.getTarejeta(), PasoMonto, compraiave.getPin(),
							idcom, idgrp, ActivaTrans);
					LOGGER.debug("XML IAVE RESPUESTA INTENTO 1: " + response);
					RespuestaIAVE tmp = null;

					tmp = ParceXMLIAVE(response);
					if (!tmp.getResponsecode().equals("00")) {
						folio = antadService.getFolioIAVE();

						response = executePurchaseIAVERecarga(folio, compraiave.getTarejeta(), PasoMonto, compraiave.getPin(),
								idcom, idgrp, ActivaTrans);
						LOGGER.debug("XML IAVE RESPUESTA INTENTO 2: " + response);
						tmp = ParceXMLIAVE(response);
						if (!tmp.getResponsecode().equals("00")) {
							folio = antadService.getFolioIAVE();

							response = executePurchaseIAVERecarga(folio, compraiave.getTarejeta(), PasoMonto,
									compraiave.getPin(), idcom, idgrp, ActivaTrans);
							LOGGER.debug("XML IAVE RESPUESTA INTENTO 3: " + response);
							tmp = ParceXMLIAVE(response);
							//if ((!tmp.getResponsecode().equals("00")) && (usuario.getTipotarjeta() != 3)) {
							if ((!tmp.getResponsecode().equals("00")) && (usuario.getId_tipo_tarjeta() != 3)) { 
								tmp.setResponsecode("771");
								tmp.setDescriptioncode("Error - Banco(8)");
								LOGGER.debug("ENTRO A LA REVERSA...");
								
								/********************/
								response = "ERRORREVERSE";
								CodeError = "1200";
								DescError = "No se pudo abonar saldo, solicitar devoluciÃ³n con I + D";
								/********************/
								
								/*new BitacoraDAO().addBitacoraReversa(bitacora.getId(), bitacora.getUsuario(), Resbanco,
										secuence, "IAVE");

								ProsaResponse ResReversa = null;
								if (ActivaTrans.equals("1")) {
									logger.debug("Ambiente PROD, Ejecuta reversa ");
									if (usuario.getTipotarjeta() == 3) {
										ResReversa = this.amexDAO.marcarReverso(Resbanco);
									} else {
										ResReversa = devolution(Resbanco.getTransactionId());
									}
								} else {
									logger.debug("Ambiente QA, REVERSA BITACORA OK ");
									ResReversa = new ProsaResponse();
									ResReversa.setIsAuthorized(true);
									ResReversa.setAfiliacion("7255872");
									ResReversa.setAutorizacion("" + numFolioTAE++);
									ResReversa.setBancoAdquirente("HSBC");
									ResReversa.setBancoEmisor("ORO");
									ResReversa.setClaveOperacion("4067056");

									ResReversa.setDireccionComercio(";AV P DE LA REFORMA NO 2654 P8");

									ResReversa.setFechaHora("2012-01-02T16:26:26.962-06:00");
									ResReversa.setImporte(""
											+ (Double.parseDouble(producto.getMonto()) + Double.parseDouble(Comision)));
									ResReversa.setMarca("VISA");
									ResReversa.setMoneda("MXN");
									ResReversa.setNombreComercio("ADDCEL 2");
									ResReversa.setNumeroCaja("99");
									ResReversa.setProducto("CREDITO");
								}
								if (ResReversa.isIsAuthorized()) {
									StringBuffer pXML2 = new StringBuffer().append("<tr><td> AUTORIZACION: </td><td>")
											.append(ResReversa.getAutorizacion()).append("</td></tr>")
											.append("<tr><td> IMPORTE: </td><td>$ ").append(ResReversa.getImporte())
											.append("0 MXN</td></tr>").append("<tr><td> REFERENCIA: </td><td>")
											.append(bitacora.getId()).append("</td></tr>");

									logger.debug("XML IAVE REVERSA EXITOSA: " + pXML2);

									new BitacoraDAO().UpdateBitacoraReversa(bitacora.getId(), bitacora.getUsuario(),
											pXML2 + " ");

									parametroDAO.EnviaMail(
											usuario.getNombre() + (usuario.getApellido() != null
													? " " + usuario.getApellido() : ""),
											usuario.getMail(), "@MENSAJE_REVERSO", "@ASUNTO_REVERSO_OK",
											pXML2.toString(), "", "", "", "", "");
									if (ActivaTrans.equals("1")) {
										parametroDAO.EnviaMail("Jorge", "jorge@addcel.com", "@MENSAJE_REVERSO",
												"@ASUNTO_REVERSO_OK", pXML2.toString(), "", "", "", "", "");
									}
								} else {
									String pXML2 = "";

									pXML2 = pXML2 + "[ Producto =  IAVE  ] </br>";
									pXML2 = pXML2 + "[ Id Bitacora = " + bitacora.getId() + " ] </br>";
									pXML2 = pXML2 + "[ Id ORDER/FOLIO = " + folio + " ] </br>";
									pXML2 = pXML2 + "[ Id TAG = " + compra.getTarejeta() + " ] </br>";
									pXML2 = pXML2 + "[ MONTO = " + PasoMonto + " ] </br>";

									pXML2 = pXML2 + "*** DATOS BANCARIOS ***  </br>";
									pXML2 = pXML2 + "[ AUTORIZACION = " + Resbanco.getAutorizacion() + " ] </br>";
									pXML2 = pXML2 + "[ BANCO ADQUIRIENTE = " + Resbanco.getBancoAdquirente()
											+ " ] </br>";
									pXML2 = pXML2 + "[ BANCO EMISOR = " + Resbanco.getBancoEmisor() + " ] </br>";
									pXML2 = pXML2 + "[ CLAVE OPERACION = " + Resbanco.getClaveOperacion() + " ] </br>";

									pXML2 = pXML2 + "[ FECHA - HORA = " + Resbanco.getFechaHora() + " ] </br>";
									pXML2 = pXML2 + "[ IMPORTE = " + Resbanco.getImporte() + " ] </br>";
									pXML2 = pXML2 + "[ MARCA = " + Resbanco.getMarca() + " ] </br>";
									pXML2 = pXML2 + "[ NUMERO CAJA = " + Resbanco.getNumeroCaja() + " ] </br>";
									pXML2 = pXML2 + "[ PRODUCTO = " + Resbanco.getProducto() + " ] </br>";

									pXML2 = pXML2 + "[ CODIGO ERROR DEVOLUCIO = " + ResReversa.getError() + " ] </br>";
									pXML2 = pXML2 + "[ ERROR DEVOLUCIO = " + ResReversa.getDescripcionRechazo()
											+ " ] </br>";

									logger.debug("XML IAVE REVERSA ERROR: " + pXML2);

									new BitacoraDAO().UpdateBitacoraReversa(bitacora.getId(), bitacora.getUsuario(),
											"ERROR EN WS DE REVERSA: " + pXML2 + " ");
									if (ActivaTrans.equals("1")) {
										parametroDAO.EnviaMail("Jorge", "jorge@addcel.com", "@MENSAJE_REVERSO_ERROR",
												"@ASUNTO_REVERSO_ERROR", pXML2, "", "", "", "", "");
									}
								}*/
							}
						}
					}
					if(tmp != null && tmp.getResponsecode().equals("00")){
						try{
							
							Date d = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(tmp.getLocal_date());
							/****************   Modificar metodo al final del proceso    04/07/2016
							new BitacoraDAO().updateBit_Hora(bitacora.getId(), d);
							********************************/
							bitacora.setBit_hora(d);
							antadService.actualizaBitacora(bitacora);
							LOGGER.debug("bit_hora actualizada en t_bitacora id: " + bitacora.getId_bitacora());
								
						}catch(Exception ex){
							LOGGER.error("Error al actualizar bit_hora", ex);
						}
					}
				}//fin de if transaction procomVO 	
				else {
					response = "ERROR";
					try {
						CodeError = Resbanco.getClaveRechazo();
						DescError = Resbanco.getDescripcionRechazo();
					} catch (Exception ex) {
						LOGGER.error("Ocurrio un error: {}", ex);
						CodeError = "9999";
						DescError = "RECHAZO BANCARIO, POR FAVOR VERIFICA TUS DATOS";
						ex.printStackTrace();
					}
				}
				/*modificacion nueva arquitectura mybatis JCDP revisar LOG */
				compraResponce = responseCompraIAVE_2(bitacora, response, compraiave,
						Double.parseDouble(Float.toString(producto.getPro_monto())), Double.parseDouble(Comision), usuario, CodeError,
						DescError, Resbanco, antadService);
			} catch (ParserConfigurationException ex) {
				LOGGER.error("Ocurrio un error: {}", ex);
				ex.printStackTrace();
			} catch (IOException ex) {
				LOGGER.error("Ocurrio un error: {}", ex);
				ex.printStackTrace();
			} catch (SAXException ex) {
				LOGGER.error("Ocurrio un error: {}", ex);
				ex.printStackTrace();
			} catch (Exception e) {
				LOGGER.error("Ocurrio un error: {}", e);
				e.printStackTrace();
			}
			} // fin de if principal
		
			/*****************************************/
			else {
				LOGGER.info("/*************************** USUARIO INEXISTENTE !!!!!! ********************************/");				
			}
	    }
		respuesta = gson.toJson(compraResponce);
	        new com.addcel.utils.U().Display("Respuesta Recarga IAVE: " + respuesta );
		json = Crypto.aesEncrypt(key, respuesta);
		return json;
	}
	
	public CompraResponce responseCompraIAVE_2(BitacoraVO bitacora, String response, CompraIAVE compra, double Cargo,
			double Comision, UsuarioVO usuario, String CodeError, String DescError, ProsaResponse Resbanco, AntadServiciosService antadService)
					throws ParserConfigurationException, IOException, SAXException,
					ClassNotFoundException {
		CompraResponce compraResponce = new CompraResponce();
                LOGGER.debug("response : " + response);
		try {
			compraResponce.setReferencia(bitacora.getId_bitacora());
			if (response.equals("ERROR")) {
				if (CodeError == null) {
					CodeError = "9999";
					DescError = "RECHAZO PROSA";
				}
				bitacora.setBit_codigo_error(Integer.parseInt(CodeError));
				bitacora.setBit_ticket("Excepcion: " + DescError);
				bitacora.setBit_status(0);
				antadService.actualizaBitacora(bitacora);
				compraResponce.setResultado(0);
                                LOGGER.debug("bitacora.getId_bitacora() : " + bitacora.getId_bitacora());
				compraResponce.setFolio("" + bitacora.getBit_codigo_error());
				compraResponce.setMensaje(antadService.getErrorByClave(compraResponce.getFolio()));
				if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
					compraResponce.setMensaje("Excepcion: " + DescError);
				}
				return compraResponce;
			}
			if (response.equals("ERRORIAVE")) {
				//bitacora.setCodigo_error(Integer.parseInt("881"));
                                LOGGER.debug("bitacora.getId_bitacora() : " + bitacora.getId_bitacora());
				bitacora.setBit_codigo_error(Integer.parseInt("881"));
				bitacora.setBit_ticket("Excepcion: servicio no disponible de I+D al consultar saldo");
				bitacora.setBit_status(0);
				antadService.actualizaBitacora(bitacora);
				compraResponce.setResultado(0);
				compraResponce.setFolio("881");
				compraResponce.setMensaje(antadService.getErrorByClave(compraResponce.getFolio()));
				//JCDP
				if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
					compraResponce.setMensaje("el servicio de recarga I+D NO esta disponible.");
				}
				return compraResponce;
			}
			/*******************/
			if(response.equals("ERRORREVERSE"))
			{
                                LOGGER.debug("bitacora.getId_bitacora() : " + bitacora.getId_bitacora());
				bitacora.setBit_codigo_error(Integer.parseInt(CodeError));
				bitacora.setBit_ticket("Excepcion: " + DescError);
                                LOGGER.debug("Excepcion Carlos Damian: " + DescError);
				bitacora.setBit_status(0);
				antadService.actualizaBitacora(bitacora);
				compraResponce.setResultado(0);
				compraResponce.setFolio(CodeError);
				compraResponce.setMensaje(antadService.getErrorByClave(compraResponce.getFolio()));
				//JCDP
				if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
					compraResponce.setMensaje(DescError);
				}
				return compraResponce;
			}
			/*******************/
			RespuestaIAVE Res = ParceXMLIAVE(response);
			try {
				bitacora.setBit_no_autorizacion(Res.getAutono());
			} catch (Exception e) {
				bitacora.setBit_no_autorizacion("0");
			}
			String ticket = "COMPRA TAG I+D AUTORIZACION: " + Res.getAutono() + " TAG: " + compra.getTarejeta()
					+ " RECARGA DE: " + Cargo + " COMISION: " + Comision;
			bitacora.setBit_ticket(ticket);
			bitacora.setBit_no_autorizacion(bitacora.getBit_no_autorizacion());
			bitacora.setBit_status(1); 
			antadService.actualizaBitacora(bitacora);
			BitacoraiaveVO  bitacoraiave = new BitacoraiaveVO();
			bitacoraiave.setId_bitacora(bitacora.getId_bitacora());
			bitacoraiave.setId_grp(Res.getId_grp());
			bitacoraiave.setCard_number(Res.getCard_number());
			bitacoraiave.setCheck_digit(Res.getCheck_digit());
			bitacoraiave.setLocal_date(Res.getLocal_date());
			bitacoraiave.setAmount(Res.getAmount());
			bitacoraiave.setAutono(Res.getAutono());
			bitacoraiave.setResponsecode(Res.getResponsecode());
			bitacoraiave.setDescriptioncode(Res.getDescriptioncode());
			bitacoraiave.setTrx_no(Res.getTrx_no());
			antadService.actualizaBitacoraIave(bitacoraiave);
			if (Res.getResponsecode().equals("00")) {
				compraResponce.setResultado(1);
				compraResponce.setMensaje("Recarga TAG I+D exitosa. ");
				compraResponce.setFolio("" + Res.getAutono());
				compraResponce.setNumAutorizacion(Resbanco.getAutorizacion());
				compraResponce.setCargo(Cargo);
				compraResponce.setComision(Comision);
				new ParametroDAO().EnviaMailIAVE(usuario.getUsr_nombre(), usuario.geteMail(), antadService.consultaParametros("@MENSAJE_COMPRAIAVE"),
						antadService.consultaParametros("@ASUNTO_COMPRAIAVE"), "", "", "" + Cargo, Resbanco.getAutorizacion(), "" + Comision, "",
						Res.getAutono(), compra.getTarejeta(), String.valueOf(bitacora.getId_bitacora()));
			} else {
				compraResponce.setResultado(Integer.parseInt(Res.getResponsecode()));
				compraResponce.setMensaje(Res.getDescriptioncode());
				compraResponce.setFolio("0");
				bitacora.setBit_codigo_error(Integer.parseInt(Res.getResponsecode()));
				bitacora.setBit_ticket(Res.getDescriptioncode());
				//new BitacoraDAO().updateIAVE(bitacora, false, Res.getDescriptioncode(), 1);
				antadService.actualizaBitacora(bitacora);
			}
			//JCDP
			/*se realiza el insert con my batis*/
			BitacoraprosaVO bitprosa = new BitacoraprosaVO();
			bitprosa.setId_bitacora(Long.parseLong(Integer.toString(bitacora.getId_bitacora())));
			bitprosa.setId_usuario(bitacora.getId_usuario());
			bitprosa.setTarjeta("");
			bitprosa.setTransaccion("");
			bitprosa.setAutorizacion(Resbanco.getAutorizacion());
			bitprosa.setConcepto(bitacora.getBit_concepto());
			bitprosa.setTarjeta(Res.getTrx_no() + "-" + Res.getAutono());
		    bitprosa.setCargo(Long.parseLong(Double.toString(Cargo)) + Long.parseLong(Double.toString(Comision)));
		    bitprosa.setComision(Long.parseLong(Double.toString(Comision)));
		    bitprosa.setCx(compra.getCx());
		    bitprosa.setCy(compra.getCy());
		    antadService.insertaBitacoraProsa(bitprosa);
		} catch (Exception e) {
			LOGGER.debug("ERROR [responseCompraIAVE]: " + e.toString());
			/*bitacora.setCodigo_error(99999999);
			new BitacoraDAO().updateIAVE(bitacora, false, "Excepcion ", 0);*/
			bitacora.setBit_codigo_error(99999999);
			//new BitacoraDAO().updateIAVE(bitacora, false, "Excepcion ", 0);
			bitacora.setBit_ticket(bitacora.getBit_ticket());
			bitacora.setBit_no_autorizacion(bitacora.getBit_no_autorizacion());
			bitacora.setBit_status(0);
			antadService.actualizaBitacora(bitacora);
			compraResponce.setResultado(0);
			compraResponce.setFolio("" + bitacora.getBit_codigo_error());
			//JCDP
			//compraResponce = new MensajeDAO().getErrorByClave(compraResponce);
			compraResponce.setMensaje(antadService.getErrorByClave(compraResponce.getFolio()));
			//JCDP
			if ((compraResponce.getMensaje() == null) || (compraResponce.getMensaje().length() == 0)) {
				compraResponce.setMensaje("error en compra");
			}
		}
		return compraResponce;
	}
	
	public CompraResponce validaReglasNegocio(long idUsuario, String tarjeta, int idProducto, AntadServiciosService antadService) {
		CompraResponce compraResponce = null;
		int numUsuario = 0;
		int montoUsuario = 0;
		int numTarjeta = 0;
		int montoTarjeta = 0;

		Map<String, Object> valoresU = null;
		Map<String, Object> valoresT = null;
		try {
			numUsuario = Integer.parseInt(antadService.consultaParametros("@IMASD_NUM_TRAN_USUARIO"));
			LOGGER.error("numUsuario :" + numUsuario);
			System.out.println("numUsuario : "+numUsuario);
			montoUsuario = Integer.parseInt(antadService.consultaParametros("@IMASD_MONTO_USUARIO"));
			LOGGER.error("montoUsuario :" + montoUsuario);
			System.out.println("montoUsuario"+montoUsuario);
			numTarjeta = Integer.parseInt(antadService.consultaParametros("@IMASD_NUM_TRAN_TARJETA"));
			LOGGER.error("numTarjeta :" + numTarjeta);
			System.out.println("numTarjeta: "+numTarjeta);
			montoTarjeta = Integer.parseInt(antadService.consultaParametros("@IMASD_MONTO_TARJETA"));
			LOGGER.error("montoTarjeta :" + montoTarjeta);
			System.out.println("montoTarjeta: "+montoTarjeta);
			valoresU = antadService.seleccionarRegla(idUsuario, null, "5,8,17", "@IMASD_DIAS_TRAN_USUARIO");
			LOGGER.info("1 : " + valoresU.get("numero"));
			LOGGER.info("2 : " + valoresU.get("monto"));
			LOGGER.info("3 : " + valoresU.get("fechaFin"));
			LOGGER.info("4 : " + valoresU.get("fechaInicio"));
			valoresT = antadService.seleccionarRegla(0L, tarjeta, "5,8,17", "@IMASD_DIAS_TRAN_TARJETA");
			if (((Integer) valoresU.get("numero")).intValue() >= numUsuario) {
				compraResponce = new CompraResponce(0, "920",
						"En el periodo " + valoresU.get("fechaInicio") + " a " + valoresU.get("fechaFin")
								+ " a superado el nÃºmero maximo de transacciones permitidas: " + numUsuario);
			} else if (((Integer) valoresU.get("monto")).intValue() >= montoUsuario) {
				compraResponce = new CompraResponce(0, "921",
						"En el periodo " + valoresU.get("fechaInicio") + " a " + valoresU.get("fechaFin")
								+ " a superado el Monto maximo permitido: $ " + U.formatoImporte(montoUsuario));
			} else if (((Integer) valoresT.get("numero")).intValue() >= numTarjeta) {
				compraResponce = new CompraResponce(0, "922",
						"En el periodo " + valoresT.get("fechaInicio") + " a " + valoresT.get("fechaFin")
								+ " a superado el nÃºmero maximo de transacciones permitidas para una Tarjeta: "
								+ numTarjeta);
			} else if (((Integer) valoresT.get("monto")).intValue() >= montoTarjeta) {
				compraResponce = new CompraResponce(0, "923",
						"En el periodo " + valoresT.get("fechaInicio") + " a " + valoresT.get("fechaFin")
								+ " a superado el Monto maximo permitido para una Tarjeta: $ "
								+ U.formatoImporte(montoTarjeta));
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error al validar Reglas de Negocio.", e);

			compraResponce = new CompraResponce(0, "920", "Ocurrio un error al validar Reglas de Negocio.");
		}
		return compraResponce;
	}
	
	
	public  Map<String, Object> selectRule(long idUsuario, String tarjeta, String idProveedores, String parametro) { 
        Map<String, Object> valores = null;
        //PreparedStatement statement = null;
        //ResultSet resultSet = null;
        StringBuilder query = new StringBuilder();
        LOGGER.debug("idUsuario : " + idUsuario);
        LOGGER.debug("tarjeta : " + tarjeta);
        LOGGER.debug("idProveedores : " + idProveedores);
        LOGGER.debug("parametro : " + parametro);
        try {
            query
                .append("SELECT  COUNT(*) numero,  IFNULL(SUM(bit_cargo  ),0) monto,  ")
                .append("CURRENT_DATE() fechaFin,   ")
				.append("DATE_ADD(CURRENT_DATE(), INTERVAL - (  ")
				.append("	SELECT valor FROM mobilecard.t_parametros  ")
				.append("	WHERE clave = ?  ")
				.append(") DAY) fechaInicio  ")
                .append("from t_bitacora ")
                .append("where id_proveedor in (").append(idProveedores).append(") ");
            if(idUsuario != 0){
                query.append("and id_usuario = ? ");
            }else if( tarjeta != null){
                query.append("and tarjeta_compra = ? ");
            }
            query
                .append("AND bit_status = 1 ")
                .append("and DATE(bit_hora)  between   ")
                .append("	DATE_ADD(CURRENT_DATE(), INTERVAL - ( ")
                .append("			SELECT valor FROM mobilecard.t_parametros ")
                .append("			WHERE clave = ? ")
                .append( "		) DAY) AND CURRENT_DATE() ")
                .append( "order by 1 desc ");

            /*if(idUsuario != 0){
                logger.debug("idUsuario: " + idUsuario);
            }
            if(tarjeta != null){
                logger.debug("tarjeta: " + tarjeta);
            }*/
            //logger.debug("idProveedores: " + idProveedores);
            //logger.debug("parametro: " + parametro);
            LOGGER.debug("Query selectRule: " + query);
            
            //statement = connect().prepareStatement( query.toString() );
            
            //statement.setString(1, parametro);
            if(idUsuario != 0){
                //statement.setLong(2, idUsuario);
            }else if( tarjeta != null){
                //statement.setString(2, tarjeta);
            }
            //statement.setString(3, parametro);
            //resultSet = statement.executeQuery();
            //if(resultSet.next()){
                valores = new HashMap<String, Object>();
                //valores.put("numero", resultSet.getInt("numero"));
                //valores.put("monto", resultSet.getInt("monto"));
                //valores.put("fechaInicio", resultSet.getString("fechaInicio"));
                //valores.put("fechaFin", resultSet.getString("fechaFin"));
                if(idUsuario != 0){
                	LOGGER.debug("idUsuario: " + idUsuario + ", \nData: " + valores);
                }
                if(tarjeta != null){
                	LOGGER.debug("tarjeta: " + tarjeta + ", \nData: " + valores);
                }
            //}
        } catch (Exception ex) {
        	LOGGER.error("Ocurrio un error: {}",ex);
        }finally {
            //closeConPrepStResSet(statement, resultSet);
        }
    return  valores;
    }
	
	public boolean esTuTag(String idProducto, AntadServiciosService antadService) {
		String productosTuTag = null;
		String[] productosTuTagArr = null;
		boolean resultado = false;
		try {
			productosTuTag = antadService.consultaParametros("@PRODUCTOS_TUTAG");
			productosTuTagArr = productosTuTag.split("\\|");
			for (String s : productosTuTagArr) {
				if (s.equalsIgnoreCase(idProducto)) {
					resultado = true;
					break;
				}
			}
		} catch (Exception e) {
			LOGGER.error("Ocurrio un error: {}", e);
		}
		return resultado;
	}
	
	public RespuestaIAVE ParceXMLIAVE(String cadena) {
		String sXML = cadena;
		RespuestaIAVE oR = new RespuestaIAVE();
		try {
			//File file = new File("response.xml");
			//BufferedWriter bw = new BufferedWriter(new FileWriter(file));
			sXML = sXML.replace("<![CDATA[", "");
			sXML = sXML.replace("]]>", "");
			sXML = sXML.replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", "");
			sXML = sXML.replace("á", "a");
			sXML = sXML.replace("é", "e");
			sXML = sXML.replace("í", "i");
			sXML = sXML.replace("ó", "o");
			sXML = sXML.replace("ú", "u");

			sXML = sXML.replace("Á", "A");
			sXML = sXML.replace("É", "E");
			sXML = sXML.replace("í", "I");
			sXML = sXML.replace("Ó", "O");
			sXML = sXML.replace("Ú", "U");

			//bw.write(sXML);
			//bw.close();
			InputStream is = new ByteArrayInputStream(sXML.getBytes());
					
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();

			Document doc = db.parse(is); //file
			doc.getDocumentElement().normalize();

			NodeList Res = doc.getElementsByTagName("ReloadResponse");
			Node firstPersonNode1 = Res.item(0);
			Node firstPersonNode2 = Res.item(0);
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("ID_GRP");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setId_grp(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode2.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode2;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("CARD_NUMBER");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setCard_number(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("CHECK_DIGIT");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setCheck_digit(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("LOCAL_DATE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setLocal_date(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("AMOUNT");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setAmount(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("TRX_NO");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setTrx_no(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("AUTONO");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setAutono(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("RESPONSECODE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setResponsecode(textFNList.item(0).getNodeValue().trim());
				}
			}
			if (firstPersonNode1.getNodeType() == 1) {
				Element firstPersonElement = (Element) firstPersonNode1;
				NodeList firstNameList = firstPersonElement.getElementsByTagName("DESCRIPTIONCODE");
				Element firstNameElement = (Element) firstNameList.item(0);
				NodeList textFNList = firstNameElement.getChildNodes();
				if (textFNList.item(0) != null) {
					oR.setDescriptioncode(textFNList.item(0).getNodeValue().trim());
				}
			}
			
		} catch (Exception ex) {
			LOGGER.debug("ERROR [PARCEXML IAVE: ]: " + ex.toString());
			oR.setResponsecode("9999");
			oR.setDescriptioncode("ERROR- Banco (1)");
			
		} finally {
			return oR;
		}
	}
    /*nuevo Request mapping con la nueva arquitectura de mybatis y Spring MVC*/
	@RequestMapping(value = PATH_INSERTA_USUARIO, method=RequestMethod.POST)
	public @ResponseBody String insertaUsuario(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
         String key = "1234567890ABCDEF0123456789ABCDEF";
            Respuesta respuesta = new Respuesta();
            String resp = null;
            UsuarioVO usuario  = new UsuarioVO();

            String json;
            
            if(jsonEnc == null ) {
			LOGGER.info("parameter json null " );
		}
		if(jsonEnc.equals("")){
		    json = (String) jsonEnc.toString();
		    LOGGER.info("Dentro de If en insertaUsuario " );
		}else{
		    json = (String) jsonEnc.toString();
		    LOGGER.info("Dentro de else en insertaUsuario " );
		}

		    LOGGER.info("Inicio INSERT USUARIO... " );
            LOGGER.info("CADENA userInsert " + json);
            //String p=usuario.getPassword();
            String p=usuario.getUsr_pwd();

            String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
            key = Cad[0];
            json = Cad[1];
            p = Cad[2];

            Gson gson = new Gson();
            Type collectionType = new TypeToken<UsuarioVO>(){}.getType();
            usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
            usuario.setUsr_pwd(p);

            LOGGER.info("REGISTRO USUARIO" );
            LOGGER.info("Usuario: " + usuario.getUsr_login() );
            LOGGER.info("Tipo: " + usuario.getTipo() );
            LOGGER.info("Software: " + usuario.getSoftware());
            LOGGER.info("Modelo: " + usuario.getModelo());
            LOGGER.info("Imei: " + usuario.getImei());
            LOGGER.info("NOMBRE: " + usuario.getUsr_nombre() + " " + usuario.getUsr_apellido()+ " " + usuario.getUsr_materno());

            if(!existeUsuario(usuario, antadService)){
            	LOGGER.info("Usuario no existe : " + usuario.getUsr_login() );    
                if(ExisteMail(usuario, antadService)){
                   respuesta.setResultado(3);
                   respuesta.setMensaje("El Correo Electronico ya fue asignado a otro usuario. Intente de nuevo.");

                }else if(ExisteT(usuario, antadService)){
                    respuesta.setResultado(4);
                    respuesta.setMensaje("La tarjeta proporcionada ya esta asignada a otro usuario. Intente de nuevo.");

                }else if(ExisteIMEI(usuario, antadService)){
                    respuesta.setResultado(5);
                    respuesta.setMensaje("No es posible registrar el usuario.");
                    LOGGER.info("ERR-00 -> El IMEI ya se encuentra registrado en la base de datos. IMEI= " + usuario.getImei() + "   Usuario = " + usuario.getUsr_login());

                }else if(ExisteTelefono(usuario, antadService)){
                    respuesta.setResultado(5);
                    respuesta.setMensaje("El telefono ya esta asignado a otro usuario. Intente de nuevo.");

                }else if(addUsuario(usuario,antadService)){
                    respuesta.setResultado(1);
                    if(usuario.getTipo().toUpperCase().equals("IPAD")  || usuario.getTipo().toUpperCase().equals("IPOD") ){
                          respuesta.setMensaje("EL sistema te envio un EMAIL con la clave de acceso para completar tu registro.");
                    }else{
                          respuesta.setMensaje("EL sistema te envio un SMS o un EMAIL con la clave de acceso para completar tu registro.");
                    }              
                }else{
                   respuesta.setResultado(2);
                   respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
                }

            }else{
                respuesta.setResultado(2);
                respuesta.setMensaje("El usuario ya existe");
            }
              resp = gson.toJson(respuesta);
              new com.addcel.utils.U().Display("Respuesta UserInsert: " + resp );
              resp = Crypto.aesEncrypt(key, resp);
              return resp;
         }
        
        public boolean existeUsuario(UsuarioVO usuario, AntadServiciosService antadService) {
        boolean flag = false;
        UsuarioVO usuarioaux = new UsuarioVO();
        try {
            usuarioaux = antadService.consultaUsuarioById(usuario.getUsr_login());
            if (usuarioaux != null) {
                LOGGER.error("Usuario Existe");
                flag = true;
            }
            if(!flag){
                LOGGER.error("Usuario No Existe");
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en metodo existeUsuario: {}",ex);
        } finally {
            LOGGER.error("Se ejecuto metodo existeUsuario: {}");
        }
        return flag;
    }
    
    public boolean ExisteMail(UsuarioVO usuario, AntadServiciosService antadService) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = antadService.consultaUsuarioByEmail(usuario.geteMail());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteMail {}",ex);
        } finally {
            LOGGER.error("Validacion de e-mail de usuario {}");
        }
        return flag;
    }    
    
    public boolean ExisteT(UsuarioVO usuario, AntadServiciosService antadService) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = antadService.consultaUsuarioByT(usuario.getUsr_tdc_numero());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteT {}",ex);
        } finally {
            LOGGER.error("Validacion de tdc de usuario {}");
        }
        return flag;
    }    
    
    public boolean ExisteIMEI(UsuarioVO usuario, AntadServiciosService antadService) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = antadService.consultaUsuarioByImei(usuario.getImei());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteIMEI {}",ex);
        } finally {
            LOGGER.error("Validacion de imei de usuario {}");
        }
        return flag;
    }    
    
    public boolean ExisteTelefono(UsuarioVO usuario, AntadServiciosService antadService) {
        boolean flag = false;
        UsuarioVO usuaux = new UsuarioVO();
        try {
            usuaux = antadService.consultaUsuarioByTelefono(usuario.getUsr_telefono());
            if (usuaux != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteTelefono {}",ex);
        } finally {
            LOGGER.error("Validacion de telefono de usuario {}");
        }
        return flag;
    }    
    
    public boolean addUsuario(UsuarioVO usuario, AntadServiciosService antadService) {
        boolean flag = false;
        try {
            //flag = antadService.addUser(usuario);
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error ExisteTelefono {}",ex);
        } finally {
            LOGGER.error("Validacion de telefono de usuario {}");
        }
        return flag;
    }
    
    @RequestMapping(value = PATH_USUARIO_LOGIN, method=RequestMethod.POST)
	public @ResponseBody String usuarioLogin(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	LOGGER.info("**********************************   LOGIN");
        String key = "1234567890ABCDEF0123456789ABCDEF";
        String resp = null;
        Respuesta respuesta = new Respuesta();
        Usuario usuario  = null;
        String json;
        if (jsonEnc == null) {
        	LOGGER.info("parameter json null ");
        }
        if (jsonEnc.equals("")) {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de If en usuarioLogin ");
        } else {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de else en usuarioLogin ");
        }
        LOGGER.info("CADENA userLogin: " + json);

        Gson gson = new Gson();
        try {
            String[] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
            key = Cad[0];
            json = Cad[1];

            Type collectionType = new TypeToken<Usuario>() {
            }.getType();
            usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
            LOGGER.info("usuario" + usuario.getUsuario());
            LOGGER.info("usuario password" + usuario.getPassword());
            LOGGER.info("usuario passwordS" + usuario.getPasswordS());
        } catch (Exception e) {
        	LOGGER.error("ERROR al desifrar JSON: " + e.getMessage());
            respuesta.setResultado(2);
            respuesta.setMensaje("Usuario Invalido. Verifique el usuario y/o password");
        }
        if (usuario != null) {
        	LOGGER.info("CADENA USUARIO 1: " + usuario.getLogin());
            
            usuario.setPassword(Crypto.aesEncrypt(key, usuario.getPassword()));

            LOGGER.info("Login: " + usuario.getUsuario());
            //new com.addcel.utils.U().Display("CADENA USUARIO 2, Usuario:" + usuario.getNombre() + " *** Password: " + usuario.getPassword());
            LOGGER.info("CADENA USUARIO 2, Usuario:" + usuario.getNombre() + " *** Password: " + usuario.getPassword());

            UsuarioVO RU = antadService.consultaUsuarioById(usuario.getLogin());
            if (RU != null && RU.getUsr_nombre() != null) {
                //new com.addcel.utils.U().Display("CADENA USUARIO VALIDO " + usuario.getNombre());
                if (RU.getId_usr_status() == 0) {
                    respuesta.setResultado(3);
                    respuesta.setMensaje("Usuario Inactivo. Consulte con el Administrador");
                } else if (RU.getId_usr_status() == 99) {
                    respuesta.setResultado(99);
                    respuesta.setMensaje("Modifique su Informacion por seguridad.|" + RU.getUsr_login());
                } else if (RU.getId_usr_status() == 98) {
                    respuesta.setResultado(98);
                    respuesta.setMensaje("Modifique su clave de acceso por seguridad.|" + RU.getUsr_login());
                } else {
                    respuesta.setResultado(1);
                    respuesta.setMensaje("Usuario Valido|" + RU.getId_usuario());
                }
            } else {
                respuesta.setResultado(2);
                respuesta.setMensaje("Usuario Invalido. Verifique el usuario y/o password");
            }
        }
        resp = gson.toJson(respuesta);
        new com.addcel.utils.U().Display("Respuesta usuarioLogin: " + resp);
        resp = Crypto.aesEncrypt(key, resp);
        return resp;
    }
    
    @RequestMapping(value = PATH_USUARIO_PASSWORD_UPDATE, method=RequestMethod.POST)
	public @ResponseBody String usuarioPasswordUpdate(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	LOGGER.info("**********************************  PASSWORD UPDATE");
        String key = "1234567890ABCDEF0123456789ABCDEF";
        String resp = null;
        Respuesta respuesta = new Respuesta();
        UserPasswordUpdate  userPasswordUpdate   = new UserPasswordUpdate ();
        String json;
        if (jsonEnc == null) {
        	LOGGER.info("parameter json null ");
        } 
        if (jsonEnc.equals("")) {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de If en insertaUsuario ");
        } else {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de else en insertaUsuario ");
        }
        LOGGER.info("CADENA json update password: " + json);
        String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
        key = Cad[0];
        json = Cad[1];
        
        Gson gson = new Gson();
        Type collectionType = new TypeToken<UserPasswordUpdate>() {
        }.getType();
        userPasswordUpdate = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
        UsuarioVO usuario = new UsuarioVO();
        usuario.setUsr_login(userPasswordUpdate.getLogin());
        
        usuario.setUsr_pwd(Crypto.aesEncrypt(key,  userPasswordUpdate.getPassword() ));
        String key2=new com.ironbit.addcell.integration.ParametroDAO().parsePass(userPasswordUpdate.getNewPassword());
        String us = usuario.getUsr_login();
        String p = userPasswordUpdate.getNewPassword();

        userPasswordUpdate.setNewPassword(Crypto.aesEncrypt(key2, userPasswordUpdate.getNewPassword()));
        
        usuario = antadService.consultaUsuarioById(usuario.getUsr_login());
        
        LOGGER.info("CADENA LogIn " + us);
        
        if(p.length() < 8 ){   
            respuesta.setResultado(3);
            respuesta.setMensaje("La nueva contraseña debe tener por lo menos 8 caracteres.");
        }else{
            if (antadService.newValidateUsuario(usuario)) {

                if (antadService.updatePassword(usuario)) {
                    respuesta.setResultado(1);
                    respuesta.setMensaje("Password actualizado");
                } else {
                    respuesta.setResultado(2);
                    respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
                }
            } else {
                respuesta.setResultado(2);
                respuesta.setMensaje("Error en Password");
            }
        }
        resp = gson.toJson(respuesta);
        LOGGER.info("Respuesta updatePassword: " + resp);
        resp = Crypto.aesEncrypt(key, resp);
        return resp;
    }
    
    @RequestMapping(value = PATH_USUARIO_UPDATE, method=RequestMethod.POST)
	public @ResponseBody String usuarioUpdate(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        String resp = null;
        String key = "1234567890ABCDEF0123456789ABCDEF";
        Respuesta respuesta = new Respuesta();
        UsuarioVO usuario  = new UsuarioVO();
        String json;
        if (jsonEnc == null) {
        	LOGGER.info("parameter json null ");
        } 
        if (jsonEnc.equals("")) {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de If en insertaUsuario ");
        } else {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de else en insertaUsuario ");
        }
        LOGGER.info("*********************     USUARIO Update... ");

        LOGGER.info("CADENA userUpdate: " + json);
        
        String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
        key = Cad[0];
        json = Cad[1];Gson gson = new Gson();
        Type collectionType = new TypeToken<UsuarioVO>(){}.getType();
        usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);

        usuario.setUsr_pwd(Crypto.aesEncrypt(key,  usuario.getUsr_pwd() ));

        LOGGER.info("CADENA LOGIN " + usuario.getUsr_login());
        LOGGER.info("CADENA IDUSUARIO " + usuario.getId_usuario());
        
        if(antadService.ExisteMailUp(usuario.geteMail(),usuario.getUsr_login() ))
	{
	      respuesta.setResultado(3);
	      respuesta.setMensaje("El Correo Electronico ya fue asignado a otro usuario. Intente de nuevo.");
	}
	else if(antadService.ExisteTUp(usuario.getUsr_tdc_numero(),usuario.getUsr_login() ))
	{
	    respuesta.setResultado(4);
	    respuesta.setMensaje("La tarjeta proporcionada ya esta asignada a otro usuario. Intente de nuevo.");
	}
	else if(antadService.update(usuario))
	{
	    respuesta.setResultado(1);
	    respuesta.setMensaje("El registro ha sido actualizado satisfactoriamente");
	}else
	{
	    respuesta.setResultado(2);
	    respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
	}
        resp = gson.toJson(respuesta);
        LOGGER.info("Respuesta updatePassword: " + resp);
        resp = Crypto.aesEncrypt(key, resp);
        return resp;
    }
    
    private String Ceros(String cad, int NumCeros) {
		String tmp = "00000000000000000000000000000";
		return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
	}
    
    	private String executePurchaseIAVERecarga(String folio, String tag, String monto, String DV, String idcom,
			String idgrp, String ActivaTrans) {
		String Res = null;
		try {
			LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 1");
			Calendar mC = Calendar.getInstance();

			String wfecha = Ceros(new StringBuilder().append("").append(mC.get(5)).toString(), 2) + "/"
					+ Ceros(new StringBuilder().append("").append(mC.get(2) + 1).toString(), 2) + "/" + mC.get(1) + " "
					+ Ceros(new StringBuilder().append("").append(mC.get(11)).toString(), 2) + ":"
					+ Ceros(new StringBuilder().append("").append(mC.get(12)).toString(), 2) + ":"
					+ Ceros(new StringBuilder().append("").append(mC.get(13)).toString(), 2);

			String prefijo = "";
			String puntos = "";
			if (tag.length() != 11) {
				while (tag.length() < 8) {
					tag = "0" + tag;
				}
				if ((tag.length() == 8) && (Long.parseLong(tag) >= 20000000L)) {
					prefijo = "IMDM";
				}
				if ((tag.length() == 8) && (Long.parseLong(tag) < 20000000L)) {
					prefijo = "CPFI";
				}
				if (tag.length() == 8) {
					puntos = "..";
				}
			}
			LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 2");
			StringBuffer pXML = new StringBuffer().append("<ReloadRequest>").append("  <ID_COM>").append(idcom)
					.append("</ID_COM>").append("  <ID_GRP>").append(idgrp).append("</ID_GRP>")
					.append("  <CARD_NUMBER>").append(prefijo).append(tag).append(puntos).append("</CARD_NUMBER>")
					.append(" <CHECK_DIGIT>").append(DV).append("</CHECK_DIGIT>").append("  <LOCAL_DATE>")
					.append(wfecha).append("</LOCAL_DATE>").append("  <AMOUNT>").append(monto).append("</AMOUNT>")
					.append(" <TRX_NO>").append(folio).append("</TRX_NO>").append("</ReloadRequest>");

			LOGGER.debug("MPX COMPRA IAVE : " + pXML);
			LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 3");
			if (ActivaTrans.equals("1")) {
				LOGGER.info("Dentro de executePurchaseIAVERecarga Entro 4");
				WsPrepagoRecargasSoapProxy proxy = new WsPrepagoRecargasSoapProxy();
				Res = proxy.reload(pXML.toString());
			} else {
				LOGGER.debug("Ambiente QA, se genera una respuesta");
				Res =  new U().XMLTestIAVE();
			}
			
			return Res;
		} catch (Exception ex) {
			LOGGER.error("MPX COMPRA IAVE ERROR: " + ex.toString());
		}
		return "";
	}
    @RequestMapping(value = PATH_GET_ESTADOS, method=RequestMethod.POST)
	public @ResponseBody String getEstados(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
        String key = "1234567890ABCDEF0123456789ABCDEF";
        Gson gson = new Gson();
        List<EstadosVO>  estado = null;
        estado = antadService.getEstados();
        String jsonOutput = gson.toJson(estado);
        Crypto.aesEncrypt(key, jsonOutput);
        return Crypto.aesEncrypt(key, jsonOutput);
    }
    @RequestMapping(value = PATH_GETUSER_DATA, method=RequestMethod.POST)
	public @ResponseBody String getUserData(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	String key = "1234567890ABCDEF0123456789ABCDEF";
    	UsuarioVO usuario  = new UsuarioVO();
    	String json;
    	if (jsonEnc == null) {
    		LOGGER.info("parameter json null ");
        } 
        if (jsonEnc.equals("")) {
            json = (String) jsonEnc.toString();
            //LOGGER.info("Dentro de If  ");
        } else {
            json = (String) jsonEnc.toString();
            //LOGGER.info("Dentro de else ");
        }
        LOGGER.info("CADENA getUserData " + json);
        String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
        key = Cad[0];
        json = Cad[1];
        Gson gson = new Gson();
        Type collectionType = new TypeToken<UsuarioVO>(){}.getType();
        usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);

        usuario.setUsr_pwd(Crypto.aesEncrypt(key,  usuario.getUsr_pwd()));
        
        usuario = antadService.obtenerUsuario(usuario);
        String jsonOutput = gson.toJson(usuario);
        
    	return Crypto.aesEncrypt(key, jsonOutput);
    }
    
    @RequestMapping(value = PATH_GETTIPOS_TARJETAS, method=RequestMethod.POST)
	public @ResponseBody String getTiposTarjetas(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	String key = "1234567890ABCDEF0123456789ABCDEF";
    	Gson gson = new Gson();
    	TTipoTarjetaVO tarjeta = new TTipoTarjetaVO();
    	tarjeta.setTarjetas(antadService.getTiposTarjeta());
    	String jsonOutput = gson.toJson(tarjeta);
    	return Crypto.aesEncrypt(key, jsonOutput);
    }
    
    @RequestMapping(value = PATH_GET_BANCOS, method=RequestMethod.POST)
	public @ResponseBody String getBancos(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	String key = "1234567890ABCDEF0123456789ABCDEF";
    	Gson gson = new Gson();
    	TBancoVO bancos = new TBancoVO();
    	bancos.setBancos(antadService.getBancos());
    	String jsonOutput = gson.toJson(bancos);
    	return Crypto.aesEncrypt(key, jsonOutput);
    }
    
    @RequestMapping(value = PATH_GET_PROVIDERS, method=RequestMethod.POST)
	public @ResponseBody String getProveedores(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	String key = "1234567890ABCDEF0123456789ABCDEF";
    	CatCarriesVO wProveedor  = new CatCarriesVO();
    	String json;

    	String jsonOutput  = "";
    	
    	LOGGER.info("CADENA getProviders " + jsonEnc);
    	        
    	if( jsonEnc == null || jsonEnc.equals("")){
    	    Gson gson = new Gson();
    	    wProveedor.setCarries(antadService.getProveedores());
    	    jsonOutput = gson.toJson(wProveedor);    	    
    	}
    	return Crypto.aesEncrypt(key, jsonOutput);
    }
    
    @RequestMapping(value = PATH_GET_PRODUCTS, method=RequestMethod.POST)
	public @ResponseBody String getProductos(@RequestParam(REQ_PARAM_JSON)  String jsonEnc) {
    	String key = "1234567890ABCDEF0123456789ABCDEF";
    	//ProductoVO producto = new ProductoVO();
    	Producto producto = new Producto();
    	String json;
    	Gson gson = new Gson();
    	if (jsonEnc == null) {
    		LOGGER.info("parameter json null ");
        } 
        if (jsonEnc.equals("")) {
            json = (String) jsonEnc.toString();
            //LOGGER.info("Dentro de If  ");
        } else {
            json = (String) jsonEnc.toString();
            LOGGER.info("Dentro de else en end point adc_getPoducts "+json);
        }
        Type collectionType = new TypeToken<Producto>(){}.getType();
        new com.addcel.utils.U().Display("llega 1 ");
        producto = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
        LOGGER.info("id_producto " + producto.getClave());
        LOGGER.info("pro_clave " + producto.getClaveWS());
        LOGGER.info("pro_monto " + producto.getMonto());
        LOGGER.info("pro_path" + producto.getPath());
        LOGGER.info("id_proveedor " + producto.getProveedor());
        LOGGER.info("nombre " + producto.getNombre());
        ProveedorVO  prov = new ProveedorVO();
        LOGGER.info("llega 3 ");
        prov = antadService.consultaProveedorByClaveWS(producto.getProveedor());
        LOGGER.info("llega 4 ");
        Contenedor contenedor = new Contenedor();
        contenedor.setProductos(antadService.getProductosByProveedor(prov.getId_proveedor()));
        LOGGER.info("llega 5 ");
        String jsonOutput = gson.toJson(contenedor);
        LOGGER.info("llega 6 ");
    	return Crypto.aesEncrypt(key, jsonOutput);
    }
    
}
