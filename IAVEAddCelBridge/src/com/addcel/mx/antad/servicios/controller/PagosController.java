package com.addcel.mx.antad.servicios.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.antad.servicios.services.PagosService;

@Controller
public class PagosController {

	@Autowired
	PagosService pagoService;
	
	@RequestMapping(value = "/pagoProsa3DS", method = RequestMethod.POST)
	public ModelAndView pagoServicios3DS(@RequestParam("json") String jsonEnc) {
		
		long idUsuario = 0; 
		double monto = 0; 
		long idBitacora = 0;
		
		return pagoService.pagoServicios3DS(idUsuario, monto, idBitacora);
	}
	
	
	@RequestMapping(value = "/comercio-con", method = RequestMethod.POST)
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest) {
		
		return pagoService.procesaRespuestaRecargaProsa(EM_Response,
				EM_Total, EM_OrderID, EM_Merchant, EM_Store,
				EM_Term, EM_RefNum, EM_Auth, EM_Digest);	
	}
	
}
