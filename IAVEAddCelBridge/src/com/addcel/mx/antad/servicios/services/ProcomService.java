package com.addcel.mx.antad.servicios.services;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.mx.antad.servicios.model.vo.ProcomVO;
import com.addcel.mx.antad.servicios.model.vo.TransactionProcomVO;
import com.addcel.mx.antad.servicios.utils.Constantes;

public class ProcomService {
	private static final Logger logger = LoggerFactory.getLogger(ProcomService.class);
	

	/*
	 * idTramite Tipo de pago: 1 Recarga, 2 Servicios
	 */

	public ProcomVO comercioFin(String user, String referencia, String total, String idTramite, String merchant,
			String monto, String idServicio, String campos) {
		String varTotal = formatoMontoProsa(String.valueOf(total));
		// cambiar por datos correctos
		String digest = digest(
				new StringBuilder(Constantes.MERCHANT).append(Constantes.STORE).append(Constantes.TERM)
						.append(varTotal).append(Constantes.CURRENCY).append(referencia).toString());
		logger.info("Digest DiestelService: {}", digest);

		ProcomVO procomObj = new ProcomVO(varTotal, Constantes.CURRENCY, Constantes.ADDRESS, referencia,
				Constantes.MERCHANT, Constantes.STORE, Constantes.TERM, digest, Constantes.URLBACK,
				String.valueOf(user), idTramite, monto, idServicio, campos);

		logger.debug("User : {}", user);
		logger.debug("Referencia: {}", referencia);
		logger.debug("IdTramite: {}", idTramite);

		return procomObj;
	}
	
	public ProcomVO comercioFin(long idBitacora, double monto) {
		String varTotal = formatoMontoProsa(Double.toString(monto));
		String referencia = Long.toString(idBitacora);
		
		String digest = digest(
				new StringBuilder(Constantes.MERCHANT).append(Constantes.STORE).append(Constantes.TERM)
						.append(varTotal).append(Constantes.CURRENCY).append(referencia).toString());
		
		logger.info("Digest DiestelService: {}", digest);

		ProcomVO procomObj = new ProcomVO(varTotal, Constantes.CURRENCY, Constantes.ADDRESS, referencia,
				Constantes.MERCHANT, Constantes.STORE, Constantes.TERM, digest, Constantes.URLBACK);
		
		logger.debug("Referencia : {}", referencia);
		

		return procomObj;
	}

	private String formatoMontoProsa(String monto) {
		String varTotal = "000";
		String pesos = null;
		String centavos = null;
		if (monto.contains(".")) {
			pesos = monto.substring(0, monto.indexOf("."));
			centavos = monto.substring(monto.indexOf(".") + 1, monto.length());
			if (centavos.length() < 2) {
				centavos = centavos.concat("0");
			} else {
				centavos = centavos.substring(0, 2);
			}
			varTotal = pesos + centavos;
		} else {
			varTotal = monto.concat("00");
		}
		logger.info("Monto a cobrar 3dSecure: " + varTotal);
		return varTotal;
	}

	private String digest(String text) {
		String digest = "";
		BigInteger bigIntDgst = null;
		
		try {
			logger.info("DIGEST cadena: " + text);
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			md.update(text.getBytes(), 0, text.length());
			bigIntDgst = new BigInteger(1, md.digest());
			digest = bigIntDgst.toString(16);
			
		} catch (NoSuchAlgorithmException e) {
			logger.info("Error al encriptar - digest", e);
		}
		logger.info("DIGEST calculado: " + digest);
		return digest;
	}

	public String digestEnvioProsa(String varMerchant, String varStore, String varTerm, String varTotal,
			String varCurrency, String varOrderId) {
		logger.info("Inicia calculo DIGEST: ");
		logger.info("varMerchant: " + varMerchant);
		logger.info("varStore: " + varStore);
		logger.info("varTerm: " + varTerm);
		logger.info("varTotal: " + varTotal);
		logger.info("varCurrency: " + varCurrency);
		logger.info("varOrderId: " + varOrderId);
		return digest(varMerchant + varStore + varTerm + varTotal + varCurrency + varOrderId);
	}

	public String digestRegresoProsa(TransactionProcomVO transactionProcomV) {

		StringBuffer digestCad = new StringBuffer().append(transactionProcomV.getEmTotal())
				.append(transactionProcomV.getEmOrderID()).append(transactionProcomV.getEmMerchant())
				.append(transactionProcomV.getEmStore()).append(transactionProcomV.getEmTerm())
				.append(transactionProcomV.getEmRefNum()).append("-").append(transactionProcomV.getEmAuth());

		return digest(digestCad.toString());
	}

}
