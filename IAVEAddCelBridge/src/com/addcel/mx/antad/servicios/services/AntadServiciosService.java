package com.addcel.mx.antad.servicios.services;

import static com.addcel.mx.antad.servicios.utils.Constantes.FORMATO_FECHA_ENCRIPT;
import static com.addcel.mx.antad.servicios.utils.Constantes.JSON_ERROR_INESPERADO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_SERVICIOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_SELECCIONAR_REGLAS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTAESTADOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_CARRIES;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTATIPOSTARJETAS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_BANCOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_PARAMETROS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_CARRIES;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_TELEFONO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_ACTUALIZA_BITACORA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_COMISION;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_BANCOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTAFOLIO_IAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTATIPOSTARJETAS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_IMEI;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_TELEFONO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_VALIDA_T;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTAESTADOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_AGREGAR_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_PROVEEDOR;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_INSERTA_BITACORAIAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_OBTENER_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_REALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_SELECCIONAR_REGLAS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_ACTUALIZAR_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_EMAIL;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTAFOLIO_BANCO_IAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_DESC_ERROR;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_CONSULTA_USUARIO_TDC;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_ACTUALIZA_BITACORAIAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_ERROR_INSERTA_BITACORA_PROSA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_TDC;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_BANCOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_CARRIES;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_SERVICIOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_PARAMETROS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_ACTUALIZA_BITACORA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_COMISION;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_ADD_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_REALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_VALIDA_T;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_TDC;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_PROVEEDOR;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTAFOLIO_IAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_ACTUALIZA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTAFOLIO_BANCO_IAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_DESC_ERROR;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_EMAIL;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_ACTUALIZA_BITACORAIAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_INSERTA_BITACORA_PROSA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_SERVICIOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_PARAMETROS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_ACTUALIZA_BITACORA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_COMISION;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_REGLAS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PREALIZA_CONSULTA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_PROCESA_PAGO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_VALIDA_T;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_TIPOSTARJETAS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_ACTUALIZA_USUARIO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_BITACORAIAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_FOLIO_IAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_FOLIO_BANCO_IAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_ESTADOS;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_DESC_ERROR;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_ACTUALIZACION_BITACORAIAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_INSERTA_BITACORA_PROSA;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_PROVEEDOR;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_EMAIL;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_IMEI;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_RESPUESTA_CONSULTA_USUARIO_TELEFONO;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_INSERTA_BITACORAIAVE;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_PROCESO_CONSULTA_USUARIO_IMEI;
import static com.addcel.mx.antad.servicios.utils.Constantes.LOG_SERVICE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.addcel.mx.antad.servicios.client.Consulta;
import com.addcel.mx.antad.servicios.client.ConsultaResponse;
import com.addcel.mx.antad.servicios.client.ObjectFactory;
import com.addcel.mx.antad.servicios.client.ProcesaAutorizacion;
import com.addcel.mx.antad.servicios.client.ProcesaAutorizacionResponse;
import com.addcel.mx.antad.servicios.model.mapper.AntadServiciosMapper;
import com.addcel.mx.antad.servicios.model.vo.BitacoraiaveVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraprosaVO;
import com.addcel.mx.antad.servicios.model.vo.BloqImeiVO;
import com.addcel.mx.antad.servicios.model.vo.CatCarriesVO;
import com.addcel.mx.antad.servicios.model.vo.EndPointsRequest;
import com.addcel.mx.antad.servicios.model.vo.EndPointsResponse;
import com.addcel.mx.antad.servicios.model.vo.ServiciosAntadRequest;
import com.addcel.mx.antad.servicios.model.vo.ServiciosAntadResponse;
import com.addcel.mx.antad.servicios.model.vo.TBancoVO;
import com.addcel.mx.antad.servicios.model.vo.TBloVO;
import com.addcel.mx.antad.servicios.model.vo.TTipoTarjetaVO;
import com.addcel.mx.antad.servicios.utils.UtilsService;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.model.vo.ProductoVO;
import com.addcel.mx.antad.servicios.model.vo.ProveedorVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.EstadosVO;
import com.addcel.mx.antad.servicios.model.vo.OzekimessageoutVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioTagsVO;
import com.addcel.utils.AddcelCrypto;
import com.ironbit.addcell.bean.Producto;
import com.ironbit.addcell.integration.ParametroDAO;

import crypto.Crypto;

import java.net.URLEncoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Random;

@Service
public class AntadServiciosService {

	private static final Logger LOGGER = LoggerFactory.getLogger(AntadServiciosService.class);

	private static final SimpleDateFormat SDF = new SimpleDateFormat(FORMATO_FECHA_ENCRIPT);
	
	private static final String TelefonoServicio="5525963513";

	public static String setSMS(String Telefono){
	    return Crypto.aesEncrypt(parsePass(TelefonoServicio),  Telefono);    
	} 
	
	@Autowired
	private AntadServiciosMapper mapper;
	
	@Autowired
	private UtilsService jsonUtils;
	
	@Autowired
    @Qualifier("webServiceAntadBridge")
    private transient WebServiceTemplate webServiceTemplate;
	
	public String consultaServicios(String json) {
		List<ServiciosAntadResponse> serviciosAntadList = null;
		ServiciosAntadRequest request = null;
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SERVICIOS + json);
			request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			serviciosAntadList = mapper.consultaServiciosAntad(request);
			json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_SERVICIOS+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}

	/*****************************************/
	/*nuevo metodo de consulta de parametros */
	/*****************************************/
	public String consultaParametros(String json) {
		String  parametro = "";
		ServiciosAntadRequest request = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_PARAMETROS + json);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			parametro = mapper.findByParametro(json);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_PARAMETROS+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_PARAMETROS+json);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return parametro;
	}
	
	/*****************************************/
	/*nuevo metodo que ontiene un error por id_error*/
	/*****************************************/
	public String getErrorByClave(String id_error) {
		String  parametro = "";
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_DESC_ERROR + id_error);
			parametro = mapper.getErrorByClave(id_error);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_DESC_ERROR+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_DESC_ERROR+id_error);
		}
		return parametro;
	}
	
	
	
	/*****************************************/
	/*nuevo metodo de consulta de usuario por usr_login */
	/*****************************************/
	public UsuarioVO consultaUsuarioById(String usr_login) {
		UsuarioVO usuario = new UsuarioVO();
		String  parametro = "";
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO + usr_login);
			usuario = mapper.getUsuario(usr_login);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO+"["+e.getCause()+"]");
			usr_login = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO+usr_login);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por email */
	/*****************************************/
	public UsuarioVO consultaUsuarioByEmail(String mail) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_EMAIL + mail);
			usuario = mapper.getUsuarioByEmail(mail);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_EMAIL+"["+e.getCause()+"]");
			mail = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_EMAIL+mail);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por email */
	/*****************************************/
	public UsuarioVO consultaUsuarioByT(String tdc) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_TDC + tdc);
			usuario = mapper.getUsuarioByT(tdc);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_TDC+"["+e.getCause()+"]");
			tdc = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_TDC+tdc);
		}
		return usuario;
	}
	
        /*****************************************/
	/*nuevo metodo de consulta de usuario por imei */
	/*****************************************/
	public UsuarioVO consultaUsuarioByImei(String imei) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_IMEI + imei);
			usuario = mapper.getUsuarioByImei(imei);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_IMEI+"["+e.getCause()+"]");
			imei = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_IMEI+imei);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo de consulta de usuario por telefono */
	/*****************************************/
	public UsuarioVO consultaUsuarioByTelefono(String telefono) {
		UsuarioVO usuario = new UsuarioVO();
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO_TELEFONO + telefono);
			usuario = mapper.getUsuarioByTelefono(telefono);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO_TELEFONO+"["+e.getCause()+"]");
			telefono = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_TELEFONO+telefono);
		}
		return usuario;
	}
        
        /*****************************************/
	/*nuevo metodo para agregar un nuevo usuario a t_usuarios o a t_usuario_tags */
	/*****************************************/
	public boolean addUser(UsuarioVO usuario) {
            boolean exito = false;
		try {
                    String wPwd = "99" + random();
                    LOGGER.info(LOG_SERVICE + LOG_PROCESO_ADD_USUARIO + usuario);
                    usuario.setId_usuario(new java.util.Date().getTime());
                    if (usuario.getId_usr_status() != 1000) {
                        usuario.setUsr_pwd(crypto.Crypto.sha1(wPwd));
                    } else {
                        String kk = ParametroDAO.parsePass(wPwd);
                        usuario.setUsr_pwd(crypto.Crypto.aesEncrypt(kk, wPwd));
                    }
                    usuario.setUsr_tdc_numero(ParametroDAO.setSMS(usuario.getUsr_tdc_numero()));
                    if (usuario.getId_usr_status() != 1000) {
                        usuario.setId_usr_status(99); // obliga a cambiar pwd y correo
                    } else {
                        usuario.setId_usr_status(1); //Deja activado el usuario con los datos por default
                    }
                    if(usuario.getUsr_fecha_nac() != null && !"".equals(usuario.getUsr_fecha_nac())){
            	           usuario.setUsr_fecha_nac(usuario.getUsr_fecha_nac());
                    } else {
                        usuario.setUsr_fecha_nac(null);
                    }
			mapper.insertUsuarios(usuario);
                        String mensaje = null;
                try {
                    if (usuario.getTipo_cliente() != 16) {
                        mensaje = URLEncoder.encode("Hola, bienvenido a MobileCard " + usuario.getUsr_nombre() + " tu usuario es: " + usuario.getUsr_login() + "  tu contraseña es: " + wPwd, "UTF-8");
                    } else {
                        mensaje = URLEncoder.encode("Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contrasena son:\n Usuario: " + usuario.getUsr_login() + "\n Contrasena: " + wPwd, "UTF-8");
                    }
                } catch (Exception e) {
                    LOGGER.debug("ERROR - generar cadena de SMS: " + e.getMessage());
                    //e.printStackTrace();
                    if (usuario.getTipo_cliente() != 16) {
                        mensaje = "Hola, bienvenido a MobileCard " + usuario.getUsr_nombre() + " tu usuario es: " + usuario.getUsr_login() + "  tu contraseña es: " + wPwd;
                    } else {
                        mensaje = "Bienvenido a TESORERIA GDF.\n Su nombre de usuario y contraseña son:\n Usuario: " + usuario.getUsr_login() + "\n Contraseña: " + wPwd;
                    }
                }
                    LOGGER.debug("Fin del Registro.");
                    if (usuario.getTipo().toUpperCase().equals("IPAD") || usuario.getTipo().toUpperCase().equals("IPOD")) {
                        if (usuario.getTipo_cliente() != 16) {
                            new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_REGISTRO", "@ASUNTO_REGISTRO", usuario.getUsr_login(), wPwd, "", "", "", "");
                        } else {
                            new ParametroDAO().EnviaMailBGDF(usuario.getUsr_login(), wPwd, usuario.geteMail());
                        }
                    } else {
                        /*          INICIO DE LA MODIFICACION
                         * SE IMPLEMENTO EL METODO DE EnviarSMS QUE UTILIZA
                         * EL ADDCELBATCH PARA VERIFICAR EN LA DB LOS SMS QUE 
                         * NO SE HAN ENVIADO. SE COMENTO EL METODO ANTERIO DE
                         * setSMS QUE ESTABA EN .NET (YA NO SE UTILIZA)
                         * 
                         * 25/07/13
                         * SE AGREGO EL ENVIO DE EMAIL A TODOS LOS DISPOSITOVOS 
                         * DESPUES DEL REGISTRO, PRIMERO ENVIA EL SMS Y LUEGO EL 
                         * MAIL EN CASO DE LOS QUE NO SON IPAD NI IPOD.
                         */
                        //setSMS(usuario.getNombre(), usuario.getTelefono(), Mensaje);
                        LOGGER.debug("Inicia envio de SMS...");
                        if (EnviarSMS(usuario.getUsr_telefono(), mensaje)) {
                            LOGGER.debug("Se registro el envio del mensaje con exito para el numero " + usuario.getUsr_telefono() + " usuario " + usuario.getUsr_nombre());
                        } else {
                            LOGGER.debug("No se pudo registrar el envio del mensaje para el numero " + usuario.getUsr_telefono() + " usuario " + usuario.getUsr_nombre());
                        }
                        if (usuario.getTipo_cliente() != 16) {
                            new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_REGISTRO", "@ASUNTO_REGISTRO", usuario.getUsr_login(), wPwd, "", "", "", "");
                        } else {
                            new ParametroDAO().EnviaMailBGDF(usuario.getUsr_login(), wPwd, usuario.geteMail());
                        }
                        /*
                         *          FIN DE LOS CAMBIOS
                         */
                    }
                    exito = true;
                    if (!usuario.getEtiqueta().equals("") && !usuario.getNumero().equals("") && usuario.getDv() != 0) {
                        UsuarioTagsVO usuariotag = new UsuarioTagsVO();
                        UsuarioVO x = new UsuarioVO();
                        x.setUsr_login(usuario.getUsr_login());
                        x.setUsr_pwd(usuario.getUsr_pwd());

                        LOGGER.debug("Cadena usuario.getLogin() " + usuario.getUsr_login());
                        LOGGER.debug("Cadena x.getLogin() " + x.getUsr_login());

                        x = mapper.getUsuario(x.getUsr_login());
                        LOGGER.debug("Cadena x.getLogin() 2 " + x.getUsr_login());
                        LOGGER.debug("Cadena x.getUsuario() 2 " + x.getId_usuario());

                        usuariotag.setId_usuario(x.getId_usuario());
                        usuariotag.setEtiqueta(usuario.getEtiqueta());
                        usuariotag.setId_tiporecargatag(usuario.getIdtiporecargatag());
                        usuariotag.setTag(usuario.getNumero());
                        usuariotag.setDv(usuario.getDv());

                        if (addTag(usuariotag)) {
                            LOGGER.debug("Se registro el usuario TAG con EXITO: " + x.getId_usuario());
                        } else {
                            LOGGER.debug("No se pudo registrar el usuario TAG: " + x.getId_usuario());
                        }
                    }
            } catch (Exception e) {
                LOGGER.error(LOG_SERVICE + LOG_ERROR_AGREGAR_USUARIO + "[" + e.getCause() + "]");
            } finally {
                LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO_TELEFONO + usuario);
            }
		return exito;
	}
        
    public boolean addTag(UsuarioTagsVO usuariotag) {
        boolean flag = false;
        try {
            mapper.insertUsuarioTags(usuariotag);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error: {}",ex);
        } 
        LOGGER.debug("Respuesta addTag: " + flag);
        return flag;
    }    
        
    /*               INICIO DEL CAMBIO
     * SE AGREGO ESTE METODO PARA EL ENVIO DE MENSAJES DE TEXTO
     * FUNCIONA CON LA APLICACION DE ADDCELBATCH
     * TODOS LOS REGISTROS QUE LLEGUEN A LA TABLA ozekimessageout
     * SE ESTAN VERIFICANDO EN BUSQUEDA DEL STATUS "Send" SI TIENE
     * ESTE ESTADO EL ADDCELBATCH LO ENCUENTRA Y LO ENVIA CAMBIANDO EL 
     * STATUS A "transmitted Y REGISTRANDO SU SALIDA EN smsOut
     */
    public boolean EnviarSMS(String numero, String mensaje){
        boolean flag = false;
        OzekimessageoutVO oze = new OzekimessageoutVO();
        try {
            oze.setReceiver(numero);
            oze.setMsg(mensaje);
            oze.setOperator("MobileCard");
            oze.setMsgtype("SMS:TEXT:GSM7BIT");
            oze.setSender("MobileCard");
            oze.setReference("MobileCard - APP");
            oze.setStatus("Send");
            mapper.insertOzekimessageout(oze);
            flag = true;
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error EnviarSMS: {}",ex);
        } 
        return flag;
    }
        
	/**
	 * Nueva clase obtenerUsuario que sustituye a la clase mostrada arriba llamada getUsuario. 
	 * se construye con la nueva arquitectura de mybatis.
	 * @author: JCDP
	 * @version: 12/07/2016
	 */
	public UsuarioVO obtenerUsuario(UsuarioVO usuario) {
        UsuarioVO userRep = null;
        String wPwd = "";
        String key = "1234567890ABCDEF0123456789ABCDEF";
        try {
        	/*nuevo codigo*/
        	userRep = mapper.getUsuario(usuario.getUsr_login());
        	/*fin de nuevo codigo*/
            
//            logger.debug("CADENA USUARIO ESTATUS 1: " + userRep.getStatus());
            if (userRep != null && userRep.getId_usr_status() == 99) {
            	LOGGER.info("obtenerUsuario IF");
                wPwd = usuario.getPasswordS();
            } else {
            	LOGGER.info("obtenerUsuario ELSE");
                wPwd = Crypto.aesEncrypt(key, usuario.getUsr_pwd());
            }
            
            if(userRep != null && userRep.getUsr_pwd() != null && Crypto.aesEncrypt(key, usuario.getUsr_pwd()).equals(wPwd)){
            	LOGGER.info("obtenerUsuario IF ALTERNATIVO");
                usuario = userRep;
            }
        } catch (Exception ex) {
        	LOGGER.error("Ocurrio un error en metodo obtenerUsuario(): {}",ex);
        } finally {
        	LOGGER.info(LOG_RESPUESTA_OBTENER_USUARIO+usuario.getUsr_nombre()); 
        }
        return usuario;
    }
	
	/*****************************************/
	/*nuevo metodo de consulta que valida usuario por usuario y password */
	/*****************************************/
	public UsuarioVO validaUsuario(String user, String password) {
		UsuarioVO usuario = new UsuarioVO();
		//ServiciosAntadRequest request = null;
		try {
			LOGGER.info("Entro validaUsuario 1");
			//json = AddcelCrypto.decryptSensitive(json);
			//LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO + json);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			usuario = mapper.validaUsuario(user, password);
			LOGGER.info("Entro validaUsuario 2");
		    LOGGER.info(LOG_SERVICE+usuario);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.info("Entro validaUsuario 3");
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO+"["+e.getCause()+"]");
			//json = JSON_ERROR_INESPERADO;
		} finally{
			//LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO+json);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		LOGGER.info("Entro validaUsuario 4");
		return usuario;
	}
	
	/*****************************************/
	/*nuevo metodo de consulta que consulta producto por clave proporcionada */
	/*****************************************/
	public ProductoVO consultaProducto(String producto) {
		ProductoVO prod = new ProductoVO();
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_USUARIO + producto);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			prod = mapper.getProductoByClaveWS(producto);
		    LOGGER.info(LOG_SERVICE+prod);
		    parametro = prod.getNombre();
		    LOGGER.info(LOG_SERVICE+parametro);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_USUARIO+"["+e.getCause()+"]");
			producto = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_USUARIO+producto);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return prod;
	}
	
	/*****************************************/
	/*nuevo metodo de consulta que consulta producto por clave proporcionada */
	/*****************************************/
	public ProveedorVO consultaProveedor(String idproveedor) {
		ProveedorVO proveedor = new ProveedorVO();
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_PROVEEDOR + idproveedor);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			proveedor = mapper.getProveedor(idproveedor);
		    LOGGER.info(LOG_SERVICE+proveedor);
		    parametro = proveedor.getPrv_nombre_comercio();
		    LOGGER.info(LOG_SERVICE+parametro);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_PROVEEDOR+"["+e.getCause()+"]");
			idproveedor = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_PROVEEDOR+idproveedor);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return proveedor;
	}
	
	/*****************************************/
	/*nuevo metodo que consulta proveedor por claveWS */
	/*****************************************/
	public ProveedorVO consultaProveedorByClaveWS(String claveWS) {
		ProveedorVO proveedor = new ProveedorVO();
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_PROVEEDOR + claveWS);
			proveedor = mapper.getProveedorByClaveWS(claveWS);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_PROVEEDOR+proveedor.getPrv_nombre_comercio());
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return proveedor;
	}
	
	/*****************************************/
	/*nuevo metodo que consulta proveedor por idproveedor */
	/*****************************************/
	public List<Producto> getProductosByProveedor(long idprov) {
		List<Producto> res = null;
		try {
			res = mapper.getProductosByProveedor(idprov);
			Iterator iter = res.iterator();
			while (iter.hasNext()) {
			  LOGGER.info("recorremos la lista");		
			  System.out.println(iter.next());
			}
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+"["+e.getCause()+"]");
		} 
		return res;
	}
	
	
	/*****************************************/
	/*nuevo metodo de consulta que consulta la comision por clave de proveedor */
	/*****************************************/
	public double consultaComision(String idproveedor) {
		double parametro = 0;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_COMISION + idproveedor);
			parametro = mapper.getComision(Integer.parseInt(idproveedor));
		    LOGGER.info(LOG_SERVICE+parametro);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_COMISION+"["+e.getCause()+"]");
			idproveedor = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_COMISION+idproveedor);
		}
		return parametro;
	}
	
	/*****************************************/
	/*nuevo metodo que actualiza la bitacora */
	/*****************************************/
	public void actualizaBitacora(BitacoraVO bitacora) {
		//BitacoraVO bitacora = new BitacoraVO();
		//String  parametro = "Bitacora actualizada";
		//bitacora.setId_bitacora(362);
		//bitacora.setBit_concepto("Compra TA Telcel 5534918527 20.00 modificado JCDP");
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_ACTUALIZA_BITACORA + bitacora.getId_bitacora());
			mapper.actualizaBitacora(bitacora);
		    //LOGGER.info(LOG_SERVICE+bitacora);
		    //LOGGER.info(LOG_SERVICE+parametro);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_ACTUALIZA_BITACORA+"["+e.getCause()+"]");
			//json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_ACTUALIZA_BITACORA);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		//return parametro;
	}
	
	/*****************************************/
	/*nuevo metodo validaT */
	/*****************************************/
	public boolean validaT(String T) {
		boolean Res=false;
        int resultado = 0;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_VALIDA_T + T);
			resultado = mapper.validaT(T);
			LOGGER.info("resultado :" + resultado);
            if(resultado == 1){
                Res=true;
            } 
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_VALIDA_T+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_VALIDA_T+Res);
		}
		return Res;
	}
	
	/*****************************************/
	/*nuevo metodo que actualiza la bitacoraiave */
	/*****************************************/
	public void actualizaBitacoraIave(BitacoraiaveVO bitacoraiave) {
		/*BitacoraiaveVO bitacoraiave = new BitacoraiaveVO();
		String  parametro = "Bitacoraiave actualizada";
		bitacoraiave.setId_bitacora(10763);
		bitacoraiave.setPosname("mofificacion de prueba by JCDP");*/
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_ACTUALIZA_BITACORAIAVE);
			mapper.actualizaBitacoraIave(bitacoraiave);
		    LOGGER.info(LOG_SERVICE+bitacoraiave);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_ACTUALIZA_BITACORAIAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_ACTUALIZACION_BITACORAIAVE);
		}
	}
	
	/*****************************************/
	/*nuevo metodo que inserta la bitacoraiave */
	/*****************************************/
	public void insertaBitacoraIave(BitacoraiaveVO bitvo) {
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_INSERTA_BITACORAIAVE);
			mapper.insertaBitacoraIave(bitvo);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_INSERTA_BITACORAIAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_BITACORAIAVE);
		}
	}
	
	/*****************************************/
	/*nuevo metodo que inserta la bitacora prosa */
	/*****************************************/
	public void insertaBitacoraProsa(BitacoraprosaVO bitprosa) {
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_INSERTA_BITACORA_PROSA);
			mapper.insertaBitacoraProsa(bitprosa);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_INSERTA_BITACORA_PROSA+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_INSERTA_BITACORA_PROSA);
		}
	}
	

	/*****************************************/
	/*nuevo metodo consulta folio iave */
	/*****************************************/
	public String getFolioIAVE() {
		String res = ""; 
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTAFOLIO_IAVE);
			res = mapper.getFolioIAVE();
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTAFOLIO_IAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_FOLIO_IAVE+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta folio banco iave */
	/*****************************************/
	public String getFolioBancoIAVE() {
		String res = ""; 
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTAFOLIO_BANCO_IAVE);
			res = mapper.getFolioBancoIAVE();
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTAFOLIO_BANCO_IAVE+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_FOLIO_BANCO_IAVE+res);
		}
		return res;
	}

    /*****************************************/
	/* nuevo metodo consulta estados */
	/*****************************************/
	public List<EstadosVO> getEstados() {
                List<EstadosVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTAESTADOS);
			res = mapper.getEstados();
                        LOGGER.info("numero de estados encontrados : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTAESTADOS+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_ESTADOS+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta tipos de tarjeta */
	/*****************************************/
	public List<TTipoTarjetaVO> getTiposTarjeta() {
                List<TTipoTarjetaVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTATIPOSTARJETAS);
			res = mapper.getTiposTar();
                        LOGGER.info("numero de tarjetas encontradas : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTATIPOSTARJETAS+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_TIPOSTARJETAS+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta bancos */
	/*****************************************/
	public List<TBancoVO> getBancos() {
                List<TBancoVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_BANCOS);
			res = mapper.getBancos();
                        LOGGER.info("numero de bancos encontrados : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_BANCOS+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_BANCOS+res);
		}
		return res;
	}
	
	/*****************************************/
	/* nuevo metodo consulta bancos */
	/*****************************************/
	public List<CatCarriesVO> getProveedores() {
                List<CatCarriesVO> res = null;
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_CARRIES);
			res = mapper.getProveedoresCarries();
            LOGGER.info("numero de proveedores : " + res.size());
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_CARRIES+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_CARRIES+res);
		}
		return res;
	}
	/*metodo para poder consultar los proveedores */
	 public List getProveedores(String IdCategorias, long idUsuario, long plataforma) throws ClassNotFoundException, SQLException {
	        List proveedores = new ArrayList();
	        UsuarioVO usu = new UsuarioVO();
	         int tipotarjeta = 0;
	        try {
	            LOGGER.debug("MPX IDCATEGORIA: " + IdCategorias);
	            tipotarjeta = mapper.getTipoTarByUsuario(idUsuario);
	        } catch (Exception ex) {
	            LOGGER.error("Ocurrio un error getProveedores(String, long, long): {}",ex);
	        } finally { 
	        	LOGGER.info("getProveedores() : OK");
	        }
	        /*try{
	            String sWhere = "";
	            if (plataforma == 1) { //IOS
	                sWhere = "  and ActivoIOS = 1 ";
	            }
	            if (plataforma == 2) { //Android
	                sWhere = "  and ActivoAndroid = 1 ";
	            }
	            if (plataforma == 3) { //BB
	                sWhere = "  and ActivoBB = 1 ";
	            }
	            if (plataforma == 4) { //WPhone
	                sWhere = "  and ActivoWPhone = 1 ";
	            }
	            if (plataforma == 5) { //Java
	                sWhere = "  and ActivoJava = 1 ";
	            }
	            
	            /*statement = connect().prepareStatement( SELECT_TPROVEEDOR_IDC + sWhere);
	            statement.setString(1, IdCategorias);
	            resultSet = statement.executeQuery();
	            if (resultSet.next()) {
	                do {
	                    proveedores.add(getProveedorRS_amex(resultSet, tipotarjeta));
	                } while (resultSet.next());
	            }
	        } catch (SQLException ex) {
	            logger.error("Ocurrio un error: {}",ex);
	        } finally {
	            closeConPrepStResSet(statement, resultSet);
	        }*/
	        return proveedores;
	    }
	
	// INI pDiaz
	public String consultaEndPointsService(String json) {
				
		List<EndPointsResponse> serviciosEndPointsList = null;
		EndPointsRequest request = null;
		
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			//LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SERVICIOS + json);
			request = (EndPointsRequest) jsonUtils.jsonToObject(json, EndPointsRequest.class);
			serviciosEndPointsList = mapper.consultaEndPoints(request);
			json = (String) jsonUtils.objectToJson(serviciosEndPointsList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_CONSULTA_SERVICIOS+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_SERVICIOS+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}
	// FIN pDiaz
	
	public String realizaPago(String json) {
		ProcesaAutorizacion autorizacion = null;
		ProcesaAutorizacionResponse response = null;
		ObjectFactory factory = new ObjectFactory();
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_PROCESA_PAGO + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			autorizacion = new ProcesaAutorizacion();
			autorizacion.setJson(json);
			response = (ProcesaAutorizacionResponse) webServiceTemplate.marshalSendAndReceive(
					factory.createProcesaAutorizacion(autorizacion));
			json = jsonUtils.objectToJson(response);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_PROCESA_PAGO+" ["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PROCESA_PAGO+json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return null;
	}

	public String realizaConsulta(String json) {
		ObjectFactory factory = new ObjectFactory();
		Consulta consulta;
		ConsultaResponse response; 
		try {
			json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_REALIZA_CONSULTA + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
			consulta = new Consulta();
			consulta.setJson(json);
			response = (ConsultaResponse) webServiceTemplate.marshalSendAndReceive(factory.createConsulta(consulta));
			json = (String) jsonUtils.objectToJson(response);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_REALIZA_CONSULTA+"["+e.getCause()+"]");
			json = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_PREALIZA_CONSULTA + json);
			json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		return json;
	}

	public WebServiceTemplate getWebServiceTemplate() {
		return webServiceTemplate;
	}

	public void setWebServiceTemplate(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}

	public Map<String, Object> seleccionarRegla(long idUsuario, String tarjeta,
			String proveedor, String clave) {
		// TODO Auto-generated method stub
		String  parametro = "";
		//ServiciosAntadRequest request = null;
		Map<String, Object> valoresU = null;
		try {
			//json = AddcelCrypto.decryptSensitive(json);
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_CONSULTA_SELECCIONAR_REGLAS + idUsuario);
			//request = (ServiciosAntadRequest) jsonUtils.jsonToObject(json, ServiciosAntadRequest.class);
			valoresU = mapper.seleccionarRegla(idUsuario, tarjeta, proveedor, clave);
			//json = (String) jsonUtils.objectToJson(serviciosAntadList);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_SELECCIONAR_REGLAS+"["+e.getCause()+"]");
			//"idUsuario = JSON_ERROR_INESPERADO;
		} finally{
			LOGGER.info(LOG_RESPUESTA_CONSULTA_REGLAS+idUsuario);
			//json = AddcelCrypto.encryptSensitive(SDF.format(new Date()),json);
		}
		LOGGER.info("EL MAP contiene : " + valoresU.size());
		return valoresU;
	}
	
	/*nuevo metodo para actualizar usuarios*/
	public void actualizaUsuario(UsuarioVO usuario){
		try {
			LOGGER.info(LOG_SERVICE+LOG_PROCESO_ACTUALIZA_USUARIO + usuario.getUsr_login());
			mapper.updateUsuarios(usuario);
		} catch (Exception e) {
			LOGGER.error(LOG_SERVICE+LOG_ERROR_ACTUALIZAR_USUARIO+"["+e.getCause()+"]");
		} finally{
			LOGGER.info(LOG_RESPUESTA_ACTUALIZA_USUARIO+usuario.getUsr_login());
		}
	}
	
        /**
	 * Nueva clase newValidateUsuario que sustituye a la clase mostrada arriba llamada validateUsuario. 
	 * se construye con la nueva arquitectura de persistencia mybatis.
	 * @author: JCDP
	 * @version: 12/07/2016
	 */
            public boolean newValidateUsuario(UsuarioVO usuario) {
            boolean flag = false;
            UsuarioVO vusuario = null;
            try {
                vusuario = mapper.validaUsuario(usuario.getUsr_login(), usuario.getUsr_pwd());
                if (vusuario != null) {
                    flag = true;
                }
            } catch (Exception ex) {
                LOGGER.error("Ocurrio un error en método newValidateUsuario(UsuarioVO) : {}", ex);
            } finally {
                //closeConPrepStResSet(statement, resultSet);
            }
            return flag;
        }
        
        /*
	 * metodo para actualizar password de usuario 
	 * by JCDP
	 * Fecha : 18/07/2016
	 * */
	//public boolean updatePassword(long usuario, String password, String sMail, String u, String p) throws SQLException, ClassNotFoundException {
	public boolean updatePassword(UsuarioVO usuario){
        boolean flag = false;
        try {
        	mapper.updateUsuarios(usuario);
        	flag = true;
            new ParametroDAO().EnviaMail("", usuario.geteMail(), "@MENSAJE_RECOVERYPWD", "@ASUNTO_RECOVERYPWD", usuario.getUsr_login(), usuario.getUsr_pwd(), "", "", "", "");
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en nuevo metodo updatePassword() : {}",ex);
        } 
        return flag;
    }    
        /*
	 * metodo para que valida si existe un usuario con un mail y un usr_login existente 
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean ExisteMailUp(String eMail, String idusuario){
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.existeMailUp(eMail, idusuario);
            if ( user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en metodo ExisteMailUp clase UsuarioDAO: {}",ex);
        }
        return flag;
    }    
        
        /*
	 * metodo para que valida si existe un usuario con un numero de tarjeta y un usr_login existente 
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean ExisteTUp(String T, String idusuario){
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
        	user = mapper.existeTUp(T, idusuario);
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error: {}",ex);
        } 
        return flag;
    }
        /*
	 * metodo para realizar un update a un usuario se encuentra activo
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
        public boolean update(UsuarioVO usuario) {
	        boolean flag = false;
	        try {
	        	if (UsuarioActivo(usuario)) {
	                mapper.updateUsuarios(usuario);
	                flag = true;
                        LOGGER.error("Actualizacion de usuario exitosa(): {}");
	            } else {
	                flag = false;
                        LOGGER.error("El usuario no se encuentra activo no se pudo actualizar");
	            }
	        } catch (Exception ex) {
	            LOGGER.error("Ocurrio un error en metodo update(): {}",ex);
	        } 
	        return flag;
	    }
        
        /*
	 * metodo para consultar si un usuario se encuentra activo
	 * by JCDP
	 * Fecha : 19/07/2016
	 * */
	public boolean UsuarioActivo(UsuarioVO usuario) {
        boolean flag = false;
        UsuarioVO user = new UsuarioVO();
        try {
            user = mapper.usuarioActivo(usuario.getUsr_login());
            if (user != null) {
                flag = true;
            }
        } catch (Exception ex) {
            LOGGER.error("Ocurrio un error en metodo UsuarioActivo: {}", ex);
        }
        return flag;
    }
        
	/*
	 * nuevo metodo en sustitucion del anterior metodo con el mismo nombre
	 * by JCDP 15072016
	 * 
	 * */
	public void  BuscaIm (String T) {
		BloqImeiVO bi = new BloqImeiVO(); 
		bi = mapper.selBloqImei(T);
	        try{
	        	if(T.trim().equals("")) return;
	            if ( bi != null) {
	            	LOGGER.debug("BUSCA IMEI ACTUALIZA: " + T );
	                bi.setActivo(1);
	                mapper.updateBloqImei(bi);
	            }else{
	               bi.setActivo(1);
	               mapper.insertBloqImei(bi);
	               LOGGER.debug("BUSCA IMEI ALTA: " + T );
	            }
	        }catch(Exception e){
	        	LOGGER.error("Ocurrio un error en nuevo metodo BuscaIm: {}", e);
	        }
    }
	
	/*
	 * metodo ValidaIM con la nueva arquitectura de MyBatis 
	 * By JCDP 
	 * 15072016 
	 * */
	public boolean  ValidaIM (String T) {
        boolean res=false;
        String act = mapper.bImei(T);
        LOGGER.info("act : -> " + act);
        try {
            if(act != null){
                res=true;
            }
        } catch (Exception ex) {
        	LOGGER.error("Ocurrio un error en metodo ValidaIM: {}",ex.getMessage());
        }
         return res;
    }
	/*
	 * metodo BTar con la nueva arquitectura de MyBatis 
	 * By JCDP 
	 * 15072016 
	 * */
	public void BTar(String tarjeta){
		TBloVO  tb = new TBloVO(); 
        try{
            if(tarjeta.trim().equals("")) return;
            tb = mapper.getTBloByTarjeta(ParametroDAO.setSMS( tarjeta));
            if ( tb != null ){
            	LOGGER.debug("BUSCA TARJETA ACTUALIZA: "  );
                tb.setActivo(1);
                tb.setTarjeta(setSMS(tarjeta));
                mapper.updateTBlo(tb);
            }else{
            	TBloVO  tb1 = new TBloVO();
            	LOGGER.debug("BUSCA TARJETA ALTA: "  );
            	tb1.setActivo(1);
            	tb1.setTarjeta(setSMS( tarjeta));
                mapper.updateTBlo(tb1);
            }
        }catch(Exception e){
        	LOGGER.error("Ocurrio un error en metodo BTar: {}", e.getMessage());
        }
    }
	public static String parsePass(String pass){
        int len = pass.length();
        String key = "";

        for (int i =0; i < 32 /len; i++){
              key += pass;
        }

        int carry = 0;
        while (key.length() < 32){
              key += pass.charAt(carry);
              carry++;
        }
        return key;
    }

    public String random() {
        Random rand = new Random();
        int x = rand.nextInt(99999999);
        return "" + x;
    }
}
