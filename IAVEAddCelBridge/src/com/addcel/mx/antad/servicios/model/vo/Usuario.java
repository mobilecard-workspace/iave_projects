/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.addcel.mx.antad.servicios.model.vo;


/**
 *
 * @author - David Hernandez
 */
public class Usuario {
    private long usuario;
    private String login;
    private String password;
    private String nacimiento;
    private String telefono;
    private String registro;
    private String nombre;
    private String apellido;
    private String direccion;
    private String tarjeta;
    private String vigencia;
    private int banco;
    private int tipotarjeta;
    private int proveedor;
    private int status;
    private String mail;
    private String passwordS;
    private String imei;
    private int idtiporecargatag;
    private String etiqueta;
    private String numero;
    private int dv;
    private String tipo;
    private String software;
    private String modelo;
    private String key;
    
    private String materno;
    private String sexo;
    private String tel_casa;
    private String tel_oficina;
    private int id_estado;
    private String ciudad;
    private String calle;
    private int num_ext;
    private String num_interior;
    private String colonia;
    private String cp;
    private String dom_amex;
    private String terminos;
    private int tipo_cliente;



    public void Usuario(){}

    
    
    public int getIdtiporecargatag() {
        return idtiporecargatag;
    }

    public void setIdtiporecargatag(int idtiporecargatag) {
        this.idtiporecargatag = idtiporecargatag;
    }

    public String getMaterno() {
        return materno;
    }

    public void setMaterno(String materno) {
        this.materno = materno;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTel_casa() {
        return tel_casa;
    }

    public void setTel_casa(String tel_casa) {
        this.tel_casa = tel_casa;
    }

    public String getTel_oficina() {
        return tel_oficina;
    }

    public void setTel_oficina(String tel_oficina) {
        this.tel_oficina = tel_oficina;
    }

    public int getId_estado() {
        return id_estado;
    }

    public void setId_estado(int id_estado) {
        this.id_estado = id_estado;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public int getNum_ext() {
        return num_ext;
    }

    public void setNum_ext(int num_ext) {
        this.num_ext = num_ext;
    }

    public String getNum_interior() {
        return num_interior;
    }

    public void setNum_interior(String num_interior) {
        this.num_interior = num_interior;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }

    public String getDom_amex() {
        return dom_amex;
    }

    public void setDom_amex(String dom_amex) {
        this.dom_amex = dom_amex;
    }

    public String getTerminos() {
        return terminos;
    }

    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }


    
    /**
     * @return the login
     */
    
    
    
    
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the nacimiento
     */
    public String getNacimiento() {
        return nacimiento;
    }

    /**
     * @param nacimiento the nacimiento to set
     */
    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the registro
     */
    public String getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(String registro) {
        this.registro = registro;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the tarjeta
     */
    public String getTarjeta() {
        return tarjeta;
    }

    /**
     * @param tarjeta the tarjeta to set
     */
    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    /**
     * @return the vigencia
     */
    public String getVigencia() {
        return vigencia;
    }

    /**
     * @param vigencia the vigencia to set
     */
    public void setVigencia(String vigencia) {
        this.vigencia = vigencia;
    }

    /**
     * @return the banco
     */
    public int getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(int banco) {
        this.banco = banco;
    }

    /**
     * @return the tipotarjeta
     */
    public int getTipotarjeta() {
        return tipotarjeta;
    }

    /**
     * @param tipotarjeta the tipotarjeta to set
     */
    public void setTipotarjeta(int tipotarjeta) {
        this.tipotarjeta = tipotarjeta;
    }

    /**
     * @return the provedor
     */
    public int getProveedor() {
        return proveedor;
    }

    /**
     * @param proveder the provedor to set
     */
    public void setProveedor(int proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the usuario
     */
    public long getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(long usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * @param apellido the apellido to set
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    
    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param apellido the mail to set
     */
    public void setMail(String mail) {
        this.mail = mail;
    }
    /**
     * @return the passwordS
     */
    public String getPasswordS() {
        return passwordS;
    }

    /**
     * @param passwordS the passwordS to set
     */
    public void setPasswordS(String passwordS) {
        this.passwordS = passwordS;
    }
    
    
    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }    
    
       
    /**
     * @return the imei
     */
    public int getIdTipoRecargaTag() {
        return idtiporecargatag;
    }

    /**
     * @param imei the imei to set
     */
    public void setIdTipoRecargaTag(int idtiporecargatag) {
        this.idtiporecargatag = idtiporecargatag;
    }    
    
    
    /**
     * @return the imei
     */
    public String getEtiqueta() {
        return etiqueta;
    }

    /**
     * @param imei the imei to set
     */
    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }    
        
    /**
     * @return the imei
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param imei the imei to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }    

    /**
     * @return the imei
     */
    public int getDv() {
        return dv;
    }

    /**
     * @param imei the imei to set
     */
    public void setDv(int dv) {
        this.dv = dv;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public int getTipo_cliente(){
        return tipo_cliente;
    }
    public void setTipo_cliente(int tipo_cliente){
        this.tipo_cliente = tipo_cliente;
    }         
  
    
}
