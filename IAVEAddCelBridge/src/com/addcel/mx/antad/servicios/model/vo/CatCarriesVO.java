package com.addcel.mx.antad.servicios.model.vo;

import java.util.List;

public class CatCarriesVO {
    
	private String id;

    private String nombre;

    private Integer activo;

    public String getId() {
        return id;
    }

    List<CatCarriesVO> carries;
    
    public List<CatCarriesVO> getCarries() {
		return carries;
	}

	public void setCarries(List<CatCarriesVO> carries) {
		this.carries = carries;
	}

	public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
}