package com.addcel.mx.antad.servicios.model.vo;

import java.util.List;

public class ProductoVO {
    private Integer id_producto;

    private String pro_clave;

    private Float pro_monto;

    private String pro_path;

    private Long id_proveedor;

    private String nombre;

    private List<ProductoVO> productos; 
    
    public List<ProductoVO> getProductos() {
		return productos;
	}

	public void setProductos(List<ProductoVO> productos) {
		this.productos = productos;
	}

	public Integer getId_producto() {
        return id_producto;
    }

    public void setId_producto(Integer id_producto) {
        this.id_producto = id_producto;
    }

    public String getPro_clave() {
        return pro_clave;
    }

    public void setPro_clave(String pro_clave) {
        this.pro_clave = pro_clave;
    }

    public Float getPro_monto() {
        return pro_monto;
    }

    public void setPro_monto(Float pro_monto) {
        this.pro_monto = pro_monto;
    }

    public String getPro_path() {
        return pro_path;
    }

    public void setPro_path(String pro_path) {
        this.pro_path = pro_path;
    }

    public Long getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(Long id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}