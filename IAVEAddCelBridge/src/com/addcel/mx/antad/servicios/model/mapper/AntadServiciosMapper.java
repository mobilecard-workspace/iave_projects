package com.addcel.mx.antad.servicios.model.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.addcel.mx.antad.servicios.model.vo.BitacoraDetalleVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraVO;
import com.addcel.mx.antad.servicios.model.vo.EndPointsRequest;
import com.addcel.mx.antad.servicios.model.vo.EndPointsResponse;
import com.addcel.mx.antad.servicios.model.vo.OzekimessageoutVO;
import com.addcel.mx.antad.servicios.model.vo.ServiciosAntadRequest;
import com.addcel.mx.antad.servicios.model.vo.ServiciosAntadResponse;
import com.addcel.mx.antad.servicios.model.vo.TBloVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioTagsVO;
import com.addcel.mx.antad.servicios.model.vo.UsuarioVO;
import com.addcel.mx.antad.servicios.model.vo.ProductoVO;
import com.addcel.mx.antad.servicios.model.vo.ProveedorVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraiaveVO;
import com.addcel.mx.antad.servicios.model.vo.BitacoraprosaVO;
import com.addcel.mx.antad.servicios.model.vo.BloqImeiVO;

public interface AntadServiciosMapper {

	public List<ServiciosAntadResponse> consultaServiciosAntad(ServiciosAntadRequest request);
	
	// INI pDiaz
	public List<EndPointsResponse> consultaEndPoints(EndPointsRequest request);
	// FIN pDiaz

	public int insertaBitacoraTransaccion(BitacoraVO bitacora);
	
	public int insertaBitacoraTransaccionDetalle(BitacoraDetalleVO bitacora);
	
	public void insertaBitacoraProsa(BitacoraprosaVO bitacoraprosa);

	public double getComision(@Param(value = "idproveedor") int idProveedor);

	public void actualizaBitacora(BitacoraVO bitacora);
	
	public void updateUsuarios(UsuarioVO usuario);
	
	public void actualizaBitacoraIave(BitacoraiaveVO bitacoraiave);
	
	public void insertaBitacoraIave(BitacoraiaveVO bitacoraiave); 

	public UsuarioVO getUsuario(@Param(value = "id") String idUsuario);
	
	public int getTipoTarByUsuario(@Param(value = "idusuario") long idUsuario);
	
	public ProveedorVO getProveedorByCategoria(@Param(value = "idusuario") long idUsuario);
	
	public UsuarioVO getUsuarioByEmail(@Param(value = "mail") String email);
	
	public UsuarioVO getUsuarioByT(@Param(value = "tc") String tc);
	
	public UsuarioVO getUsuarioByImei(@Param(value = "imei") String imei);
	
	public UsuarioVO getUsuarioByTelefono(@Param(value = "telefono") String telefono);
	
	public ProductoVO getProductoByClaveWS(@Param(value = "prodclave") String proclave);
	
	public UsuarioVO validaUsuario(@Param(value = "id") String idUsuario, @Param(value = "pwd") String password); 
	
	public UsuarioVO existeMailUp(@Param(value = "email") String email, @Param(value = "login") String usrlogin);
	
	public UsuarioVO existeTUp(@Param(value = "tarjeta") String email, @Param(value = "login") String usrlogin);
	
	public UsuarioVO usuarioActivo(@Param(value = "login") String login);
	
	public String findByParametro(@Param(value = "pclave") String clave);
	
	public String getFolioIAVE();
	
	public String getFolioBancoIAVE();
	
	public ProveedorVO getProveedor(@Param(value = "idprov") String provclave);
	
	public ProveedorVO getProveedorByClaveWS(@Param(value = "claveWS") String claveWS);
	
	public List getProductosByProveedor(@Param(value = "idproveedor") long idproveedor);
	
	public String getErrorByClave(@Param(value = "pclave") String iderror);
	
	public int validaT(@Param(value = "idtarjeta") String idtarjeta);
	
	public TBloVO getTBloByTarjeta(@Param(value = "tarjeta") String tarjeta);
	
	public String bImei(@Param(value = "imei") String idimei);
	
	public void insertBloqImei(BloqImeiVO bloqimei);
	
	public void insertUsuarios(UsuarioVO usuario);
	
	public void updateBloqImei(BloqImeiVO bloqimeiupdate);
	
	public BloqImeiVO selBloqImei(@Param (value = "imei" ) String imei);
    
	public void insertTBlo(TBloVO tblo);
	
	public void updateTBlo(TBloVO tblo);
	
	public void insertOzekimessageout(OzekimessageoutVO Ozekimessageout);
	
	public void insertUsuarioTags(UsuarioTagsVO usuariotags);
	
	public Map<String, Object> seleccionarRegla(@Param(value = "idusuario") long idusuario, @Param(value = "idtarjeta") String idtarjeta, @Param(value = "idproveedor") String idproveedor, @Param(value = "clave") String clave);
        
    public List getEstados();
    
    public List getTiposTar();
    
    public List getBancos();
    
    public List getProveedoresCarries();
}
