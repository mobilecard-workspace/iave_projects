package com.addcel.mx.antad.servicios.model.vo;

public class TBloVO {
    private String tarjeta;

    private Integer activo;

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Integer getActivo() {
        return activo;
    }

    public void setActivo(Integer activo) {
        this.activo = activo;
    }
}