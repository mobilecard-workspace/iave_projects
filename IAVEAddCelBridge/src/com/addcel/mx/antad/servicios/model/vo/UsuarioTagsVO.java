package com.addcel.mx.antad.servicios.model.vo;

public class UsuarioTagsVO {
    private Long id_usuario;

    private Integer id_tiporecargatag;

    private String etiqueta;

    private String tag;

    private Integer dv;

    public Long getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(Long id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Integer getId_tiporecargatag() {
        return id_tiporecargatag;
    }

    public void setId_tiporecargatag(Integer id_tiporecargatag) {
        this.id_tiporecargatag = id_tiporecargatag;
    }

    public String getEtiqueta() {
        return etiqueta;
    }

    public void setEtiqueta(String etiqueta) {
        this.etiqueta = etiqueta;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getDv() {
        return dv;
    }

    public void setDv(Integer dv) {
        this.dv = dv;
    }
}