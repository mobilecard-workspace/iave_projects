<%-- 
    Document   : comercio_fin
    Created on : 15/03/2016
    Author     : RHT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="viewport" content="width=device-width" />
		
		<%	
		   	response.setHeader("Expires","0");
		   	response.setHeader("Pragma","no-cache");
		   	response.setDateHeader("Expires",-1);
		 	
		%>
        <title>ANTAD</title>
        <script type="text/javascript">
            function sendform() {
                if (self.name.length === 0) {
                    self.name = "gotoProsa";
                }
                //var twnd = window.open("","wnd","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=1,copyhistory=0,width=760,height=750");
                document.form1.return_target.value = self.name.toString();
                //document.form1.target = "wnd";
                setTimeout (function () {
        	    	document.form1.submit();
        	    }, 1000);
            }
        </script>
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #D3441C;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>
    </head>
    <body onload="sendform();">
        <form method="post" name="form1" action="https://www.procom.prosa.com.mx/eMerchant/7627488_ADCL_Diestel.jsp">
            <input type="hidden" name="total" value="${prosa.total}">
            <input type="hidden" name="currency" value="${prosa.currency}">
            <input type="hidden" name="address" value="${prosa.address}">
            <input type="hidden" name="order_id" value="${prosa.orderId}">
            <input type="hidden" name="merchant" value="${prosa.merchant}">
            <input type="hidden" name="store" value="${prosa.store}">
            <input type="hidden" name="term" value="${prosa.term}">
            <input type="hidden" name="digest" value="${prosa.digest}">
            <input type="hidden" name="return_target" value="">                       
            <input type="hidden" name="urlBack" value="${prosa.urlBack}">                        
        </form>
        <div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr>
				<th><p>
				Envio de informaci&oacute;n.
				</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>
					<p>
						<br/>
						Se est&aacute; enviando la informaci&oacute;n, favor de esperar.						
						<br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
    </body>
</html>
