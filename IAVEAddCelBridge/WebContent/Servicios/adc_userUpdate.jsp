<%-- 
    Document   : user_update
    Created on : Jul 19, 2016
    Author     : -
--%>

<%@page import="crypto.Crypto"%>
<%@page import="com.ironbit.addcell.integration.UsuarioDAO"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.ironbit.addcell.bean.Usuario"%>
<%@page import="com.addcel.mx.antad.servicios.model.vo.UsuarioVO"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.ironbit.addcell.bean.Respuesta"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
String key = "1234567890ABCDEF0123456789ABCDEF";
Respuesta respuesta = new Respuesta();
UsuarioVO usuario  = new UsuarioVO();
String json;
if(request.getParameter("json").isEmpty()){
    json = (String) request.getAttribute("json").toString();
}else{
    json = (String) request.getParameter("json").toString();
}

new com.addcel.utils.U().Display("*********************     USUARIO Update... ");

new com.addcel.utils.U().Display("CADENA userUpdate: " + json);

String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
key = Cad[0];
json = Cad[1];
//proceso
//new com.addcel.utils.U().Display("CADENA key " + key);
//new com.addcel.utils.U().Display("CADENA json " + json);


Gson gson = new Gson();
Type collectionType = new TypeToken<Usuario>(){}.getType();
usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);

usuario.setUsr_pwd(Crypto.aesEncrypt(key,  usuario.getUsr_pwd() ));

new com.addcel.utils.U().Display("CADENA LOGIN " + usuario.getUsr_login());
new com.addcel.utils.U().Display("CADENA IDUSUARIO " + usuario.getId_usuario());

	if(new UsuarioDAO().ExisteMailUp(usuario.geteMail(),usuario.getUsr_login() ))
	{
	      respuesta.setResultado(3);
	      respuesta.setMensaje("El Correo Electronico ya fue asignado a otro usuario. Intente de nuevo.");
	}
	else if(new UsuarioDAO().ExisteTUp(usuario.getUsr_tdc_numero(),usuario.getUsr_login() ))
	{
	    respuesta.setResultado(4);
	    respuesta.setMensaje("La tarjeta proporcionada ya esta asignada a otro usuario. Intente de nuevo.");
	}
	else if(new UsuarioDAO().update(usuario))
	{
	    respuesta.setResultado(1);
	    respuesta.setMensaje("El registro ha sido actualizado satisfactoriamente");
	}else
	{
	    respuesta.setResultado(2);
	    respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
	}
	
	String jsonOutput = gson.toJson(respuesta);
%>
<%=Crypto.aesEncrypt(key, jsonOutput)%>