<%--
    Document   : user_login
    Created on : Jul 18 2016 
    Author     : JCDP
--%>
<%@page import="crypto.Crypto"%>
<%@page import="com.ironbit.addcell.integration.UsuarioDAO"%>
<%@page import="com.addcel.mx.antad.servicios.model.vo.UsuarioVO"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.ironbit.addcell.bean.Usuario"%>
<%@page import="com.ironbit.addcell.bean.Respuesta"%>
<%@page import="com.google.gson.Gson"%>
<%
String key = "1234567890ABCDEF0123456789ABCDEF";
Respuesta respuesta = new Respuesta();
String resp = null;
UsuarioVO usuario  = new UsuarioVO();

String json;
if(request.getParameter("json").isEmpty()){
    json = (String) request.getAttribute("json").toString();
}else{
    json = (String) request.getParameter("json").toString();
}

new com.addcel.utils.U().Display("Inicio INSERT USUARIO... " );
new com.addcel.utils.U().Display("CADENA userInsert " + json);
//String p=usuario.getPassword();
String p=usuario.getUsr_pwd();

String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
key = Cad[0];
json = Cad[1];
p = Cad[2];

//new com.addcel.utils.U().Display("CADENA key " + key);
//new com.addcel.utils.U().Display("CADENA json Encript: " + json);

Gson gson = new Gson();
Type collectionType = new TypeToken<Usuario>(){}.getType();
usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
//usuario.setPassword(p);
usuario.setUsr_pwd(p);
//usuario.setPassword(Crypto.aesEncrypt(key, usuario.getPassword()));
//usuario.setPassword(Crypto.sha1(usuario.getPassword()));
//new com.addcel.utils.U().Display("CADENA json  " + Crypto.aesDecrypt(key, json.replace(" ", "+")));
//new com.addcel.utils.U().Display("CADENA json Des " + usuario.getImei());

//new com.addcel.utils.U().Display("REGISTRO USUARIO" );
new com.addcel.utils.U().Display("Tipo: " + usuario.getTipo() );
new com.addcel.utils.U().Display("Software: " + usuario.getSoftware());
new com.addcel.utils.U().Display("Modelo: " + usuario.getModelo());
//new com.addcel.utils.U().Display("Key: " + usuario.getKey());
new com.addcel.utils.U().Display("Imei: " + usuario.getImei());
new com.addcel.utils.U().Display("NOMBRE: " + usuario.getUsr_nombre() + " " + usuario.getUsr_apellido()+ " " + usuario.getUsr_materno());

if(!new UsuarioDAO().existeUsuario(usuario)){
    
    if(new UsuarioDAO().ExisteMail(usuario.geteMail())){
       respuesta.setResultado(3);
       respuesta.setMensaje("El Correo Electronico ya fue asignado a otro usuario. Intente de nuevo.");

    }else if(new UsuarioDAO().ExisteT(usuario.getUsr_tdc_numero())){
        respuesta.setResultado(4);
        respuesta.setMensaje("La tarjeta proporcionada ya este asignada a otro usuario. Intente de nuevo.");

    }else if(new UsuarioDAO().ExisteIMEI(usuario.getImei())){
        respuesta.setResultado(5);
        respuesta.setMensaje("No es posible registrar el usuario.");
        new com.addcel.utils.U().Display("ERR-00 -> El IMEI ya se encuentra registrado en la base de datos. IMEI= " + usuario.getImei() + "   Usuario = " + usuario.getUsr_login());

    }else if(new UsuarioDAO().ExisteTelefono(usuario.getUsr_telefono())){
        respuesta.setResultado(5);
        respuesta.setMensaje("El telefono ya esta asignado a otro usuario. Intente de nuevo.");
    
    }else if(new UsuarioDAO().add(usuario,p)){
        respuesta.setResultado(1);
        if(usuario.getTipo().toUpperCase().equals("IPAD")  || usuario.getTipo().toUpperCase().equals("IPOD") ){
              respuesta.setMensaje("EL sistema te envio un EMAIL con la clave de acceso para completar tu registro.");
        }else{
              respuesta.setMensaje("EL sistema te envio un SMS o un EMAIL con la clave de acceso para completar tu registro.");
        }                 
    }else{
       respuesta.setResultado(2);
       respuesta.setMensaje("Ha ocurrido un error, intentelo nuevamente");
    }

}else{
    respuesta.setResultado(2);
    respuesta.setMensaje("El usuario ya existe");
}
  resp = gson.toJson(respuesta);
  new com.addcel.utils.U().Display("Resuesta UserInsert: " + resp );
  resp = Crypto.aesEncrypt(key, resp);
%>
<%=resp%>
