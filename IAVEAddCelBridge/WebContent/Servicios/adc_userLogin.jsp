<%-- 
    Document   : user_login
    Created on : Feb 12, 2011
    Author     : -
--%>

<%@page import="crypto.Crypto"%>
<%@page import="com.ironbit.addcell.integration.UsuarioDAO"%>
<%@page import="com.google.gson.reflect.TypeToken"%>
<%@page import="java.lang.reflect.Type"%>
<%@page import="com.ironbit.addcell.bean.Usuario"%>
<%@page import="com.addcel.mx.antad.servicios.model.vo.UsuarioVO"%>
<%@page import="com.google.gson.Gson"%>
<%@page import="com.ironbit.addcell.bean.Respuesta"%>
<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
new com.addcel.utils.U().Display("**********************************   LOGIN");

String key = "1234567890ABCDEF0123456789ABCDEF";
String resp = null;
Respuesta respuesta = new Respuesta();
UsuarioVO usuario  = null;
String json;
if(request.getParameter("json").isEmpty()){
    json = (String) request.getAttribute("json").toString();
}else{
    json = (String) request.getParameter("json").toString();
}

new com.addcel.utils.U().Display("CADENA userLogin: " + json);

Gson gson = new Gson();

try{
    String [] Cad = new com.ironbit.addcell.integration.ParametroDAO().Asf(json);
    key = Cad[0];
    json = Cad[1];
    
    Type collectionType = new TypeToken<Usuario>(){}.getType();
    usuario = gson.fromJson(Crypto.aesDecrypt(key, json.replace(" ", "+")), collectionType);
}catch(Exception e){
    new com.addcel.utils.U().Display("ERROR al desifrar JSON: " + e.getMessage());
    respuesta.setResultado(2);
    respuesta.setMensaje("Usuario Invalido. Verifique el usuario y/o password");
}

if(usuario != null){
    //new com.addcel.utils.U().Display("CADENA USUARIO 1: " + usuario.getPassword());

    //usuario.setPassword(Crypto.aesEncrypt(key,  usuario.getPassword() ));
    usuario.setUsr_pwd(Crypto.aesEncrypt(key,  usuario.getUsr_pwd() ));

    new com.addcel.utils.U().Display("Login: " + usuario.getUsr_login() );
    //new com.addcel.utils.U().Display("Usuario: " + usuario.getUsuario() );
    //new com.addcel.utils.U().Display("Tipo: " + usuario.getTipo() );
    //new com.addcel.utils.U().Display("Software: " + usuario.getSoftware());
    //new com.addcel.utils.U().Display("Modelo: " + usuario.getModelo());
    //new com.addcel.utils.U().Display("Key: " + usuario.getKey());
    //new com.addcel.utils.U().Display("Imei: " + usuario.getImei());
    //new com.addcel.utils.U().Display("CADENA USUARIO 2, Usuario:" + usuario.getNombre() + " *** Password: " + usuario.getPassword());
    new com.addcel.utils.U().Display("CADENA USUARIO 2, Usuario:" + usuario.getUsr_nombre() + " *** Password: " + usuario.getUsr_pwd());

    UsuarioVO RU = new UsuarioDAO().getUsuario(usuario);
    if(RU != null && RU.getUsr_nombre() != null){
        //new com.addcel.utils.U().Display("CADENA USUARIO VALIDO " + usuario.getNombre());
        if(RU.getId_usr_status()== 0){
                respuesta.setResultado(3);
                respuesta.setMensaje("Usuario Inactivo. Consulte con el Administrador");
           }else if(RU.getId_usr_status() == 99){
                respuesta.setResultado(99);
                respuesta.setMensaje("Modifique su Informacion por seguridad.|" + RU.getUsr_login());
           }else if(RU.getId_usr_status() == 98){
                respuesta.setResultado(98);
                respuesta.setMensaje("Modifique su clave de acceso por seguridad.|" + RU.getUsr_login());
           } else{
                respuesta.setResultado(1);
                respuesta.setMensaje("Usuario Valido|" + RU.getUsr_login());
           }
    }else{
        respuesta.setResultado(2);
        respuesta.setMensaje("Usuario Invalido. Verifique el usuario y/o password");
    }
}
resp = gson.toJson(respuesta);
new com.addcel.utils.U().Display("Resuesta LOGIN: " + resp );
resp = Crypto.aesEncrypt(key, resp);
%>
<%=resp%>