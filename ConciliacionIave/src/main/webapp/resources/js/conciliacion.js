/**
 * FUNCIONES DE VALIDACION
 */

function checkForm(form) {
	console.log("Entrando a checkForm");
	console.log(form.txtFecha.value);
	
	if(!checkDate(form.txtFecha)) {
		console.log("Error en validacion");
		return false;
	}

	console.log("Validacion exitosa");
	return true;
}

function checkFormEdocuenta(form) {
	console.log("Entrando a checkForms");
	console.log(form.txtFecIni.value);
	console.log(form.txtFecFin.value);
	
	if(!checkDates(form.txtFecIni, form.txtFecFin)) {
		console.log("Error en validacion");
		return false;
	}

	console.log("Validacion exitosa");
	return true;
}

function checkDates(fieldInicio, fieldFin) {
	return checkDate(fieldInicio) && checkDate(fieldFin);
}

function checkDate(field) {
	var allowBlank = false;
	var minYear = 2011;
	var maxYear = (new Date()).getFullYear();

	var errorMsg = "";

	// regular expression to match required date format
	re = /^(\d{4})\/(\d{1,2})\/(\d{1,2})$/;

	console.log(field.value);
	
	if(field.value != '') {
		console.log("Campo no vacio");
		if(regs = field.value.match(re)) {
			if(regs[3] < 1 || regs[3] > 31) {
				console.log("Día no v&acute;lido");
				errorMsg = "Día no valido: " + regs[1];
			} else if(regs[2] < 1 || regs[2] > 12) {
				console.log("Mes no v&acute;lido");
				errorMsg = "Mes no valido: " + regs[2];
			} else if(regs[1] < minYear || regs[1] > maxYear) {
				console.log("A&ntilde;o no v&acute;lido");
				errorMsg = "Año no valido: " + regs[3] + " - must be between " + minYear + " and " + maxYear;
			}
		} else {
			console.log("Formato de fecha no valido");
			errorMsg = "Formato de fecha no valido: " + field.value;
		}
	} else if(!allowBlank) {
		console.log("Campo vacio");
		errorMsg = "Ingresa una fecha valida";
	}

	if(errorMsg != "") {
		alert(errorMsg);
		field.focus();
		return false;
	}

	return true;
}

function goBack() {
    window.history.back();
}

function loadError(error) {
	document.getElementsByClassName("error")[0].innerHTML
}

