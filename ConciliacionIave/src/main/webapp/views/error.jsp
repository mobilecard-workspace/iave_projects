<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style type="text/css">
body {
	background: none repeat scroll 0 0 #FFFFFF;
	font-family: 'Open Sans', sans-serif;
}

table.info_description {
	height: auto;
	width: 620px;
	position: relative;
}

table.info_description td {
	height: auto;
	color: #DF1D44;
	font-size: 30px;
	line-height: 32px;
	text-align: center;
	display: inline-block;
	vertical-align: top;
	*display: inline;
	*zoom: 1;
}

section.helpers {
	background: none repeat scroll 0 0 #FFFFFF;
	width: 940px;
	height: auto;
	position: relative;
	padding: 26px 0 0;
	margin: 0 auto;
	vertical-align: center;
	color: #000000;
	font-weight: bold;
	text-transform: uppercase;
	font-size: 12px;
	line-height: 14px;
	text-align: center;
}

section.helpers div.extra_text_helpers {
	margin: 44px auto 20px;
}

section.helpers div.footer_text_helpers {
	margin: 0 0 0 0;
}
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>No se pudo generar el archivo solicitado</title>
<script>
function goBack() {
    window.history.back();
}
</script>
</head>
<body>
<body>
	<table class="info_description" align="center">
		<tbody>
			<tr>
				<td><div>${mensajeError}</div></td>

				<td><BR>
				<BR>Presione el boton Atras para terminar.</td>
			</tr>
		</tbody>
	</table>
</body>
<button onclick="goBack()">Intenta de nuevo</button>
</body>
</html>