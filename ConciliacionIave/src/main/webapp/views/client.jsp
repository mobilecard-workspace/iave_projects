<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Estado de Cuenta MobileCard</title>
		<!-- Meta Tags -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

		<!--
			    response.setHeader("Expires","0");
		    response.setHeader("Pragma","no-cache");
		    response.setDateHeader("Expires",-1);
		
		 -->
		<style type="text/css">
			* {
				padding: 0;
				margin: 0;
			}
			
			body {
				font-size: 62.5%;
				background-color: rgb(255, 255, 255);
				font-family: verdana, arial, sans-serif;
			}
			
			.page-container {
				width: 1458px;
				margin: 0px auto;
				margin-top: 10px;
				margin-bottom: 10px;
				border: solid 1px rgb(150, 150, 150);
				font-size: 1.0em;
			}
			
			/* HEADER */
			.header {
				width: 1458px;
				height: 170px;
				font-family: "trebuchet ms", arial, sans-serif;
				overflow: visible !important /*Firefox*/;
				overflow: hidden /*IE6*/;
			}
			
			.main-content {
				display: inline; /*Fix IE floating margin bug*/;
				float: left;
				width: 1400px;
				margin: 0 0 0 30px;
				overflow: visible !important /*Firefox*/;
				overflow: hidden /*IE6*/;
				margin-bottom: 10px !important /*Non-IE6*/;
				margin-bottom: 5px /*IE6*/;
			}
			
			/* MAIN CONTENT */
			.main-content h1.pagetitle {
				margin: 0 0 0.4em 0;
				padding: 0 0 2px 0;
				font-family: Verdana, Geneva, sans-serif;
				color: #000000;
				font-weight: bold;
				font-size: 180%;
			}
			
			.main-content p {
				margin: 0 0 1.0em 0;
				line-height: 1.5em;
				font-size: 120%;
				text-align: justify;
				color: #000000;
			}
			
			.main-content table {
				clear: both;
				/*width: 100%;*/
				margin: 0 0 0 0;
				table-layout: fixed;
				border-collapse: collapse;
				empty-cells: show;
				background-color: rgb(233, 232, 244);
				text-align: justify;
			}
			
			.main-content table td {
				height: 2.5em;
				padding: 2px 5px 2px 5px;
				border-left: solid 2px rgb(255, 255, 255);
				border-right: solid 2px rgb(255, 255, 255);
				border-top: solid 2px rgb(255, 255, 255);
				border-bottom: solid 2px rgb(255, 255, 255);
				background-color: rgb(225, 225, 225);
				text-align: center;
				font-weight: normal;
				color: #000000;
				font-size: 11.5px;
			}
			
			/*  FOOTER SECTION  */
			.footer {
				clear: both;
				width: 1458px;
				height: 7em;
				padding: 1.1em 0 0;
				background: rgb(225, 225, 225);
				font-size: 1.0em;
				overflow: visible !important /*Firefox*/;
				overflow: hidden /*IE6*/;
			}
			
			.footer p {
				line-height: 1.3em;
				text-align: center;
				color: rgb(125, 125, 125);
				font-weight: bold;
				font-size: 104%;
			}
		</style>
		<script type="text/javascript" src="<c:url value="/resources/js/conciliacion.js"/>"></script>
	</head>
<body>
	<div class="page-container">
		<div class="header">
	        <img src="<c:url value="/resources/images/mobilecard_header.png"/>" alt="Powered by mobilecard" />
	    </div>
		<br>

		<div class="main-content">

			<h1 class="pagetitle">Generar Reportes:</h1>
			<h3>&nbsp;</h3>

			<form name="form1" id="form1" action="https://www.mobilecard.mx/ConciliacionIave/createEdocuenta"
				method="GET" onsubmit="return checkFormEdocuenta(this);">
				<table width="400px">
					<tr>
						<td colspan="2">Estado de cuenta.</td>
					</tr>
					<tr>
						<td>Fecha Inicial (aaaa/mm/dd):</td>
						<td><input id="txtFecIni" name="txtFecIni" maxlength="10"></input>
						</td>
					</tr>
					<tr>
						<td>Fecha Final (aaaa/mm/dd):</td>
						<td><input id="txtFecFin" name="txtFecFin" maxlength="10"></input>
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit"></input></td>
					</tr>
				</table>
			</form>
			<br />
			<br />
			<form name="form2" id="form1" action="https://www.mobilecard.mx/ConciliacionIave/createArchivo" 
			onsubmit="return checkForm(this);" method="GET">

				<table width="400px">
					<tr>
						<td colspan="2">Generaci&oacute;n del Archivo</td>
					</tr>
					<tr>
						<td colspan="2"><select id="tipoarchivo" name="tipoarchivo">
								<option>ADDCEL</option>
								<option>TUTAG</option>
						</select></td>
					</tr>
					<tr>
						<td>Fecha (aaaa/mm/dd):</td>
						<td><input id="txtFecha" name="txtFecha" maxlength="10" placeholder="aaaa/mm/dd"></input>
						</td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit"></input></td>
					</tr>
				</table>
			</form>
			<h3>&nbsp;</h3>

		</div>
		<div class="footer">
			<p>MobileCard | Todos los derechos reservados.</p>
			<p>Powered by Addcel.</p>
		</div>
	</div>
</body>
</html>
