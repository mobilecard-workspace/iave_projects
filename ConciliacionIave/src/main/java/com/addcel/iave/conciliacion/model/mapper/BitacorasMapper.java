package com.addcel.iave.conciliacion.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.iave.conciliacion.model.vo.EdocuentaVO;
import com.addcel.iave.conciliacion.model.vo.RegistroVO;

public interface BitacorasMapper {
	
	
	public List<RegistroVO> buscarPagosAddcel(String fecha);
	public List<RegistroVO> buscarPagosId(String fecha);
	public List<RegistroVO> buscarPagosLegacy(String fecha);
	public List<EdocuentaVO> buscarEdocuenta(@Param("fechaIni") String fechaIni, @Param("fechaFin") String fechaFin);
}
