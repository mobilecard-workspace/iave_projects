package com.addcel.iave.conciliacion.services;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.addcel.android.crypto.AddcelCrypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.iave.conciliacion.model.mapper.BitacorasMapper;
import com.addcel.iave.conciliacion.model.vo.EdocuentaVO;
import com.addcel.iave.conciliacion.model.vo.RegistroVO;
import com.google.common.base.Strings;
import com.google.common.primitives.Longs;

@Service
public class ConciliacionService {

	private static final Logger logger = LoggerFactory.getLogger(ConciliacionService.class);

	@Autowired
	private BitacorasMapper bitMapper;

	public void buildAddcelFile(String fecha, HttpServletRequest request, HttpServletResponse response) {

		logger.debug("Iniciando construccion de archivo addcel en buildAddcelFile");

		List<RegistroVO> registros;
		StringBuffer cadena = null;
		OutputStream outStream = null;
		String[] fechaArr = fecha.split("/");
		registros = bitMapper.buscarPagosAddcel(fechaArr[0] + "-" + fechaArr[1] + "-" + fechaArr[2]);

		cadena = buildFileString(registros, fecha);
		String fileName = "ADDCEL_" + fechaArr[2] + fechaArr[1] + fechaArr[0] + ".txt";

		try {
			// set content attributes for the response
			response.setContentType("text/plain");
			//	        response.setContentLength(cadena.length());

			// set headers for the response
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

			// get output stream of the response
			outStream = response.getOutputStream();

			// write bytes read from the input stream into the output stream
			outStream.write(cadena.toString().getBytes());

			logger.debug("Archivo addcel construido exitosamente en buildAddcelFile");

		} catch (IOException e) {
			logger.error("Ocurrio un error general guardar el archivo: {}", e.getMessage());
		}

	}

	public void buildTuTagFile(String fecha, HttpServletRequest request, HttpServletResponse response) {

		logger.debug("Iniciando construccion de archivo tutag en buildTuTagFile");

		List<RegistroVO> registros;
		StringBuffer cadena = null;
		OutputStream outStream = null;
		String[] fechaArr = fecha.split("/");
		registros = bitMapper.buscarPagosId(fechaArr[0] + "-" + fechaArr[1] + "-" + fechaArr[2]);

		cadena = buildFileString(registros, fecha);

		String fileName = "TUTAG_" + fechaArr[2] + fechaArr[1] + fechaArr[0] + ".txt";

		try {
			// set content attributes for the response
			response.setContentType("text/plain");
			//	        response.setContentLength(cadena.length());

			// set headers for the response
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

			// get output stream of the response
			outStream = response.getOutputStream();

			// write bytes read from the input stream into the output stream
			outStream.write(cadena.toString().getBytes());

			logger.debug("Archivo tutag construido exitosamente en buildTuTagFile");

		} catch (IOException e) {
			logger.error("Ocurrio un error general guardar el archivo: {}", e.getMessage());
		}

	}
	
	/*
	 * PARA CONSTRUIR ARCHIVOS CON FECHAS ERRONEAS
	 */
	public void buildTuTagLegacyFile(String fecha, HttpServletRequest request, HttpServletResponse response) {

		logger.debug("Iniciando construccion de archivo tutag en buildTuTagFile");

		List<RegistroVO> registros;
		StringBuffer cadena = null;
		OutputStream outStream = null;
		String[] fechaArr = fecha.split("/");
		registros = bitMapper.buscarPagosLegacy(fechaArr[0] + "-" + fechaArr[1] + "-" + fechaArr[2]);

		cadena = buildFileString(registros, fecha);

		String fileName = "TUTAG_" + fechaArr[2] + fechaArr[1] + fechaArr[0] + ".txt";

		try {
			// set content attributes for the response
			response.setContentType("text/plain");
			//	        response.setContentLength(cadena.length());

			// set headers for the response
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName));

			// get output stream of the response
			outStream = response.getOutputStream();

			// write bytes read from the input stream into the output stream
			outStream.write(cadena.toString().getBytes());

			logger.debug("Archivo tutag construido exitosamente en buildTuTagFile");

		} catch (IOException e) {
			logger.error("Ocurrio un error general guardar el archivo: {}", e.getMessage());
		}

	}
	
	

	public ModelAndView getEdoCuenta(String fechaIni, String fechaFin) {
		
		logger.debug("Iniciando obtencion de edocuenta en getEdoCuenta: {} {}", fechaIni, fechaFin);
		
		String cadena = "";
		ModelAndView mav = new ModelAndView("edo_cuenta");
		mav.addObject("txtFecIni", fechaIni);
		mav.addObject("txtFecFin", fechaFin);
		
		List<EdocuentaVO> edoCuenta = bitMapper.buscarEdocuenta(fechaIni, fechaFin);
		
		if (edoCuenta != null && !edoCuenta.isEmpty()) {
			try {
				cadena = buildEdocuentaString(edoCuenta).toString();
			} catch (ClassNotFoundException e) {
				logger.error("Ocurrio un error: {}", e);
			}
		}
		
		mav.addObject("Cadena", cadena);
		
		return mav;

	}
	
	private StringBuffer buildFileString(List<RegistroVO> registros, String fecha) {

		StringBuffer cadena = new StringBuffer();

		String[] tmp = fecha.split("/");

		cadena.append( "HDR" ).append( tmp[2] ).append( tmp[1] ).append( tmp[0] ).append( "\n" );
		int consecu = 0;
		double total = 0;

		String[] fec = null;
		String hor = null;
		String tipo = null;
		String tag = null;
		String cargo = null;
		String Cargo = null;
		String[] Aut = null;

		try {

			for (RegistroVO registro : registros) {
				consecu += 1;

				fec = registro.getBit_fecha().split("-");
				hor = registro.getBit_hora().replace(":", "").trim();
				tag = registro.getDestino();
				cargo = String.valueOf(registro.getBit_cargo());
				Cargo = Ceros(cargo.replace(".", "0"), 12);
				Aut = registro.getNum_TarjetaIAVE_autorizacion().split("-");

				total = total + registro.getBit_cargo();


				if (registro.getDestino().length() == 11) {
					tipo = "";
					tag = tag + " " + " " + " ";
				} else {
					
					tag = tag + " " + " ";
					tag = Strings.padStart(tag, 10, '0');
					
					long destinoAsLong = Longs.tryParse(registro.getDestino());
					
					//int primerCharEnTagAsInt = Ints.tryParse(registro.getDestino().substring(0,1));
					
					//if (primerCharEnTagAsInt <= 1)
					if (destinoAsLong < 20000000)
						tipo = "CPFI";
					else
						tipo = "IMDM";
					
				}


				cadena.append( "REG" ).append( Ceros(String.valueOf(consecu), 6) )
				.append( fec[2].trim() ).append( fec[1].trim() ).append( fec[0].trim() ).append( hor )
				.append( tipo ).append( tag ).append( Cargo ).append( "0000" )
				.append( Ceros(Aut[0], 5) ).append( Ceros(Aut[1], 6) ).append( "\n" );
			}

			cadena.append( "TRL" ).append( Ceros(String.valueOf(consecu), 6) ).append( Ceros( String.valueOf(total).replace(".", "0"), 12) ).append( "\n" );
		} catch (Exception e) {
			logger.error("Ocurrio un error: {}", e);
			cadena = new StringBuffer("");
		}

		return cadena;
	}

	private String buildEdocuentaString(List<EdocuentaVO> edoCuenta) throws ClassNotFoundException {

		StringBuffer Cadena = new StringBuffer();
		String Tarjeta = null;
		String Tar2 = null;
		String autIAVE = null;
		int totalEdoCuenta = 0;

		try {

			Cadena.append("<TR>");
			Cadena.append("<TD><strong>ID BITACORA</strong></TD>");
			Cadena.append("<TD><strong>ID USUARIO</strong></TD>");

			Cadena.append("<TD><strong>NOMBRE</strong></TD>");
			Cadena.append("<TD><strong>APELLIDO</strong></TD>");

			Cadena.append("<TD><strong>NUMERO DE TARJETA</strong></TD>");
			Cadena.append("<TD><strong>FECHA</strong></TD>");
			Cadena.append("<TD><strong>HORA</strong></TD>");
			Cadena.append("<TD width=\"250px\"><strong>CONCEPTO</strong></TD>");
			Cadena.append("<TD><strong>MONTO</strong></TD>");
            Cadena.append("<TD><strong>STATUS TRANSACCION</strong></TD>");
			Cadena.append("<TD><strong>CODIGO ERROR</strong></TD>");
			Cadena.append("<TD><strong>CARGO APLICADO</strong></TD>");

			Cadena.append("<TD><strong>NUM AUTORIZACION TAE</strong></TD>");
			Cadena.append("<TD><strong>NUM AUTORIZACION TELEPEAJE</strong></TD>");

			Cadena.append("<TD><strong>AUTORIZACION BANCARIA</strong></TD>");
			Cadena.append("</TR>");

			for (EdocuentaVO edocuentaVO : edoCuenta) {

				totalEdoCuenta++;
				Cadena.append("<TR><TD>");
				Cadena.append(edocuentaVO.getId_bitacora());
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getId_usuario());
				Cadena.append("</TD><TD>");

				Cadena.append(edocuentaVO.getUsr_nombre());
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getUsr_apellido());
			
				Tarjeta = AddcelCrypto.decryptCard(edocuentaVO.getTarjeta_compra());
				
				if (Strings.isNullOrEmpty(Tarjeta)) {
					Tar2 = "S/T";
				} else if (Tarjeta.equals("null")) {
					Tar2 = "";
				} else  {
					switch (Tarjeta.length()) {
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
						Tar2 =  "************" + Tarjeta;
						break;
					case 15:
						Tar2 = Tarjeta.substring(0, 5) + "******" + Tarjeta.substring(11);
						break;
					case 16:
						Tar2 = Tarjeta.substring(0, 5) + "******" + Tarjeta.substring(12);
						break;
					default:
						Tar2 = "";
						break;
					}
				}
				Cadena.append("</TD><TD>");
				Cadena.append(Tar2);
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getBit_fecha());
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getBit_hora());
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getBit_concepto());
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getBit_cargo());
                Cadena.append("</TD><TD>");
                Cadena.append(edocuentaVO.getBit_status());
				Cadena.append("</TD><TD>");
				Cadena.append(edocuentaVO.getBit_codigo_error());
				Cadena.append("</TD><TD>");
				if (edocuentaVO.getCargo_Aplicado() <= 0) {
					Cadena.append("&nbsp;");
				} else {
					Cadena.append(edocuentaVO.getCargo_Aplicado());
				}
				Cadena.append("</TD><TD>");
				if (edocuentaVO.getNumTae() == null) {
					Cadena.append("&nbsp;");
				} else {
					Cadena.append(edocuentaVO.getNumTae());
				}
				Cadena.append("</TD><TD>");
				try
				{
					autIAVE = edocuentaVO.getNum_TarjetaIAVE_autorizacion();
				}
				catch (Exception exx)
				{
					autIAVE = edocuentaVO.getNum_TarjetaIAVE_autorizacion_1();
				}
				if (autIAVE == null) {
					Cadena.append("&nbsp;");
				} else {
					Cadena.append(autIAVE);
				}
				Cadena.append("</TD><TD>");
				if (edocuentaVO.getAutorizacion() == null) {
					Cadena.append("&nbsp;");
				} else {
					Cadena.append(edocuentaVO.getAutorizacion());
				}
				Cadena.append("</TD></TR>");
			}
			logger.debug("CADENA ESTADO DE CUENTA, Total Registros: " + totalEdoCuenta);
		} catch (Exception ex) {
			logger.error("Ocurrio un error: {}", ex);
			Cadena = new StringBuffer(ex.toString());
		}
		
		return Cadena.toString();
	}

	public ModelAndView getError(String error) {
		ModelAndView model = new ModelAndView("error");
		model.addObject("mensajeError", error);

		return model;
	}

	private String Ceros(String cad, int NumCeros) {
		String tmp = "00000000000000000000000000000";
		return (tmp + cad).substring((tmp + cad).length() - NumCeros, (tmp + cad).length());
	}

}
