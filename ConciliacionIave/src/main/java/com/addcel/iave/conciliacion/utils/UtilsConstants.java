package com.addcel.iave.conciliacion.utils;


public final class UtilsConstants {
	
	private UtilsConstants() {
		
	}
	
	public static String ERROR_REGISTROS_VACIO = "No existen registros para la fecha y proveedor especificados.";
	public static String ERROR_DEFAULT = "Hubo un error al procesar tu petición. Intenta de nuevo";
	
	public static String CADENA_CEROS = "00000000000000000000000000000";
}
