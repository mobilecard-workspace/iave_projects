
package com.addcel.iave.conciliacion.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.iave.conciliacion.services.ConciliacionService;

@Controller
public class ConciliacionController {
	
	private static final Logger logger = LoggerFactory.getLogger(ConciliacionController.class);
	
	
	@Autowired
	private ConciliacionService addcelService;
	
	//"https://www.mobilecard.mx:8443/AddCelBridge/Servicios/EstadoCuentaResArchivo.jsp?
	//tipoarchivo=IAVE+TuTag&txtFecha=2016%2F04%2F25".
	
	@RequestMapping(value = "/client")
	public ModelAndView index() {
	  return new ModelAndView("client");
	}
	
	@RequestMapping(value = "/createArchivo", method = {RequestMethod.POST, RequestMethod.GET})
	public void createArchivo(@RequestParam("txtFecha") String fecha, @RequestParam("tipoarchivo") String tipoArchivo,
			HttpServletRequest request, HttpServletResponse response){
		
		logger.debug("Recibiendo peticion en /createArchivo");
		
		try{
			if (tipoArchivo.equals("ADDCEL"))
				addcelService.buildAddcelFile(fecha, request, response);
			else if (tipoArchivo.equals("TUTAG"))
				addcelService.buildTuTagFile(fecha, request, response);
			else if (tipoArchivo.equals("TUTAG_CORRECCION"))
				addcelService.buildTuTagLegacyFile(fecha, request, response);
			
			logger.debug(" Archivo generado exitosamente con /createArchivo");
			
		}catch(Exception e){
			logger.error("Ocurrio un error general en el servicio /createArchivo: {}", e );
		}
	}
	
	@RequestMapping(value = "/createEdocuenta", method = {RequestMethod.POST, RequestMethod.GET})
	public ModelAndView createEdocuenta(@RequestParam("txtFecIni") String fechaIni, 
			@RequestParam("txtFecFin") String fechaFin) {
		
		logger.debug("Recibiendo peticion en /createEdocuenta");
		
		return addcelService.getEdoCuenta(fechaIni, fechaFin);
		
		
	}
	
}