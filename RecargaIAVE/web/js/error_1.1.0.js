var errorPanel;
Ext.application({
    launch: function() {
        errorPanel = Ext.create('Ext.form.Panel', {
            fullscreen: true,
            itemId: 'errorGeneral',
            scrollable: 'vertical',
            items: [{
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Error',
                    style: 'background:#09569b',
                    items: [{
                            xtype: 'button',
                            align: 'left',
                            //ui: 'decline-small',
                            //icon: 'images/cancel.svg',
                            //iconCls: 'svg-image',
                            iconAlign: 'center',
                            iconCls: 'delete',
                            iconMask: true,
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE/close.html";
                            }
                        }]
                }, {
                    xtype: 'panel',
                    styleHtmlContent: true,
                    html: '<center>' + error + '<br/><br/>Seleccione nuevamente su tag</center>',
                    height: '100%',
                    width: '100%',
                    ui: 'dark'
                }, {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    title: 'Powered by Mobilecard &reg;',                    
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b',
                    listeners: [
                        {
                            fn: function(element, options) {
                                Ext.getCmp(element.id).titleComponent.innerElement.dom.style.color = 'white';
                                Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontSize = '10px';
                                //Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontStyle = 'italic';
                            },
                            event: 'painted'
                        }
                    ]
                }]
        }); // create()
    } // launch
}); // application()

