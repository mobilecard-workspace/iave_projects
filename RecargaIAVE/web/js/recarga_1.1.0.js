var pago;
var regargaForm;
Ext.application({
    launch: function() {
      //define Model que se enviará desde el form
        regargaForm = Ext.define('recargaModel', {
            extend: 'Ext.data.Model',
            //asigna los tipos de dato de cada atributo
            config: {
                fields: [{
                        name: 'tagAlias',
                        type: 'string'
                    }, {
                        name: 'tag',
                        type: 'string'
                    }, {
                        name: 'monto',
                        type: 'string'
                    }, {
                        name: 'passMobile',
                        type: 'string'
                    }, {
                        name: 'cvv2',
                        type: 'string'
                    }], // fields
                validations: [{
                        field: 'tagAlias',
                        type: 'presence',
                        message: 'El alias del tag es requerido'
                    }, {
                        field: 'tag',
                        type: 'presence',
                        message: 'El tag es requerido'
                    }, {
                        field: 'monto',
                        type: 'presence',
                        message: 'El monto de la recarga es requerido'
                    }, {
                        field: 'passMobile',
                        type: 'presence',
                        message: 'El password MobileCard es requerido'
                    }, {
                        field: 'passMobile',
                        type: 'length',
                        min: 8,
                        message: 'El password MobileCard no es valido'
                    }, {
                        field: 'cvv2',
                        type: 'presence',
                        message: 'El cvv2 es requerido'
                    }, {
                        field: 'cvv2',
                        type: 'length',
                        min: 3,
                        max: 4,
                        message: 'cvv2 invalido.'
                    }
                ] // validations
            } // config
        }); // define()

        pago = Ext.create('Ext.form.Panel', {
            fullscreen: true,
            itemId: 'datosPago',
            scrollable: 'vertical',
            masked: {
                xtype: 'loadmask',
                message: 'Dando seguridad al canal... ' +
                        '<hr>Espere porfavor...'
            },
            items: [{
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Pago',
                    style: 'background:#09569b',
                    items: [{
                            xtype: 'button',
                            align: 'left',
                            //ui: 'decline-small',
                            //icon: 'images/cancel.svg',
                            //iconCls: 'svg-image',
                            iconAlign: 'center',
                            iconCls: 'delete',
                            iconMask: true,
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE/close.html";
                            }
                        }]
                }, {
                    xtype: 'fieldset',
                    items: [
                        {
                            xtype: 'panel',
                            html: '<b><i><font size="3" color="#0000FF">Todas tus recargas prepago IAVE tendr&aacute;n una comisi&oacute;n de $ 8.00 con IVA INCLUIDO</font></i></b>'
                        },
                        {
                            xtype: 'textfield',
                            label: 'Alias del TAG:',
                            labelAlign: 'top',
                            name: 'tagAlias',
                            id: 'tagAlias',
                            readOnly: true,
                            value: new String(aliasTag)
                        },
                        {
                            xtype: 'textfield',
                            label: 'TAG:',
                            labelAlign: 'top',
                            name: 'tag',
                            id: 'tag',
                            readOnly: true,
                            value: new String(tag)
                        },
                        {
                            xtype: 'selectfield',
                            label: 'Monto:',
                            labelAlign: 'top',
                            name: 'monto',
                            id: 'monto',
                            displayField: 'descripcion',
                            valueField: 'claveWS',
                            options: productos.productos
                        },
                        {
                            xtype: 'passwordfield',
                            label: 'Password MobileCard:',
                            labelAlign: 'top',
                            name: 'passMobile',
                            id: 'passMobile'
                        },
                        {
                            xtype: 'passwordfield',
                            label: 'CVV2:',
                            labelAlign: 'top',
                            name: 'cvv2',
                            id: 'cvv2'
                        },
                        {
                            xtype: 'button',
                            text: 'Comprar',
                            id: 'btnComprar',
                            handler: function(btn, evt) {
                                Ext.getCmp("btnComprar").disable();
                                Ext.getCmp("btnCancelar").disable();
                                var recargaModel = Ext.create('recargaModel');
                                var errors, errorMessage = '';
                                pago.updateRecord(recargaModel);
                                errors = recargaModel.validate();
                                if (!errors.isValid()) {
                                    errors.each(function(err) {
                                        errorMessage += err.getMessage() + '<br/>';
                                    }); // each()
                                    Ext.Msg.alert('Datos incorrectos', errorMessage, function(btn) {
                                        Ext.getCmp("btnComprar").enable();
                                        Ext.getCmp("btnCancelar").enable();
                                    });
                                } else {
                                    pagoEnc.setValues({tagAliasEnc: $.jCryption.encrypt(Ext.getCmp("tagAlias").getValue(), password_),
                                        tagEnc: $.jCryption.encrypt(Ext.getCmp("tag").getValue(), password_),
                                        montoEnc: $.jCryption.encrypt(Ext.getCmp("monto").getValue(), password_),
                                        passMobileCardEnc: $.jCryption.encrypt(Ext.getCmp("passMobile").getValue(), password_),
                                        cvv2Enc: $.jCryption.encrypt(Ext.getCmp("cvv2").getValue(), password_),
                                        plataformaEnc: $.jCryption.encrypt(navigator.platform, password_)
                                    });
                                    pagoEnc.submit({url: 'recarga.do?method=realizaRecarga', method: 'POST'});
                                } // if
                            } // handler
                        }
                    ]
                }, {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    title: 'Powered by MobileCard &reg;',
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b',
                    listeners: [
                        {
                            fn: function(element, options) {
                                Ext.getCmp(element.id).titleComponent.innerElement.dom.style.color = 'white';
                                Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontSize = '10px';
                                //Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontStyle = 'italic';
                            },
                            event: 'painted'
                        }
                    ]
                }]
        }); // create()
        var pagoEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'tagAliasEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'tagEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'montoEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'passMobileCardEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'cvv2Enc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'plataformaEnc'
                }] // items (formpanel)
        }); // create()
        if (typeof mensajeExito !== "undefined") {
            Ext.Msg.alert("Registro con exito", mensajeExito);
        }
        if (typeof mensajeError !== "undefined") {
            Ext.Msg.alert(tituloError, mensajeError);
        }
    } // launch
}); // application()
