var confirmacion;
Ext.application({
    launch: function() {
        confirmacion = Ext.create('Ext.form.Panel', {
            fullscreen: true,
            itemId: 'confirmacionPago',
            scrollable: 'vertical',
            items: [{
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Recarga exitosa',
                    style: 'background:#09569b',
                    items: [{
                            xtype: 'button',
                            align: 'left',
                            //ui: 'decline-small',
                            //icon: 'images/cancel.svg',
                            //iconCls: 'svg-image',
                            iconAlign: 'center',
                            iconCls: 'delete',
                            iconMask: true,
                            id: 'btnCancelar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE/close.html";
                            }
                        }]
                }, {
                    xtype: 'fieldset',
                    items: [
                        {
                            xtype: 'textfield',
                            label: 'Alias Tag',
                            labelAlign: 'top',
                            value: aliasTag,
                            readOnly: true
                        },
                        {
                            xtype: 'textfield',
                            label: 'Tag',
                            labelAlign: 'top',
                            readOnly: true,
                            value: tag
                        },
                        {
                            xtype: 'textfield',
                            label: 'Mensaje',
                            labelAlign: 'top',
                            readOnly: true,
                            value: mensaje
                        },
                        {
                            xtype: 'textfield',
                            label: 'Autorizaci&oacute;n',
                            labelAlign: 'top',
                            readOnly: true,
                            value: autorizacion
                        }, {
                            xtype: 'button',
                            text: 'Finalizar',
                            handler: function(btn, evt) {
                                location.href = "http://www.mobilecard.mx:8080/RecargaIAVE/end.html";
                            } // handler
                        }
                    ]}, {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    styleHtmlContent: true,
                    title: 'Powered by MobileCard &reg;',
                    minHeight: '1.3em',
                    height: '1.3em',
                    style: 'background:#09569b',
                    listeners: [
                        {
                            fn: function(element, options) {
                                Ext.getCmp(element.id).titleComponent.innerElement.dom.style.color = 'white';
                                Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontSize = '10px';
                                //Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontStyle = 'italic';
                            },
                            event: 'painted'
                        }
                    ]
                }]
        }); // create()
    } // launch
}); // application()

