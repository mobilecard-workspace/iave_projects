var datosAdicionales;

Ext.application({
    launch: function() {
        ////Ext.getBody().dom.style.fontSize = String(resValue) + '%';
        Ext.applyIf(Ext.data.Validations, {
            emailValidation: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                return value === Ext.getCmp("confEmail").getValue();
            },
            numCelularValidation: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                return value === Ext.getCmp("confNumCelular").getValue();
            }
        });

        Ext.define('datosAdicionalesModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [
                    {
                        name: 'nombreUsuario',
                        type: 'string'
                    }, {
                        name: 'numCelular',
                        type: 'string'
                    }, {
                        name: 'confNumCelular',
                        type: 'string'
                    }, {
                        name: 'proveedor',
                        type: 'string'
                    }, {
                        name: 'email',
                        type: 'string'
                    }, {
                        name: 'confEmail',
                        type: 'string'
                    }, {
                        name: 'nombre',
                        type: 'string'
                    }, {
                        name: 'fechaNac',
                        type: Ext.data.Types.DATE
                    }, {
                        name: 'apellidoP',
                        type: 'string'
                    }, {
                        name: 'apellidoM',
                        type: 'string'
                    }, {
                        name: 'sexo',
                        type: 'string'
                    }, {
                        name: 'telefonoCasa',
                        type: 'string'
                    }, {
                        name: 'telefonoOficina',
                        type: 'string'
                    }
                ], // fields
                validations: [
                    {
                        field: 'nombreUsuario',
                        type: 'presence',
                        message: 'El login es requerido'
                    },
                    {
                        field: 'nombreUsuario',
                        type: 'format',
                        message: 'El login no es valido, use solo letras, n&uacute;meros y -._ sin espacios',
                        matcher: /^[0-9a-zA-Z_.-]{0,35}$/
                    }, {
                        field: 'nombreUsuario',
                        type: 'length',
                        min: 8,
                        max: 20,
                        message: 'El login debe tener por lo menos ocho caracteres'
                    },
                    {
                        field: 'numCelular',
                        type: 'presence',
                        message: 'El n&uacute;mero de celular es requerido'
                    }, {
                        field: 'confNumCelular',
                        type: 'presence',
                        message: 'La confirmaci&oacute;n de n&uacute;mero de celular es requerida'
                    }, {
                        field: 'numCelular',
                        type: 'length',
                        min: 10,
                        max: 10,
                        message: 'El n&uacute;mero de celular debe ser de diez digitos'
                    }, {
                        field: 'numCelular',
                        type: 'numCelularValidation',
                        message: 'El n&uacute;mero celular no coincide'
                    }, {
                        field: 'proveedor',
                        type: 'presence',
                        message: 'El proveedor es requerido'
                    }, {
                        field: 'email',
                        type: 'email',
                        message: 'El email no es un email valido'
                    }, {
                        field: 'email',
                        type: 'presence',
                        message: 'El email es requerido'
                    }, {
                        field: 'confEmail',
                        type: 'presence',
                        message: 'La confirmaci&oacute;n del email es requerida'
                    }, {
                        field: 'confEmail',
                        type: 'email',
                        message: 'La confirmaci&oacute;n del email no es un email valido'
                    }, {
                        field: 'email',
                        type: 'emailValidation',
                        message: 'El email no coincide'
                    }, {
                        field: 'nombre',
                        type: 'presence',
                        message: 'El nombre es requerido'
                    }, {
                        field: 'nombre',
                        type: 'format',
                        matcher: /^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1]{0,35}$/,
                        message: 'El nombre no tiene el formato adecuado, use solo letras'
                    }, {
                        field: 'apellidoP',
                        type: 'presence',
                        message: 'El apellido paterno es requerido'
                    }, {
                        field: 'apellidoP',
                        type: 'format',
                        matcher: /^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1]{0,45}$/,
                        message: 'El apellido paterno no tiene el formato adecuado, use solo letras'
                    }, {
                        field: 'apellidoM',
                        type: 'format',
                        matcher: /^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1]{0,45}$/,
                        message: 'El apellido materno no tiene el formato adecuado, use solo letras'
                    }, {
                        field: 'fechaNac',
                        type: 'presence',
                        message: 'La fecha de nacimiento es requerida'
                    }, {
                        field: 'sexo',
                        type: 'inclusion',
                        list: ['M', 'F'],
                        message: 'Debe seleccionar su sexo'
                    }, {
                        field: 'telefonoCasa',
                        type: 'format',
                        matcher: /(^$|^\d{10}$)/,
                        message: 'El telefono de casa debe tener diez digitos'
                    }, {
                        field: 'telefonoOficina',
                        type: 'format',
                        matcher: /(^$|^\d{10}$)/,
                        message: 'El telefono de oficina debe tener diez digitos'
                    }] // validations
            } // config
        }); // define()
        datosAdicionales = Ext.create(
                'Ext.form.FormPanel',
                {
                    fullscreen: true,
                    itemId: 'datosAdicionales',
                    scrollable: 'vertical',
                    standardSubmit: true,
                    masked: {
                        xtype: 'loadmask',
                        message: 'Dando seguridad al canal... ' +
                                '<hr>Espere porfavor...'
                    }, // masked
                    items: [
                        {
                            xtype: 'titlebar',
                            docked: 'top',
                            title: 'Datos Adicionales',
                            style: 'background:#09569b',
                            items: [{
                                    xtype: 'button',
                                    align: 'left',
                                    //ui: 'decline-small',
                                    //icon: 'images/cancel.svg',
                                    //iconCls: 'svg-image',
                                    iconAlign: 'center',
                                    iconCls: 'delete',
                                    iconMask: true,
                                    id: 'btnCancelar',
                                    handler: function(btn, evt) {
                                        location.href = "http://www.mobilecard.mx:8080/RecargaIAVE/close.html";
                                    }
                                }
                            ]
                        },
                        {
                            xtype: 'fieldset',
                            items: [
                                {
                                    xtype: 'textfield',
                                    label: 'Login',
                                    labelAlign: 'top',
                                    name: 'nombreUsuario',
                                    id: 'nombreUsuario',
                                    maxLength: 35,
                                    value: login,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("nombreUsuario").setValue(Ext.getCmp("nombreUsuario").getValue().trim());
                                        },
                                        change: function() {
                                            if (Ext.getCmp("nombreUsuario").getValue().length > 0) {
                                                if (/^[0-9a-zA-Z_.-]+$/.test(Ext.getCmp("nombreUsuario").getValue())) {
                                                    Ext.Viewport.mask({xtype: 'loadmask', message: 'Validando login'});
                                                    Ext.Ajax.request({
                                                        url: 'consultasAjax.do?method=validaLogin',
                                                        method: 'POST',
                                                        params: {login: $.jCryption.encrypt(Ext.getCmp("nombreUsuario").getValue(), password_)},
                                                        success: function(xhr) {
                                                            var data = Ext.JSON.decode($.jCryption.decrypt(xhr.responseText, password));
                                                            if (data.error === '0') {
                                                                if (data.valido === '0') {
                                                                    Ext.Msg.alert("Error", data.errorMensaje, function(btn) {
                                                                        datosAdicionales.setValues({nombreUsuario: ''});
                                                                    });
                                                                }
                                                            } else {
                                                                Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el login", function(btn) {
                                                                    datosAdicionales.setValues({nombreUsuario: ''});
                                                                });
                                                            }
                                                            Ext.Viewport.unmask();

                                                        },
                                                        failure: function(response, opts) {
                                                            Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el login", function(btn) {
                                                                datosAdicionales.setValues({nombreUsuario: ''});
                                                            });
                                                            Ext.Viewport.unmask();
                                                        }
                                                    });
                                                } else {
                                                    Ext.Msg.alert("Login no valido", "El login no es valido, use solo letras, numeros y -._", function(btn) {
                                                        datosAdicionales.setValues({nombreUsuario: ''});
                                                    });
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Numero de celular:',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    labelAlign: 'top',
                                    name: 'numCelular',
                                    id: 'numCelular',
                                    maxLength: 10,
                                    value: dn,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("numCelular").setValue(Ext.getCmp("numCelular").getValue().trim);
                                        },
                                        change: function() {
                                            var numC = Ext.getCmp("numCelular").getValue();
                                            if (numC.length !== 0) {
                                                if (/^[0-9]{10}$/.test(numC)) {
                                                    Ext.Viewport.mask({xtype: 'loadmask', message: 'Validando numero celular'});
                                                    Ext.Ajax.request({
                                                        url: 'consultasAjax.do?method=validaDn',
                                                        method: 'POST',
                                                        params: {dn: $.jCryption.encrypt(numC, password_)},
                                                        success: function(xhr) {
                                                            var data = Ext.JSON.decode($.jCryption.decrypt(xhr.responseText, password));
                                                            if (data.error === '0') {
                                                                if (data.valido === '0') {
                                                                    Ext.Msg.alert("Numero celular ocupado", "El numero celular ingresado ya esta en uso, seleccione la opci&oacute;n ya soy usuario MobileCard", function(btn) {
                                                                        datosAdicionales.setValues({numCelular: '', confNumCelular: ''});
                                                                    });
                                                                }
                                                            } else {
                                                                Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar numero de celular", function(btn) {
                                                                    datosAdicionales.setValues({numCelular: '', confNumCelular: ''});
                                                                });
                                                            }
                                                            Ext.Viewport.unmask();

                                                        },
                                                        failure: function(response, opts) {
                                                            Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar numero de celular", function(btn) {
                                                                datosAdicionales.setValues({numCelular: '', confNumCelular: ''});
                                                            });
                                                            Ext.Viewport.unmask();
                                                        }
                                                    });

                                                } else {
                                                    Ext.Msg.alert("Numero de celular no valido", "El Numero de celular no es valido", function(btn) {
                                                        datosAdicionales.setValues({numCelular: '', confNumCelular: ''});
                                                    });
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Confirmar Celular:',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    labelAlign: 'top',
                                    name: 'confNumCelular',
                                    id: 'confNumCelular',
                                    value: dn,
                                    maxLength: 10,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("confNumCelular").setValue(Ext.getCmp("confNumCelular").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'selectfield',
                                    label: 'Proveedor:',
                                    labelAlign: 'top',
                                    name: 'proveedor',
                                    id: 'proveedor',
                                    itemId: 'proveedor',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    options: proveedores.proveedores,
                                    required: true
                                },
                                {
                                    xtype: 'emailfield',
                                    label: 'Correo Electr&oacute;nico:',
                                    labelAlign: 'top',
                                    name: 'email',
                                    id: 'email',
                                    value: email,
                                    maxLength: 250,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("email").setValue((Ext.getCmp("email").getValue().trim()));
                                        },
                                        change: function() {
                                            if (Ext.getCmp("email").getValue().length !== 0) {
                                                if (/^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/.test(Ext.getCmp("email"))) {
                                                    Ext.Viewport.mask({xtype: 'loadmask', message: 'Validando email'});
                                                    Ext.Ajax.request({
                                                        url: 'consultasAjax.do?method=validaEmail',
                                                        method: 'POST',
                                                        params: {email: $.jCryption.encrypt(Ext.getCmp("email").getValue(), password_)},
                                                        success: function(xhr) {
                                                            var data = Ext.JSON.decode($.jCryption.decrypt(xhr.responseText, password));
                                                            if (data.error === '0') {
                                                                if (data.valido === '0') {
                                                                    Ext.Msg.alert("Email ocupado", "El email ingresado ya esta en uso, seleccione la opci&oacute;n ya soy usuario MobileCard", function(btn) {
                                                                        datosAdicionales.setValues({email: '', confEmail: ''});
                                                                    });
                                                                } else {
                                                                    Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el email", function(btn) {
                                                                        datosAdicionales.setValues({email: '', confEmail: ''});
                                                                    });
                                                                }
                                                                Ext.Viewport.unmask();
                                                            }
                                                        },
                                                        failure: function(response, opts) {
                                                            Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar el email", function(btn) {
                                                                datosAdicionales.setValues({email: '', confEmail: ''});
                                                            });
                                                            Ext.Viewport.unmask();
                                                        }
                                                    });
                                                } else {
                                                    Ext.Msg.alert("Email no valido", "El email no tiene el formato valido", function(btn) {
                                                        datosAdicionales.setValues({email: '', confEmail: ''});
                                                    });
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'emailfield',
                                    label: 'Confirmar Correo Electr&oacute;nico:',
                                    labelAlign: 'top',
                                    name: 'confEmail',
                                    id: 'confEmail',
                                    value: email,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("confEmail").setValue(Ext.getCmp("confEmail").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Nombre(s):',
                                    labelAlign: 'top',
                                    name: 'nombre',
                                    id: 'nombre',
                                    maxLength: 35,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("nombre").setValue(Ext.getCmp("nombre").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Apellido Paterno:',
                                    labelAlign: 'top',
                                    name: 'apellidoP',
                                    id: 'apellidoP',
                                    maxLength: 45,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("apellidoP").setValue(Ext.getCmp("apellidoP").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Apellido Materno:',
                                    labelAlign: 'top',
                                    name: 'apellidoM',
                                    id: 'apellidoM',
                                    maxLength: 45,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("apellidoM").setValue(Ext.getCmp("apellidoM").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'datepickerfield',
                                    label: 'Fecha de nacimiento:',
                                    labelAlign: 'top',
                                    name: 'fechaNac',
                                    id: 'fechaNac',
                                    placeHolder: 'Seleccionar Fecha [03/07/1995]',
                                    dateFormat: 'd/m/Y',
                                    required: true,
                                    picker: {
                                        slotOrder: [
                                            'day',
                                            'month',
                                            'year'],
                                        yearFrom: parseInt(yearTo),
                                        yearTo: parseInt(yearFrom)
                                    }
                                },
                                {
                                    xtype: 'selectfield',
                                    label: 'Sexo:',
                                    labelAlign: 'top',
                                    name: 'sexo',
                                    id: 'sexo',
                                    options: [
                                        {
                                            text: 'Seleccionar ...',
                                            value: '0'
                                        },
                                        {
                                            text: 'Masculino',
                                            value: 'M'
                                        },
                                        {
                                            text: 'Femenino',
                                            value: 'F'
                                        }],
                                    required: true
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Tel&eacute;fono de Casa:',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    labelAlign: 'top',
                                    name: 'telefonoCasa',
                                    id: 'telefonoCasa',
                                    maxLength: 10,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("telefonoCasa").setValue(Ext.getCmp("telefonoCasa").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Tel&eacute;fono de Oficina:',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    labelAlign: 'top',
                                    name: 'telefonoOficina',
                                    id: 'telefonoOficina',
                                    maxLength: 10,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("telefonoOficina").setValue(Ext.getCmp("telefonoOficina").getValue().trim());
                                        }
                                    }
                                }, {
                                    xtype: 'button',
                                    text: 'Continuar',
                                    id: 'btnContinuar',
                                    handler: function(btn, evt) {
                                        Ext.getCmp("btnContinuar").disable();
                                        Ext.getCmp("btnCancelar").disable();
                                        var datosAdicionalesModel = Ext.create('datosAdicionalesModel');
                                        var errors, errorMessage = '';
                                        datosAdicionales.updateRecord(datosAdicionalesModel);
                                        errors = datosAdicionalesModel.validate();
                                        if (!errors.isValid()) {
                                            errors.each(function(err) {
                                                errorMessage += err.getMessage() + '<br/>';
                                            }); // each()
                                            Ext.Msg.alert('Datos incorrectos', errorMessage);
                                            Ext.getCmp("btnContinuar").enable();
                                            Ext.getCmp("btnCancelar").enable();
                                        } else {
                                            Ext.Msg.confirm('Los datos son correctos?', 'El n&uacute;mero celular y el email que nos proporciono son correctos ?, con esta informaci&oacute;n le sera enviado un sms y un email con los datos de acceso a su cuenta', function(btn) {
                                                if (btn === 'yes') {
                                                    var d = Ext.getCmp("fechaNac").getValue();
                                                    var daym = d.getDate();
                                                    if (daym < 10)
                                                        daym = "0" + daym;
                                                    var month = (d.getMonth() + 1);
                                                    if (month < 10)
                                                        month = "0" + month;
                                                    var fechaNac = d === null ? "" : (d.getFullYear() + '-' + month + '-' + daym);
                                                    datosAdicionalesEnc.setValues({
                                                        nombreUsuarioEnc: $.jCryption.encrypt(Ext.getCmp("nombreUsuario").getValue(), password_),
                                                        numCelularEnc: $.jCryption.encrypt(Ext.getCmp("numCelular").getValue(), password_),
                                                        proveedorEnc: $.jCryption.encrypt(Ext.getCmp("proveedor").getValue(), password_),
                                                        emailEnc: $.jCryption.encrypt(Ext.getCmp("email").getValue(), password_),
                                                        nombreEnc: $.jCryption.encrypt(Ext.getCmp("nombre").getValue(), password_),
                                                        apellidoPEnc: $.jCryption.encrypt(Ext.getCmp("apellidoP").getValue(), password_),
                                                        apellidoMEnc: $.jCryption.encrypt(Ext.getCmp("apellidoM").getValue(), password_),
                                                        fechaNacEnc: $.jCryption.encrypt(fechaNac, password_),
                                                        sexoEnc: $.jCryption.encrypt(Ext.getCmp("sexo").getValue(), password_),
                                                        telefonoCasaEnc: $.jCryption.encrypt(Ext.getCmp("telefonoCasa").getValue(), password_),
                                                        telefonoOficinaEnc: $.jCryption.encrypt(Ext.getCmp("telefonoOficina").getValue(), password_)
                                                    }); // contactForm()
                                                    Ext.getCmp("btnContinuar").disable();
                                                    datosAdicionalesEnc.submit({url: 'complementariosUno.do?method=datosComplementarios', method: 'POST'});
                                                } // switch
                                                else {
                                                    Ext.getCmp("btnContinuar").enable();
                                                    Ext.getCmp("btnCancelar").enable();
                                                }
                                            }); // confirm()
                                        } // if
                                    }
                                }]
                        }, {
                            xtype: 'toolbar',
                            docked: 'bottom',
                            styleHtmlContent: true,
                            //title: '<div style="margin:-10px 0px 0px 0px;font-size: 10px; color: white;">Powered by MobileCard &reg;</div>',
                            title: 'Powered by MobileCard &reg;',
                            //html: '<font style="padding:-10px 0px 0px 0px;font-size:10px;color:white"><center>Powered by Mobilecard</center></font>',
                            minHeight: '1.3em',
                            height: '1.3em',
                            style: 'background:#09569b',
                            listeners: [
                                {
                                    fn: function(element, options) {
                                        Ext.getCmp(element.id).titleComponent.innerElement.dom.style.color = 'white';
                                        Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontSize = '10px';
                                        //Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontStyle = 'italic';
                                    },
                                    event: 'painted'
                                }
                            ]
                        }]
                }); // create()

        var datosAdicionalesEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'nombreUsuarioEnc',
                    id: 'nombreUsuarioEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'numCelularEnc',
                    id: 'numCelularEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'proveedorEnc',
                    id: 'proveedorEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'emailEnc',
                    id: 'emailEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'nombreEnc',
                    id: 'nombreEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'apellidoPEnc',
                    id: 'apellidoPEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'apellidoMEnc',
                    id: 'apellidoMEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'fechaNacEnc',
                    id: 'fechaNacEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'sexoEnc',
                    id: 'sexoEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'telefonoCasaEnc',
                    id: 'telefonoCasaEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'telefonoOficinaEnc',
                    id: 'telefonoOficinaEnc'
                }
            ] // items (formpanel)
        }); // create()
    } // launch
}); // application()
