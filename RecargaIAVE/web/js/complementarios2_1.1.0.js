var datosAdicionales;
var terminos = '';
var terminosAceptados = '';
Ext.application({
    launch: function() {
        Ext.applyIf(Ext.data.Validations, {
            validaTarjeta: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value !== null) {
                    if (Ext.getCmp("tipoTarjeta").getValue() === '3' && (new String(value)).length !== 15) {
                        return false;
                    } else if (Ext.getCmp("tipoTarjeta").getValue() !== '3' && (new String(value)).length !== 16) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            },
            validaFechaTarjeta: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value === null) {
                    return true;
                } else {
                    return !((value.getMonth() + 1) <= parseInt(curMonth) && value.getFullYear() === parseInt(yearFrom));
                }
            },
            validaAmexDatosDomicilio: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value !== null) {
                    if (Ext.getCmp("tipoTarjeta").getValue() === '3') {
                        if(Ext.getCmp("domicilioAMEX").getValue().trim().length===0){
                            return false;
                        }else{
                            return true;
                        }
                    } else  if (Ext.getCmp("tipoTarjeta").getValue() !== '3') {
                        return true;
                    }
                } 
            },
            validaAmexDatosCP: function(config, value) {
                if (arguments.length === 1) {
                    value = config;
                }
                if (value !== null) {
                    if (Ext.getCmp("tipoTarjeta").getValue() === '3') {
                        if(Ext.getCmp("codigoPostalAMEX").getValue().trim().length===0){
                            return false;
                        }else{
                            return true;
                        }
                    } else  if (Ext.getCmp("tipoTarjeta").getValue() !== '3') {
                        return true;
                    }
                } 
            }
        });
        Ext.define('datosAdicionalesModel', {
            extend: 'Ext.data.Model',
            config: {
                fields: [{
                        name: 'estado',
                        type: 'string'
                    }, 
                    {
                        name: 'ciudad',
                        type: 'string'
                    }, {
                        name: 'calle',
                        type: 'string'
                    }, {
                        name: 'numExt',
                        type: 'string'
                    }, {
                        name: 'numInt',
                        type: 'string'
                    }, {
                        name: 'colonia',
                        type: 'string'
                    }, {
                        name: 'numTarjeta',
                        type: 'string'
                    },{
                        name: 'domicilioAMEX',
                        type: 'string'
                    }, {
                        name: 'vigenciaTarjeta',
                        type: Ext.data.Types.DATE
                    }, {
                        name: 'terminos',
                        type: 'string'
                    }], // fields
                validations: [
                     {
                        field: 'estado',
                        type: 'presence',
                        message: 'La ciudad es requerida'
                    },
                    {
                        field: 'ciudad',
                        type: 'presence',
                        message: 'La ciudad es requerida'
                    },
                    {
                        field: 'calle',
                        type: 'presence',
                        message: 'La calle es requerida'
                    },
                    {
                        field: 'numExt',
                        type: 'presence',
                        message: 'El numero exterior es requerido'
                    },
                    {
                        field: 'colonia',
                        type: 'presence',
                        message: 'La colonia es requerida'
                    }, {
                        field: 'numTarjeta',
                        type: 'validaTarjeta',
                        message: 'El n&uacute;mero de tarjeta proporcionado no es valido'
                    }, {
                        field: 'numTarjeta',
                        type: 'presence',
                        message: 'El n&uacute;mero de tarjeta es requerido'
                    },
                    {
                        field: 'vigenciaTarjeta',
                        type: 'presence',
                        message: 'La fecha de expiraci&oacute;n de la tarjeta es requerida'
                    }, {
                        field: 'vigenciaTarjeta',
                        type: 'validaFechaTarjeta',
                        message: 'La fecha de expiraci&oacute;n de la tarjeta debe ser mayor que la fecha actual'
                    },{
                        field: 'codigoPostalAMEX',
                        type: 'validaAmexDatosCP',
                        message: 'Debe ingresar el codigo postal de su tarjeta AMEX'
                    },{
                        field: 'domicilioAMEX',
                        type: 'validaAmexDatosDomicilio',
                        message: 'Debe ingresar el domicilio de su tarjeta AMEX'
                    }, {
                        field: 'terminos',
                        type: 'presence',
                        message: 'Es necesario aceptar los terminos'
                    }] // validations
            } // config
        }); // define()

        datosAdicionales = Ext
                .create(
                'Ext.form.FormPanel',
                {
                    fullscreen: true,
                    itemId: 'datosAdicionales',
                    scrollable: 'vertical',
                    standardSubmit: true,
                    masked: {
                        xtype: 'loadmask',
                        message: 'Dando seguridad al canal... ' +
                                '<hr>Espere porfavor...'
                    }, // masked
                    items: [
                        {
                            xtype: 'titlebar',
                            docked: 'top',
                            title: 'Datos Adicionales',
                            style: 'background:#09569b',
                            items: [{
                                    xtype: 'button',
                                    align: 'left',
                                    //ui: 'decline-small',
                                    //icon: 'images/cancel.svg',
                                    //iconCls: 'svg-image',
                                    iconAlign: 'center',
                                    iconCls: 'delete',
                                    iconMask: true,
                                    id: 'btnCancelar',
                                    handler: function(btn, evt) {
                                        location.href = "http://www.mobilecard.mx:8080/RecargaIAVE/close.html";
                                    }
                                }]
                        },
                        {
                            xtype: 'fieldset',
                            items: [
                                {
                                    xtype: 'selectfield',
                                    label: 'Estado:',
                                    labelAlign: 'top',
                                    name: 'estado',
                                    id: 'estado',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    options: estados.estados
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Ciudad:',
                                    labelAlign: 'top',
                                    name: 'ciudad',
                                    id: 'ciudad',
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("ciudad").setValue(Ext.getCmp("ciudad").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Calle:',
                                    labelAlign: 'top',
                                    name: 'calle',
                                    id: 'calle',
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("calle").setValue(Ext.getCmp("calle").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'N&uacute;mero Exterior:',
                                    labelAlign: 'top',
                                    name: 'numExt',
                                    id: 'numExt',
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("numExt").setValue(Ext.getCmp("numExt").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'N&uacute;mero Interior:',
                                    labelAlign: 'top',
                                    name: 'numInt',
                                    id: 'numInt',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("numInt").setValue(Ext.getCmp("numInt").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'Colonia:',
                                    labelAlign: 'top',
                                    name: 'colonia',
                                    id: 'colonia',
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("colonia").setValue(Ext.getCmp("colonia").getValue().trim());
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'C&oacute;digo Postal:',
                                    labelAlign: 'top',
                                    name: 'codigoPostal',
                                    id: 'codigoPostal',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("codigoPostal").setValue(Ext.getCmp("codigoPostal").getValue().trim());
                                        }
                                    },
                                },
                                {
                                    xtype: 'selectfield',
                                    label: 'Tipo de tarjeta:',
                                    labelAlign: 'top',
                                    name: 'tipoTarjeta',
                                    id: 'tipoTarjeta',
                                    displayField: 'descripcion',
                                    valueField: 'clave',
                                    options: tarjetas.tarjetas,
                                    listeners: {
                                        change: function(field, value) {
                                            if (value === '3') {
                                                Ext.getCmp(
                                                        "domicilioAMEX")
                                                        .setDisabled(
                                                        false);
                                            } else {
                                                Ext.getCmp(
                                                        "domicilioAMEX")
                                                        .setDisabled(
                                                        true);
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'N&uacute;mero de tarjeta:',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    labelAlign: 'top',
                                    name: 'numTarjeta',
                                    id: 'numTarjeta',
                                    maxlength: 16,
                                    required: true,
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("numTarjeta").setValue(Ext.getCmp("numTarjeta").getValue().trim());
                                        },
                                        change: function() {
                                            var tarjeta = Ext.getCmp("numTarjeta").getValue();
                                            if (tarjeta.length !== 0) {
                                                if ((Ext.getCmp("tipoTarjeta").getValue() === '3' && tarjeta.length === 15) ||
                                                        (Ext.getCmp("tipoTarjeta").getValue() !== '3' && tarjeta.length === 16)) {
                                                    Ext.Viewport.mask({xtype: 'loadmask', message: 'Validando tarjeta'});
                                                    Ext.Ajax.request({
                                                        url: 'consultasAjax.do?method=validaTDC',
                                                        method: 'POST',
                                                        params: {tdc: $.jCryption.encrypt(tarjeta, password_)},
                                                        success: function(xhr) {
                                                            var data = Ext.JSON.decode($.jCryption.decrypt(xhr.responseText, password));
                                                            if (data.error === '0') {
                                                                if (data.valido === '0') {
                                                                    Ext.Msg.alert("Tarjeta registrada", "El n&uacute;mero de tarjeta ingresado ya esta en uso, seleccione la opci&oacute;n Usuario MobileCard", function(btn) {
                                                                        datosAdicionales.setValues({numTarjeta: ''});
                                                                        datosAdicionalesEnc.submit({url: 'welcome.do?method=inicio', method: 'POST'});
                                                                    });                                                                     
                                                                }
                                                            } else {
                                                                Ext.Msg.alert("Error", data.error, function(btn) {
                                                                    datosAdicionales.setValues({numTarjeta: ''});
                                                                });

                                                            }
                                                            Ext.Viewport.unmask();
                                                        },
                                                        failure: function(response, opts) {
                                                            Ext.Msg.alert("Error de comunicaci&oacute;n", "No fue posible validar la tarjeta", function(btn) {
                                                                datosAdicionales.setValues({numTarjeta: ''});
                                                            });
                                                            Ext.Viewport.unmask();
                                                        }
                                                    });
                                                } else {
                                                    Ext.Msg.alert("Tarjeta no valida", "La tarjeta no tiene el formato valido", function(btn) {
                                                        datosAdicionales.setValues({numTarjeta: ''});
                                                    });
                                                }
                                            }
                                        }
                                    }
                                },
                                {
                                    xtype: 'textfield',
                                    disabled: true,
                                    label: 'Domicilio de Estado de Cuenta de tarjeta:',
                                    labelAlign: 'top',
                                    name: 'domicilioAMEX',
                                    id: 'domicilioAMEX',
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("domicilioAMEX").setValue(Ext.getCmp("domicilioAMEX").getValue().trim());
                                        }
                                     }
                                },
                                {
                                    xtype: 'textfield',
                                    label: 'C&oacute;digo Postal AMEX:',
                                    labelAlign: 'top',
                                    name: 'codigoPostalAMEX',
                                    id: 'codigoPostalAMEX',
                                    component: {
                                        xtype: 'input',
                                        type: 'tel'
                                    },
                                    listeners: {
                                        keyup: function() {
                                            Ext.getCmp("codigoPostalAMEX").setValue(Ext.getCmp("codigoPostalAMEX").getValue().trim());
                                        }
                                    },
                                },
                                {
                                    xtype: 'datepickerfield',
                                    label: 'Vigencia de Tarjeta:',
                                    labelAlign: 'top',
                                    name: 'vigenciaTarjeta',
                                    id: 'vigenciaTarjeta',
                                    placeHolder: 'mm/yyyy',
                                    dateFormat: 'm/Y',
                                    picker: {
                                        slotOrder: [
                                            'month',
                                            'year'],
                                        yearFrom: parseInt(yearFrom),
                                        yearTo: parseInt(yearTo)
                                    },
                                    required: true

                                },
                                {
                                    xtype: 'button',
                                    text: 'T&eacute;rminos y condiciones',
                                    id: 'btnTerminos',
                                    handler: function(btn, evt) {
                                        Ext.getCmp("btnTerminos").disable();
                                        if (terminos.length === 0) {
                                            Ext.Ajax.request({
                                                url: 'consultasAjax.do?method=terminosCondiciones',
                                                method: 'POST',
                                                success: function(xhr) {
                                                    var data = Ext.JSON.decode(xhr.responseText);
                                                    if (data.error === '0') {
                                                        var password = new jsSHA(data.llavePublica, "ASCII").getHash("SHA-512", "HEX");
                                                        var decryptedString = $.jCryption.decrypt(data.Descripcion, password);
                                                        terminos = decryptedString;
                                                        Ext.getCmp('terminos').setHtml(decryptedString);
                                                        Ext.Viewport.add(popup);
                                                        popup.show();
                                                    } else {
                                                        Ext.Msg.alert('Error', data.errorMensaje);
                                                    }
                                                    Ext.getCmp("btnTerminos").enable();
                                                },
                                                failure: function(response, opts) {
                                                    Ext.Msg.alert('Error en comunicaci&oacuten', "No se pudieron obtener los t&eacute;rminos");
                                                    Ext.getCmp("btnTerminos").enable();
                                                }
                                            });
                                        } else {
                                            Ext.getCmp('terminos').setHtml(terminos);
                                            Ext.Viewport.add(popup);
                                            popup.show();
                                            Ext.getCmp("btnTerminos").enable();
                                        }

                                    }
                                },
                                {
                                    xtype: 'checkboxfield',
                                    label: 'Acepto los T&eacute;rminos y Condiciones',
                                    labelAlign: 'right',
                                    labelWidth: '80%',
                                    labelWrap: true,
                                    required: true,
                                    id: 'terminos',
                                    name: 'terminos',
                                    listeners: {
                                        check: function() {
                                            terminosAceptados = 'S';
                                        },
                                        uncheck: function() {
                                            terminosAceptados = 'N';
                                        }
                                    }
                                }, {
                                    xtype: 'button',
                                    centered: false,
                                    left: '',
                                    width: '',
                                    text: 'Continuar',
                                    id: 'btnContinuar',
                                    handler: function(btn, evt) {
                                        Ext.getCmp("btnContinuar").disable();
                                        Ext.getCmp("btnCancelar").disable();
                                        var datosAdicionalesModel = Ext.create('datosAdicionalesModel');
                                        var errors, errorMessage = '';
                                        datosAdicionales.updateRecord(datosAdicionalesModel);
                                        errors = datosAdicionalesModel.validate();
                                        if (!errors.isValid()) {
                                            errors.each(function(err) {
                                                errorMessage += err.getMessage() + '<br/>';
                                            }); // each()
                                            Ext.Msg.alert('Datos incorrectos', errorMessage);
                                            Ext.getCmp("btnContinuar").enable();
                                            Ext.getCmp("cancelar").enable();
                                        } else {
                                            var d = Ext.getCmp("vigenciaTarjeta").getValue();
                                            var mes=(d.getMonth() + 1);
                                            if(mes<9){
                                                mes='0'+mes;
                                            }
                                            var vigencia = d === null ? "" : (mes + '/' + new String(d.getFullYear()).substr(2));
                                            datosAdicionalesEnc.setValues({
                                                estadoEnc: $.jCryption.encrypt(Ext.getCmp("estado").getValue(), password_),
                                                ciudadEnc: $.jCryption.encrypt(Ext.getCmp("ciudad").getValue(), password_),
                                                calleEnc: $.jCryption.encrypt(Ext.getCmp("calle").getValue(), password_),
                                                numExtEnc: $.jCryption.encrypt(Ext.getCmp("numExt").getValue() !== null ? new String(Ext.getCmp("numExt").getValue()) : "", password_),
                                                numIntEnc: $.jCryption.encrypt(Ext.getCmp("numInt").getValue(), password_),
                                                coloniaEnc: $.jCryption.encrypt(Ext.getCmp("colonia").getValue(), password_),
                                                codigoPostalEnc: $.jCryption.encrypt(Ext.getCmp("codigoPostal").getValue() !== null ? new String(Ext.getCmp("codigoPostal").getValue()) : "", password_),
                                                numTarjetaEnc: $.jCryption.encrypt(Ext.getCmp("numTarjeta").getValue() !== null ? new String(Ext.getCmp("numTarjeta").getValue()) : "", password_),
                                                tipoTarjetaEnc: $.jCryption.encrypt(Ext.getCmp("tipoTarjeta").getValue(), password_),                                               
                                                domicilioAMEXEnc: $.jCryption.encrypt(Ext.getCmp("domicilioAMEX").getValue(), password_),
                                                codigoPostalAMEXEnc: $.jCryption.encrypt(Ext.getCmp("codigoPostalAMEX").getValue(), password_),
                                                vigenciaTarjetaEnc: $.jCryption.encrypt(vigencia, password_),
                                                plataformaEnc: $.jCryption.encrypt(navigator.platform, password_),
                                                terminosEnc: $.jCryption.encrypt(terminosAceptados, password_)
                                            }); // contactForm()                                            
                                            datosAdicionalesEnc.submit({url: 'complementariosDos.do?method=datosComplementarios', method: 'POST'});
                                        }
                                    }
                                }]
                        }, {
                            xtype: 'toolbar',
                            docked: 'bottom',
                            styleHtmlContent: true,
                            title: 'Powered by MobileCard &reg;',
                            minHeight: '1.3em',
                            height: '1.3em',
                            style: 'background:#09569b',
                            listeners: [
                                {
                                    fn: function(element, options) {
                                        Ext.getCmp(element.id).titleComponent.innerElement.dom.style.color = 'white';
                                        Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontSize = '10px';
                                        //Ext.getCmp(element.id).titleComponent.innerElement.dom.style.fontStyle = 'italic';
                                    },
                                    event: 'painted'
                                }
                            ]                            
                        }]
                }); // create()
        var popup = new Ext.Panel({
            floating: true,
            centered: true,
            modal: true,
            width: '98%',
            height: '90%',
            //fullscreen: true,
            items: [
                {
                    xtype: 'titlebar',
                    docked: 'top',
                    title: 'Terminos y condiciones',
                    items: [{
                            xtype: 'image',
                            align: 'right',
                            height: 55,
                            width: 55,
                            mode: 'foreground',
                            src: 'images/img_mobilecardlogo_1.1.0.png'
                        }]},
                {
                    xtype: 'panel',
                    styleHtmlContent: true,
                    scrollable: 'vertical',
                    width: '100%',
                    height: '80%',
                    id: 'terminos'
                },
                {
                    xtype: 'toolbar',
                    docked: 'bottom',
                    layout: {
                        pack: 'center'
                    }, // layout
                    ui: 'plain',
                    items: [{
                            xtype: 'button',
                            text: 'Regresar',
                            handler: function(btn, evt) {
                                popup.hide();
                            }
                        }] // items (toolbar)
                }
            ]
        });
        var datosAdicionalesEnc = Ext.create('Ext.form.FormPanel', {
            standardSubmit: true,
            items: [
                {
                    xtype: 'hiddenfield',
                    name: 'estadoEnc',
                    id: 'estadoEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'ciudadEnc',
                    id: 'ciudadEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'calleEnc',
                    id: 'calleEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'numExtEnc',
                    id: 'numExtEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'numIntEnc',
                    id: 'numIntEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'coloniaEnc',
                    id: 'coloniaEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'codigoPostalEnc',
                    id: 'codigoPostalEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'numTarjetaEnc',
                    id: 'numTarjetaEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'tipoTarjetaEnc',
                    id: 'tipoTarjetaEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'domicilioAMEXEnc',
                    id: 'domicilioAMEXEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'codigoPostalAMEXEnc',
                    id: 'codigoPostalAMEXEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'vigenciaTarjetaEnc',
                    id: 'vigenciaTarjetaEnc'
                }
                , {
                    xtype: 'hiddenfield',
                    name: 'terminosEnc',
                    id: 'terminosEnc'
                }, {
                    xtype: 'hiddenfield',
                    name: 'plataformaEnc',
                    id: 'plataformaEnc'

                }

            ] // items (formpanel)
        }); // create()
    } // launch
}); // application()

