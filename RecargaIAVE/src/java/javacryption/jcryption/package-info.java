/**
 * Provides jCryption implementation for Java. 
 *
 * @author Gabriel Andery
 * @version 1.0
 */
package javacryption.jcryption;