/**
 * Provides exception classes for cryptographic algorithms. 
 *
 * @author Gabriel Andery
 * @version 1.0
 */
package javacryption.exception;