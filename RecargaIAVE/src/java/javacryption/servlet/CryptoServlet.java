package javacryption.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.KeyPair;
import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javacryption.aes.AesCtr;
import javacryption.jcryption.JCryption;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

/**
 * Servlet example for jCryption
 *
 * @author Gabriel Andery
 * @version 1.0
 */
public class CryptoServlet extends HttpServlet {

    private Logger log = Logger.getLogger(CryptoServlet.class);
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 4510110365995157499L;

    /**
     * Handles a POST request
     *
     * @see HttpServlet
     */
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        PrintWriter out = null;
        String callback = null;
        String element = null;
        String output = null;
        Enumeration en = null;
        JCryption jc = null;
        KeyPair keys = null;
        String key = null;
        String e = null;
        String n = null;
        String md = null;
        en = request.getParameterNames();
        log.info("***************inicio***************");
        while (en.hasMoreElements()) {
            element = (String) en.nextElement();
            log.info(element + " : " + request.getParameter(element));
        }
        log.info("***************fin***************");
        /**
         * Generates a KeyPair for RSA *
         */
        if (req.getParameter("publicKey") != null) {
            log.info("llave publica : " + req.getParameter("publicKey"));
            HttpSession session = request.getSession(false);
            if(session!=null){
                session.invalidate();
            }
            session=request.getSession(true);
            session.setAttribute("publicKey", req.getParameter("publicKey"));
        }
        if (req.getParameter("generateKeyPair") != null && req.getParameter("generateKeyPair").equals("true")) {
            jc = new JCryption();
            keys = jc.getKeyPair();
            request.getSession().setAttribute("jCryptionKeys", keys);
            e = jc.getPublicExponent();
            n = jc.getKeyModulus();
            md = String.valueOf(jc.getMaxDigits());
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            output = null;
            if (callback != null) {
                response.setContentType("text/javascript");
                output = callback + "({\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"});";
            } else {
                output = "{\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"}";
            }
            log.info(output);
            out = response.getWriter();
            out.print(output);
            out.flush();
            return;
        } /**
         * jCryption handshake *
         */
        else if (req.getParameter("handshake") != null && req.getParameter("handshake").equals("true")) {
            /**
             * Decrypts password using private key *
             */
            jc = new JCryption((KeyPair) request.getSession().getAttribute("jCryptionKeys"));
            key = jc.decrypt(req.getParameter("key"));
            request.getSession().removeAttribute("jCryptionKeys");
            request.getSession().setAttribute("jCryptionKey", key);
            /**
             * Encrypts password using AES *
             */
            String ct = AesCtr.encrypt(key, key, 256);
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            if (callback != null) {
                response.setContentType("text/javascript");
                output = callback + "({\"challenge\":\"" + ct + "\"});";
            } else {
                output = "{\"challenge\":\"" + ct + "\"}";
            }
            log.info(output);
            out = response.getWriter();
            out.print(output);
            out.flush();
            return;
        } else if (req.getParameter("generateKeyPair_") != null && req.getParameter("generateKeyPair_").equals("true")) {
            jc = new JCryption();
            keys = jc.getKeyPair();
            request.getSession().setAttribute("jCryptionKeys_", keys);
            e = jc.getPublicExponent();
            n = jc.getKeyModulus();
            md = String.valueOf(jc.getMaxDigits());
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            output = null;
            if (callback != null) {
                response.setContentType("text/javascript");
                output = callback + "({\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"});";
            } else {
                output = "{\"e\":\"" + e + "\",\"n\":\"" + n + "\",\"maxdigits\":\"" + md + "\"}";
            }
            log.info(output);
            out = response.getWriter();
            out.print(output);
            out.flush();
            return;
        } /**
         * jCryption handshake *
         */
        else if (req.getParameter("handshake_") != null && req.getParameter("handshake_").equals("true")) {
            /**
             * Decrypts password using private key *
             */
            jc = new JCryption((KeyPair) request.getSession().getAttribute("jCryptionKeys_"));
            key = jc.decrypt(req.getParameter("key"));
            request.getSession().removeAttribute("jCryptionKeys_");
            request.getSession().setAttribute("jCryptionKey_", key);
            /**
             * Encrypts password using AES *
             */
            String ct = AesCtr.encrypt(key, key, 256);
            /**
             * Sends response *
             */
            callback = request.getParameter("callback");
            if (callback != null) {
                response.setContentType("text/javascript");
                output = callback + "({\"challenge\":\"" + ct + "\"});";
            } else {
                output = "{\"challenge\":\"" + ct + "\"}";
            }
            log.info(output);
            out = response.getWriter();
            out.print(output);
            out.flush();
            return;
        }
    }

    /**
     * Handles a GET request
     *
     * @see HttpServlet
     */
    public void doGet(HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {
        doPost(req, res);
    }
}
