/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.service;

import java.util.HashMap;
import mx.addcel.ServiceIAVE.dao.ConsumeServiciosDAO;

/**
 *
 * @author victor
 */
public class ConsumeServiciosService {

    private ConsumeServiciosDAO consumeServiciosDAO;

    public String getCatalogo(String urlServicio, HashMap parametros) {
        return consumeServiciosDAO.consumeServicio(urlServicio, parametros);
    }

    public String loginCorrecto(String urlServicio, HashMap parametros) {
        return consumeServiciosDAO.consumeServicio(urlServicio, parametros);
    }

    public String registraUsuario(String urlServicio, HashMap parametros) {
        return consumeServiciosDAO.consumeServicio(urlServicio, parametros);
    }
    public String compraIAVE(String urlServicio, HashMap parametros) {
        return consumeServiciosDAO.consumeServicio(urlServicio, parametros);
    }

    /**
     * @param consumeServiciosDAO the consumeServiciosDAO to set
     */
    public void setConsumeServiciosDAO(ConsumeServiciosDAO consumeServiciosDAO) {
        this.consumeServiciosDAO = consumeServiciosDAO;
    }
}
