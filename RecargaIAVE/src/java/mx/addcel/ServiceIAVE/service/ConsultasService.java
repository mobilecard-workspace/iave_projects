/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.service;

import com.ironbit.mc.system.crypto.Crypto;
import java.util.HashMap;
import mx.addcel.ServiceIAVE.dao.ConsultasDAO;
import mx.addcel.ServiceIAVE.service.utils.Constantes;
import mx.addcel.ServiceIAVE.service.utils.Utils;

/**
 *
 * @author victor
 */
public class ConsultasService {

    private ConsultasDAO consultasDAO;

    /**
     * @param consultasDAO the consultasDAO to set
     */
    public void setConsultasDAO(ConsultasDAO consultasDAO) {
        this.consultasDAO = consultasDAO;
    }

    public String getMapeoIAVE(String idIAVE) {
        return consultasDAO.existeUsuarioAsociado(idIAVE);
    }

    public Integer existeUsuario(String login) {
        return consultasDAO.existeUsuario(login);
    }
    public String existeTDC(String tdc) {
        String tdcEnc= Crypto.aesEncrypt(Utils.parsePass(Constantes.PASS_ENCTDC), tdc);
        return consultasDAO.existeTDC(tdcEnc);
    }
    public String existeEmail(String email) {
        return consultasDAO.existeEmail(email);
    }
    public String existeTelefono(String dn) {
        return consultasDAO.existeTelefono(dn);
    }
     public String existeIMEI(String imei) {
        return consultasDAO.existeIMEI("IAVE"+imei);
    }

    public void insertaMapeoUsuario(String loginIAVE, String loginMobileCard) {
        HashMap map = new HashMap();
        map.put("id_IAVE", loginIAVE);
        map.put("loginMobileCard", loginMobileCard);
        consultasDAO.insertaMapeoIAVE(map);
    }
    public String tdcVigencia(String loginIAVE) {
        return consultasDAO.tdcVigencia(loginIAVE);
    }
}
