package mx.addcel.ServiceIAVE.service.vo;
import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * @author Administrador
 *
 */
public abstract class AbstractVO implements Serializable {

    @Override
    public String toString() {
        StringBuffer resultado = new StringBuffer();
        Class myClass = this.getClass();
        Method[] metodos = myClass.getDeclaredMethods();
        int methodsLength = metodos.length;
        try {
            for (int i = 0; i < methodsLength; i++) {
                if (metodos[i].getParameterTypes().length == 0) {
                    resultado.append(" ");
                    resultado.append(metodos[i].getName());
                    resultado.append(": ");
                    resultado.append(metodos[i].invoke(this, null));
                    resultado.append(" \n");
                }
            }
        } catch (Exception e) {
        }
        return resultado.toString();
    }
}
