/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.service.utils;

import com.ironbit.mc.system.crypto.Crypto;
import java.util.HashMap;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.Producto;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.json.JSONObject;

/**
 *
 * @author victor
 */
public class Utils {

    private static Logger log = Logger.getLogger(Utils.class);

    public static String parsePass(String pass) {
        int len = pass.length();
        String key = "";
        for (int i = 0; i < 32 / len; i++) {
            key += pass;
        }
        int carry = 0;
        while (key.length() < 32) {
            key += pass.charAt(carry);
            carry++;
        }
        return key;
    }

    public static String mergeStr(String first, String second) {
        String result = "";
        String other = reverse(second);
        result += (Integer.toString(other.length()).length() < 2) ? "0" + other.length() : Integer.toString(other.length());
        String sub1 = first.substring(0, 19);
        String sub2 = first.substring(19, first.length());
        result += sub1;
        for (int i = 0; i < other.length(); i += 2) {
            int offset = ((i + 2) <= other.length()) ? (i + 2) : (i + 1);
            String next = other.substring(i, offset);
            result += next;
            result += sub2.substring(0, 2);
            sub2 = sub2.substring(2);
        }
        result += sub2;

        return result;
    }

    public static String reverse(String chain) {
        String nuevo = "";
        for (int i = chain.length() - 1; i >= 0; i--) {
            nuevo += chain.charAt(i);
        }
        return nuevo;
    }

    public static String[] validacionLogin(ResourceBundle bundle, ConsumeServiciosService consumeServiciosService, String imei, String login, String password, boolean seguro) throws Exception {
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String protocol = "http://";
        JSONObject jsonObj = null;
        String jEnc = null;
        String newEnc = null;
        HashMap parametros = null;
        String resultado[] = null;
        log.info("validacionLogin");
        try {
            jsonObj = new JSONObject();
            jsonObj.put("login", login);
            jsonObj.put("passwordS", Crypto.sha1(password));
            jsonObj.put("password", password);
            jsonObj.put("imei", "IAVE" + imei);
            jsonObj.put("tipo", "tuTag_IAVE");
            jsonObj.put("software", "1.1.1");
            jsonObj.put("modelo", "web");
            jsonObj.put("key", imei);
            if (seguro) {
                protocol = "https://";
            }
            log.info(jsonObj);
            jEnc = Crypto.aesEncrypt(Utils.parsePass(password), jsonObj.toString());
            newEnc = Utils.mergeStr(jEnc, password);

            urlBase = bundle.getString("urlbase");
            servicio = bundle.getString("loginUrl");
            log.info(new StringBuilder(protocol).append(urlBase).append(servicio).append("?json=").append(newEnc).toString());
            parametros = new HashMap();
            parametros.put("json", newEnc);
            cadenaOriginal = consumeServiciosService.loginCorrecto(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametros);
            if (cadenaOriginal != null & cadenaOriginal.length() > 0) {
                log.info("cadenaOriginal : " + cadenaOriginal);
                cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(password), cadenaOriginal);
                log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                jsonObj = new JSONObject(cadenaDesencriptada);
                resultado = new String[2];
                resultado[0] = Integer.toString((Integer) jsonObj.get("resultado"));
                resultado[1] = (String) jsonObj.get("mensaje");
            } else {
                resultado = null;
            }
        } catch (Exception e) {
            log.error("error al validar login : ", e);
            throw e;
        }
        return resultado;
    }

    public static String obtenDatosRecargaIAVE(ResourceBundle bundle, ConsumeServiciosService consumeServiciosService, String keyResponse, HttpServletRequest request) throws Exception {
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String protocol = "http://";
        String forward = null;
        DatosIAVE_VO datosIAVE;
        Producto p = null;
        JSONObject json = null;
        String jsonStr = null;
        String productos = null;
        HashMap parametros = null;
        try {
            urlBase = bundle.getString("urlbase");
            servicio = bundle.getString("servicioCatalogoProductosURL");
            json = new JSONObject();
            json.put("proveedor", "IAVE_TuTag");
            log.info(json.toString());
            jsonStr = Crypto.aesEncrypt(Constantes.PASS_ADDCEL, json.toString());
            parametros = new HashMap();
            parametros.put("json", jsonStr);
            cadenaOriginal = consumeServiciosService.getCatalogo(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametros);
            if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                log.info("cadenaOriginal : " + cadenaOriginal);
                cadenaDesencriptada = Crypto.aesDecrypt(Constantes.PASS_ADDCEL, cadenaOriginal);
                log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                productos = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
                log.info(productos);
                datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
                log.info(datosIAVE);
                request.setAttribute("aliasTag", AesCtr.encrypt(datosIAVE.getNombreTag(), keyResponse, 256));
                request.setAttribute("tag", AesCtr.encrypt(datosIAVE.getTag(), keyResponse, 256));
                request.setAttribute("productos", productos);
                request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                forward = "recargaIAVE";
            } else {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error de comunicación, intentelo nuevamente.");
                forward = "error";
            }
        } catch (Exception e) {
            log.error("error al obtener datos para recargaIAVE", e);
            throw e;
        }
        return forward;
    }
}
