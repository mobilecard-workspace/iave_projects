/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.service.utils;

import java.util.Locale;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.MessageResources;
import org.apache.struts.util.PropertyMessageResources;

/**
 *
 * @author victor
 */
public class StrutsMensajes {

    protected static final Locale defaultLocale = Locale.getDefault();
    private String arg0 = null;
    private String arg1 = null;
    private String arg2 = null;
    private String arg3 = null;
    protected String key = null;

    public static String getErrorMessage(PropertyMessageResources p,ActionMessage msg) {
        String text = p.getMessage(defaultLocale, msg.getKey(), msg.getValues());
        System.out.println(text);
        return text;
    }

    /**
     * @return the arg0
     */
    public String getArg0() {
        return arg0;
    }

    /**
     * @param arg0 the arg0 to set
     */
    public void setArg0(String arg0) {
        this.arg0 = arg0;
    }

    /**
     * @return the arg1
     */
    public String getArg1() {
        return arg1;
    }

    /**
     * @param arg1 the arg1 to set
     */
    public void setArg1(String arg1) {
        this.arg1 = arg1;
    }

    /**
     * @return the arg2
     */
    public String getArg2() {
        return arg2;
    }

    /**
     * @param arg2 the arg2 to set
     */
    public void setArg2(String arg2) {
        this.arg2 = arg2;
    }

    /**
     * @return the arg3
     */
    public String getArg3() {
        return arg3;
    }

    /**
     * @param arg3 the arg3 to set
     */
    public void setArg3(String arg3) {
        this.arg3 = arg3;
    }
}
