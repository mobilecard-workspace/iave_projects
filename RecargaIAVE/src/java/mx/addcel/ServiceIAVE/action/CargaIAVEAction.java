/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.form.CargaIAVEForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author victor
 */
public class CargaIAVEAction extends DispatchAction {

    private ConsumeServiciosService consumeServiciosService;
    private ConsultasService consultasService;
    private Logger log = Logger.getLogger(CargaIAVEAction.class);
    private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

    public ActionForward validaUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String forward = null;
        String keyResponse = null;
        CargaIAVEForm datos = null;
        DatosIAVE_VO datosIAVE = null;
        String loginAsociado = null;
        String loginEmail = null;
        String loginDn = null;
        String loginIMEI = null;
        try {
            log.info("Entro a validaUsuario");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (keyResponse == null || keyResponse.length() == 0) {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                datos = (CargaIAVEForm) form;
                log.info(datos);
                datosIAVE = new DatosIAVE_VO();
                datosIAVE.setIduser(datos.getIduser());
                datosIAVE.setDn(datos.getDn());
                datosIAVE.setEmail(datos.getEmail());
                datosIAVE.setImei(datos.getImei());
                datosIAVE.setTag(datos.getTag());
                datosIAVE.setNombreTag(datos.getNombreTag());
                datosIAVE.setPin(datos.getPin());
                log.info(datosIAVE);

                request.getSession().setAttribute("datosIAVE", datosIAVE);
                loginAsociado = consultasService.getMapeoIAVE(datosIAVE.getIduser());
                loginEmail = consultasService.existeEmail(datosIAVE.getEmail());
                loginDn = consultasService.existeTelefono(datosIAVE.getDn());
                loginIMEI = consultasService.existeIMEI(datosIAVE.getImei());
                if (loginAsociado != null && loginAsociado.length() > 0) {
                    log.info("entro a login asociado");
                    log.info(loginAsociado);
                    request.getSession().setAttribute("loginMobileCard", loginAsociado);
                    request.setAttribute("nextPage","goToRecargaIAVE");  
                    forward = "recargaIAVE";
                } else if (loginDn != null && loginDn.length() > 0) {       
                    log.info("entro a usuario existente DN");
                    request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
                    request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
                    request.setAttribute("login", AesCtr.encrypt(loginDn, keyResponse, 256));
                    request.getSession().setAttribute("jCryptionKey_",keyResponse);
                    request.setAttribute("nextPage","goToLogin");
                    forward = "loginRedirect";
                } else if (loginEmail != null && loginEmail.length() > 0) {
                    log.info("entro a usuario existente email");
                    request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
                    request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
                    request.setAttribute("login", AesCtr.encrypt(loginEmail, keyResponse, 256));
                    request.getSession().setAttribute("jCryptionKey_",keyResponse);
                    request.setAttribute("nextPage","goToLogin");
                    forward = "loginRedirect";
                } else if (loginIMEI != null && loginIMEI.length() > 0) {     
                    log.info("entro a usuario existente imei");
                    request.setAttribute("titulo", AesCtr.encrypt("Login con su usuario MobileCard", keyResponse, 256));
                    request.setAttribute("errorMensaje", AesCtr.encrypt("Usted ya es usuario mobileCard, haga login con su cuenta", keyResponse, 256));
                    request.setAttribute("login", AesCtr.encrypt(loginIMEI, keyResponse, 256));
                    request.getSession().setAttribute("jCryptionKey_",keyResponse);
                    request.setAttribute("nextPage","goToLogin");
                    forward = "loginRedirect";
                } else {
                    log.info("entro a usuario nuevo");
                    request.setAttribute("nextPage","goToWelcome");
                    forward = "welcome";
                }

            }

        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al realizar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }

    /**
     * @param consumeServiciosService the consumeServiciosService to set
     */
    public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
        this.consumeServiciosService = consumeServiciosService;
    }

    /**
     * @param consultasService the consultasService to set
     */
    public void setConsultasService(ConsultasService consultasService) {
        this.consultasService = consultasService;
    }
}
