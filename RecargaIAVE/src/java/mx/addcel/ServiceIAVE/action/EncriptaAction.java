/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import java.io.PrintWriter;
import java.security.KeyPair;
import javacryption.aes.AesCtr;
import javacryption.jcryption.JCryption;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author victor
 */
public class EncriptaAction extends DispatchAction {

    private Logger log = Logger.getLogger(EncriptaAction.class);

    public ActionForward generateKeyPair(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        JCryption jc = new JCryption();
        KeyPair keys = null;
        PrintWriter out = null;
        try {
            keys = jc.getKeyPair();
            request.getSession().setAttribute("jCryptionKeys", keys);
            String e = jc.getPublicExponent();
            String n = jc.getKeyModulus();
            String md = String.valueOf(jc.getMaxDigits());

            /**
             * Sends response *
             */
            out = response.getWriter();
            out.print("{\"e\":\"" + e + "\",\"n\":\"" + n
                    + "\",\"maxdigits\":\"" + md + "\"}");
            out.flush();
        } catch (Exception e) {
            log.fatal("Exceltion : ", e);
        }
        return null;
    }

    public ActionForward handshake(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PrintWriter out = null;
        String ct = null;
        JCryption jc = null;
        String key = null;
        try {
            /**
             * Decrypts password using private key *
             */
            jc = new JCryption((KeyPair) request.getSession().getAttribute("jCryptionKeys"));
            key = jc.decrypt(request.getParameter("key"));
            request.getSession().removeAttribute("jCryptionKeys");
            request.getSession().setAttribute("jCryptionKey", key);

            /**
             * Encrypts password using AES *
             */
            ct = AesCtr.encrypt(key, key, 256);
            /**
             * Sends response *
             */
            out = response.getWriter();
            out.print("{\"challenge\":\"" + ct + "\"}");
            out.flush();
        } catch (Exception e) {
            log.fatal("Exceltion : ", e);
        }
        return null;
    }
}
