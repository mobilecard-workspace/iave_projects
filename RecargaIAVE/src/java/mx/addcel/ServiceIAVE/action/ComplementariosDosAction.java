/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import com.ironbit.mc.system.crypto.Crypto;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.form.DatosComplementariosDosForm;
import mx.addcel.ServiceIAVE.form.DatosInicioForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Constantes;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

/**
 *
 * @author victor
 */
public class ComplementariosDosAction extends DispatchAction {

    private Logger log = Logger.getLogger(ComplementariosUnoAction.class);
    private ConsumeServiciosService consumeServiciosService;
    private ConsultasService consultasService;
    private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

    public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String protocol = "http://";
        String forward = null;
        Calendar cal = null;
        String estados = null;
        String tarjetas = null;
        String keyResponse = null;
        String keyRequest = null;
        try {
            log.info("Entro a ComplementariosDosAction inicio");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyRequest == null || keyRequest.length() == 0 || keyResponse == null || keyResponse.length() == 0) {
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                urlBase = bundle.getString("urlbase");
                servicio = bundle.getString("servicioCatalogoEstadosURL");
                cadenaOriginal = consumeServiciosService.getCatalogo(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), null);
                if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                    log.info("cadenaOriginal : " + cadenaOriginal);
                    cadenaDesencriptada = Crypto.aesDecrypt(Constantes.PASS_ADDCEL, cadenaOriginal);
                    log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                    estados = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
                    servicio = bundle.getString("servicioCatalogoTarjetasURL");
                    cadenaOriginal = consumeServiciosService.getCatalogo(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), null);
                    if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                        log.info("cadenaOriginal : " + cadenaOriginal);
                        cadenaDesencriptada = Crypto.aesDecrypt(Constantes.PASS_ADDCEL, cadenaOriginal);
                        log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                        tarjetas = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
                        cal = Calendar.getInstance();
                        request.setAttribute("estados", estados);
                        request.setAttribute("tarjetas", tarjetas);
                        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                        request.setAttribute("yearFrom", AesCtr.encrypt(Integer.toString(cal.get(Calendar.YEAR)), keyResponse, 256));
                        request.setAttribute("yearTo", AesCtr.encrypt(Integer.toString(cal.get(Calendar.YEAR) + 15), keyResponse, 256));
                        request.setAttribute("curMonth", AesCtr.encrypt(Integer.toString(cal.get(Calendar.MONTH) + 1), keyResponse, 256));
                        forward = "datosComplementarios2";
                    } else {
                        request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
                        forward = "error";
                    }
                } else {
                    request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
                    forward = "error";
                }
            }
        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al completar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }
    public ActionForward datosComplementarios(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
               JSONObject jsonObj = null;
        UsuarioVO usuarioVO = null;
        String protocol = "http://";
        String keyResponse = null;
        String keyRequest = null;
        DatosComplementariosDosForm datos = null;
        String forward = null;
        DateFormat df = null;
        DatosIAVE_VO datosIAVE = null;
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String jEnc = null;
        String newEnc = null;
        HashMap parametros = null;
        String resultado[] = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                datos = (DatosComplementariosDosForm) form;
                usuarioVO = (UsuarioVO) request.getSession().getAttribute("usuarioVO");
                datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
                usuarioVO.setEstado(datos.getEstado());
                usuarioVO.setCiudad(datos.getCiudad());
                usuarioVO.setCalle(datos.getCalle());
                usuarioVO.setNumExt(datos.getNumExt());
                usuarioVO.setNumInt(datos.getNumInt());
                usuarioVO.setColonia(datos.getColonia());
                usuarioVO.setCodigoPostal(datos.getCodigoPostal());
                usuarioVO.setNumTarjeta(datos.getNumTarjeta());
                usuarioVO.setTipoTarjeta(datos.getTipoTarjeta());
                usuarioVO.setDomicilioAMEX(datos.getDomicilioAMEX());
                usuarioVO.setVigenciaTarjeta(datos.getVigenciaTarjeta());
                usuarioVO.setTerminos(datos.getTerminos());
                usuarioVO.setPlataforma(datos.getPlataforma());
                log.info(usuarioVO);
                request.getSession().setAttribute("usuarioVO", usuarioVO);
                df = new SimpleDateFormat("dd/MM/yyyy");
                jsonObj = new JSONObject();
                if (usuarioVO.getNombreUsuario() != null) {
                    jsonObj.put("login", usuarioVO.getNombreUsuario());
                }
                if (usuarioVO.getFechaNac() != null && usuarioVO.getFechaNac().length() > 0) {
                    jsonObj.put("nacimiento", usuarioVO.getFechaNac());
                }
                if (usuarioVO.getNumCelular() != null) {
                    jsonObj.put("telefono", usuarioVO.getNumCelular());
                }
                jsonObj.put("registro", df.format(new Date()));
                if (usuarioVO.getNombre() != null) {
                    jsonObj.put("nombre", usuarioVO.getNombre());
                }
                if (usuarioVO.getApellidoP() != null) {
                    jsonObj.put("apellido", usuarioVO.getApellidoP());
                }
                if (usuarioVO.getApellidoM() != null) {
                    jsonObj.put("materno", usuarioVO.getApellidoM());
                }
                if (usuarioVO.getSexo() != null) {
                    jsonObj.put("sexo", usuarioVO.getSexo());
                }
                if (usuarioVO.getTelefonoCasa() != null) {
                    jsonObj.put("tel_casa", usuarioVO.getTelefonoCasa());
                }
                if (usuarioVO.getTelefonoOficina() != null) {
                    jsonObj.put("tel_oficina", usuarioVO.getTelefonoOficina());
                }
                if (usuarioVO.getCiudad() != null) {
                    jsonObj.put("ciudad", usuarioVO.getCiudad());
                }
                if (usuarioVO.getCalle() != null) {
                    jsonObj.put("calle", usuarioVO.getCalle());
                }
                if (usuarioVO.getNumExt() != null) {
                    jsonObj.put("num_ext", Integer.parseInt(usuarioVO.getNumExt()));
                }
                if (usuarioVO.getNumInt() != null) {
                    jsonObj.put("num_interior", usuarioVO.getNumInt());
                }
                if (usuarioVO.getColonia() != null) {
                    jsonObj.put("colonia", usuarioVO.getColonia());
                }
                if(usuarioVO.getTipoTarjeta().equals("3")){
                    if (usuarioVO.getCodigoPostalAMEX()!= null) {
                        jsonObj.put("cp", usuarioVO.getCodigoPostalAMEX());
                    }
                }else{
                    if (usuarioVO.getCodigoPostal() != null) {
                        jsonObj.put("cp", usuarioVO.getCodigoPostal());
                    }
                }
                if (usuarioVO.getDomicilioAMEX() != null) {
                    jsonObj.put("dom_amex", usuarioVO.getDomicilioAMEX());
                }
                if (usuarioVO.getEstado() != null) {
                    jsonObj.put("id_estado", Integer.parseInt(usuarioVO.getEstado()));
                }
                jsonObj.put("direccion", "");
                if (usuarioVO.getEmail() != null) {
                    jsonObj.put("mail", usuarioVO.getEmail());
                }
                if (usuarioVO.getNumTarjeta() != null) {
                    jsonObj.put("tarjeta", usuarioVO.getNumTarjeta());
                }
                if (usuarioVO.getVigenciaTarjeta() != null) {
                    jsonObj.put("vigencia", usuarioVO.getVigenciaTarjeta());
                }
                jsonObj.put("banco", 0);
                if (usuarioVO.getTipoTarjeta() != null) {
                    jsonObj.put("tipotarjeta", Integer.parseInt(usuarioVO.getTipoTarjeta()));
                }
                if (usuarioVO.getProveedor() != null) {
                    jsonObj.put("proveedor", Integer.parseInt(usuarioVO.getProveedor()));
                }
                jsonObj.put("status", 1000);
                if (datosIAVE.getImei() != null) {
                    jsonObj.put("imei", "IAVE" + datosIAVE.getImei());
                }

                jsonObj.put("etiqueta", "");
                jsonObj.put("numero", "");
                jsonObj.put("tipo", usuarioVO.getPlataforma());
                jsonObj.put("software", "1.1.1");
                jsonObj.put("modelo", "web");
                jsonObj.put("terminos", "1");
                if (datosIAVE.getImei() != null) {
                    jsonObj.put("key", datosIAVE.getImei());
                }

                if (request.isSecure()) {
                    protocol = "https://";
                }
                log.info(jsonObj);
                urlBase = bundle.getString("urlbase");
                servicio = bundle.getString("newUserURL");
                jEnc = Crypto.aesEncrypt(Utils.parsePass(Constantes.PASS_NEWUSER), jsonObj.toString());
                newEnc = Utils.mergeStr(jEnc, Constantes.PASS_NEWUSER);

                log.info(new StringBuilder(protocol).append(urlBase).append(servicio).append("?json=").append(newEnc).toString());
                parametros = new HashMap();
                parametros.put("json", newEnc);
                cadenaOriginal = consumeServiciosService.registraUsuario(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametros);
                if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                    log.info("cadenaOriginal : " + cadenaOriginal);
                    cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(Constantes.PASS_NEWUSER), cadenaOriginal);
                    log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                    jsonObj = new JSONObject(cadenaDesencriptada);
                    resultado = new String[2];
                    resultado[0] = (String) Integer.toString((Integer) jsonObj.get("resultado"));
                    resultado[1] = (String) jsonObj.get("mensaje");
                    if (resultado[0].equals("1")) {
                        consultasService.insertaMapeoUsuario(datosIAVE.getIduser(), usuarioVO.getNombreUsuario());
                        request.setAttribute("mensajeExito", resultado[1]);
                        request.getSession().setAttribute("loginMobileCard", usuarioVO.getNombreUsuario());
                        request.setAttribute("nextPage","goToRecargaIAVE");
                        forward = "recarga";
                    } else {
                        request.setAttribute("titulo", "Error");
                        request.setAttribute("errorMensaje", resultado[1]);
                        forward = "error";
                    }
                } else {
                    request.setAttribute("titulo", "Error");
                    request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
                    forward = "error";
                }
            }
        } catch (Exception e) {
            log.error("Error al regsitrar usuario : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Ocurrio un error al realizar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }

    /**
     * @param consumeServiciosService the consumeServiciosService to set
     */
    public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
        this.consumeServiciosService = consumeServiciosService;
    }

    /**
     * @param consultasService the consultasService to set
     */
    public void setConsultasService(ConsultasService consultasService) {
        this.consultasService = consultasService;
    }
}
