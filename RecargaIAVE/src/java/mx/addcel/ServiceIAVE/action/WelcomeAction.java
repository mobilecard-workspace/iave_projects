/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author victor
 */
public class WelcomeAction extends DispatchAction {

    public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String forward = null;
        String keyResponse = null;
        try {
            log.info("Entro a WelcomeAction inicio");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (keyResponse == null || keyResponse.length() == 0) {
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                request.setAttribute("nextPage", "goToWelcome");
                forward = "welcomePage";
            }
        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al completar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }

    public ActionForward iniciaRegistro(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String forward = null;
        String keyResponse = null;
        try {
            log.info("Entro a WelcomeAction iniciaRegistro");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (keyResponse == null || keyResponse.length() == 0) {
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                request.setAttribute("nextPage", "goToDatosComplementariosUno");
                forward = "nuevoUsuario";
            }
        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al completar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }
}
