/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import java.util.Enumeration;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.form.LoginForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author victor
 */
public class LoginAction extends DispatchAction {

    private ConsumeServiciosService consumeServiciosService;
    private ConsultasService consultasService;
    private Logger log = Logger.getLogger(LoginAction.class);
    private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

    /**
     * @param consultasService the consultasService to set
     */
    public void setConsultasService(ConsultasService consultasService) {
        this.consultasService = consultasService;
    }

    public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String forward = null;
        String keyResponse = null;
        String keyRequest = null;
        String nextPage = null;
        try {
            log.info("Entro inicio");
            nextPage = (String) request.getAttribute("nextPage");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (GenericValidator.isBlankOrNull(keyResponse) || GenericValidator.isBlankOrNull(keyRequest)) {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                forward = "login";
            }
        } catch (Exception e) {
            log.fatal("Error al validar login : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al realizar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }

    public ActionForward loginUsuario(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String element = null;
        Enumeration en = null;
        String forward = null;
        String keyResponse = null;
        String keyRequest = null;
        String resultado[] = null;
        DatosIAVE_VO datosIAVE = null;
        LoginForm loginForm = null;
        log.info("Entro loginUsuario");
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                loginForm = (LoginForm) form;
                datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
                resultado = Utils.validacionLogin(bundle, consumeServiciosService, datosIAVE.getImei(), loginForm.getLogin(), loginForm.getPassword(), request.isSecure());
                if (resultado != null && resultado[0].equals("1")) {
                    datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
                    consultasService.insertaMapeoUsuario(datosIAVE.getIduser(), loginForm.getLogin());
                    request.setAttribute("nextPage", "goToRecargaIAVE");
                    request.getSession().setAttribute("loginMobileCard", loginForm.getLogin());
                    forward = "recargaIAVE";
                } else {
                    request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                    request.setAttribute("titulo", AesCtr.encrypt("Falla al autenticar usuario", keyResponse, 256));
                    request.setAttribute("errorMensaje", AesCtr.encrypt((String) resultado[1], keyResponse, 256));
                    request.setAttribute("nextPage", "goToLogin");
                    forward = "loginRedirect";
                }
            }
        } catch (Exception e) {
            log.fatal("Error al validar login : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al realizar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }

    /**
     * @param consumeServiciosService the consumeServiciosService to set
     */
    public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
        this.consumeServiciosService = consumeServiciosService;
    }
}
