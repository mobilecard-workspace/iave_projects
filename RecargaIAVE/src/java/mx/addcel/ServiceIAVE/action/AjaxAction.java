/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import com.ironbit.mc.system.crypto.Crypto;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Constantes;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.util.PropertyMessageResources;
import org.json.JSONObject;

/**
 *
 * @author victor
 */
public class AjaxAction extends DispatchAction {

    private ConsumeServiciosService consumeServiciosService;
    private ConsultasService consultasService;
    private Logger log = Logger.getLogger(AjaxAction.class);
    private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

    public ActionForward terminosCondiciones(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String urlBase = null;
        String servicio = null;
        String protocol = "http://";
        String keyResponse = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String terminos = null;
        PrintWriter out = null;
        JSONObject json = null;
        PropertyMessageResources messagesRB = null;
        try {
            log.info("Entro a terminosCondiciones");
            messagesRB = (PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (keyResponse == null || keyResponse.length() == 0) {
                json = new JSONObject();
                json.put("error", "-2");
                json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.sessionNoValida")));
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                urlBase = bundle.getString("urlbase");
                servicio = bundle.getString("servicioCondicionesURL");
                cadenaOriginal = consumeServiciosService.getCatalogo(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), null);
                if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                    log.info("cadenaOriginal : " + cadenaOriginal);
                    cadenaDesencriptada = Crypto.aesDecrypt(Constantes.PASS_ADDCEL, cadenaOriginal);
                    json = new JSONObject(cadenaDesencriptada);
                    terminos = (String) json.get("Descripcion");
                    terminos = terminos.replace("\n", "<br/>");
                    if (terminos != null) {
                        terminos = AesCtr.encrypt(terminos, keyResponse, 256);
                        json.put("Descripcion", terminos);
                    }
                    json.put("llavePublica", request.getSession().getAttribute("publicKey"));
                    json.put("error", "0");
                } else {
                    json = new JSONObject();
                    json.put("error", "-3");
                    json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.servicioTerminosNoDisponible")));
                }
            }
        } catch (Exception e) {
            log.fatal("Error al obtener terminos: ", e);
            json = new JSONObject();
            json.put("error", "-1");
            json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.errorInesperadoEnLaAplicacion", "T&eacute;rminos y condiciones")));

        } finally {
            try {
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(json.toString());
                log.info(json.toString());
                out.flush();
                out.close();
            } catch (Exception e) {
                log.fatal("Error al escribir terminos y condiciones : ", e);
            }
        }
        return null;
    }

    public ActionForward validaLogin(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String login = null;
        String keyResponse = null;
        String keyRequest = null;
        PrintWriter out = null;
        JSONObject json = null;
        PropertyMessageResources messagesRB = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                json = new JSONObject();
                json.put("error", "-2");
                json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.sessionNoValida")));
            } else {
                json = new JSONObject();
                login = request.getParameter("login");
                if (login != null && login.length() > 0) {
                    login = AesCtr.decrypt(login, keyRequest, 256);
                    if (!GenericValidator.isBlankOrNull(login)) {
                        if (consultasService.existeUsuario(login) > 0) {
                            json.put("valido", "0");
                            json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.loginExistente")));
                        } else {
                            json.put("valido", "1");
                        }
                        json.put("error", "0");
                    } else {
                        json.put("error", "-3");
                        json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.parametroNoExistente", "Login")));
                    }
                } else {
                    json.put("error", "-4");
                    json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.parametroNoExistente", "Login")));
                }
            }
        } catch (Exception e) {
            log.fatal("Error al validar login: ", e);
            json = new JSONObject();
            json.put("error", "-1");
            json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.errorInesperadoEnLaAplicacion", "validar login")));

        } finally {
            try {
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
                log.info(json.toString());
                out.flush();
                out.close();
            } catch (Exception e) {
                log.fatal("Error al validar login : ", e);
            }
        }
        return null;

    }

    public ActionForward validaTDC(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String tdc = null;
        String login = null;
        PrintWriter out = null;
        JSONObject json = null;
        String keyResponse = null;
        String keyRequest = null;
        PropertyMessageResources messagesRB = null;
        try {
            messagesRB=(PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                json = new JSONObject();
                json.put("error", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.sessionNoValida")));
            } else {
                json = new JSONObject();
                tdc = request.getParameter("tdc");
                if (tdc != null && tdc.trim().length() > 0) {
                    tdc = AesCtr.decrypt(tdc, keyRequest, 256);
                    login = consultasService.existeTDC(tdc);
                    if (login != null && login.length() > 0) {
                        json.put("valido", "0");
                        json.put("login", login);
                    } else {
                        json.put("valido", "1");
                    }
                    json.put("error", "0");
                } else {
                    json.put("error",  StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.parametroNoExistente","TDC")));
                }
            }
        } catch (Exception e) {
            log.fatal("Error al obtener terminos: ", e);
            json = new JSONObject();
            json.put("error",  StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.errorInesperadoEnLaAplicacion", "validar TDC")));

        } finally {
            try {
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
                log.info(json.toString());
                out.flush();
                out.close();
            } catch (Exception e) {
                log.fatal("Error al validar login");
            }
        }
        return null;
    }

    public ActionForward validaEmail(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String login = null;
        String email = null;
        PrintWriter out = null;
        JSONObject json = null;
        String keyResponse = null;
        String keyRequest = null;
        PropertyMessageResources messagesRB = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                json = new JSONObject();
                json.put("error", "-2");
                json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.sessionNoValida")));
            } else {
                json = new JSONObject();
                email = request.getParameter("email");
                if (email != null && email.length() > 0) {
                    email = AesCtr.decrypt(email, keyRequest, 256);
                    if (email != null && email.length() > 0) {
                        login = consultasService.existeEmail(email);
                        if (login != null && login.length() > 0) {
                            json.put("valido", "0");
                            json.put("login", login);
                            json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.emailExistente")));
                        } else {
                            json.put("valido", "1");
                        }
                        json.put("error", "0");
                    } else {
                        json.put("error", "-4");
                        json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.parametroNoExistente", "email")));
                    }
                } else {
                    json.put("error", "-3");
                    json.put("errorMensaje", StrutsMensajes.getErrorMessage(messagesRB, new ActionMessage("AjaxAction.parametroNoExistente", "email")));
                }
            }
        } catch (Exception e) {
            log.fatal("Error al validar email: ", e);
            json = new JSONObject();
            json.put("error", "-1");

        } finally {
            try {
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
                log.info(json.toString());
                out.flush();
                out.close();
            } catch (Exception e) {
                log.fatal("Error al validar login");
            }
        }
        return null;

    }

    public ActionForward recuperaPass(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String login = null;
        PrintWriter out = null;
        JSONObject json = null;
        JSONObject jsonObj = null;
        int count = 0;
        String keyResponse = null;
        String keyRequest = null;
        String urlBase = null;
        String servicio = null;
        String jEnc = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String respuesta = null;
        String protocol = "http://";
        log.info("Entro a recuperaPass");
        HashMap parametros = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                json = new JSONObject();
                json.put("error", "-4");
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                json = new JSONObject();
                login = request.getParameter("login");
                if (login != null && login.length() > 0) {
                    login = AesCtr.decrypt(login, keyRequest, 256);
                    count = consultasService.existeUsuario(login);
                    if (count > 0) {
                        jsonObj = new JSONObject();
                        jsonObj.put("cadena", login);
                        log.info(jsonObj);
                        jEnc = Crypto.aesEncrypt(Constantes.PASS_ADDCEL, jsonObj.toString());
                        parametros = new HashMap();
                        parametros.put("json", jEnc);
                        urlBase = bundle.getString("urlbase");
                        servicio = bundle.getString("servicioRecuperaPassURL");
                        cadenaOriginal = consumeServiciosService.getCatalogo(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametros);
                        if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                            log.info("cadenaOriginal : " + cadenaOriginal);
                            cadenaDesencriptada = Crypto.aesDecrypt(Constantes.PASS_ADDCEL, cadenaOriginal);
                            log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                            if (cadenaDesencriptada != null && cadenaDesencriptada.equals("0")) {
                                json.put("valido", "1");
                            } else {
                                json.put("error", "-5");
                            }
                        } else {
                            json.put("error", "-4");
                        }
                    } else {
                        json.put("valido", "0");
                    }
                    json.put("error", "0");
                } else {
                    json.put("error", "-3");
                }
            }
        } catch (Exception e) {
            log.fatal("Error al validar login: ", e);
            json = new JSONObject();
            json.put("error", "-1");

        } finally {
            try {
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
                log.info(json.toString());
                out.flush();
                out.close();
            } catch (Exception e) {
                log.fatal("Error al validar login, envio de json");
            }
        }
        return null;
    }

    public ActionForward validaDn(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String dn = null;
        PrintWriter out = null;
        JSONObject json = null;
        String login = null;
        String keyResponse = null;
        String keyRequest = null;
        log.info("Entro a validaDn");
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                json = new JSONObject();
                json.put("error", "-4");
            } else {
                json = new JSONObject();
                dn = request.getParameter("dn");
                if (dn != null && dn.length() > 0) {
                    dn = AesCtr.decrypt(dn, keyRequest, 256);
                    login = consultasService.existeTelefono(dn);
                    if (login != null && login.length() > 0) {
                        json.put("valido", "0");
                        json.put("login", login);
                    } else {
                        json.put("valido", "1");
                    }
                    json.put("error", "0");
                } else {
                    json.put("error", "-3");
                }
            }
        } catch (Exception e) {
            log.fatal("Error al validar dn: ", e);
            json = new JSONObject();
            json.put("error", "-1");

        } finally {
            try {
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(AesCtr.encrypt(json.toString(), keyResponse, 256));
                log.info(json.toString());
                out.flush();
                out.close();
            } catch (Exception e) {
                log.fatal("Error al validar dn, envio de json");
            }
        }
        return null;

    }

    /**
     * @param consumeServiciosService the consumeServiciosService to set
     */
    public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
        this.consumeServiciosService = consumeServiciosService;
    }

    /**
     * @param consultasService the consultasService to set
     */
    public void setConsultasService(ConsultasService consultasService) {
        this.consultasService = consultasService;
    }
}
