/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import com.ironbit.mc.system.crypto.Crypto;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.form.DatosComplementariosUnoForm;
import mx.addcel.ServiceIAVE.form.DatosInicioForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Constantes;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author victor
 */
public class ComplementariosUnoAction extends DispatchAction{
    private Logger log =Logger.getLogger(ComplementariosUnoAction.class);
    private ConsumeServiciosService consumeServiciosService;
    private ConsultasService consultasService;
    private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

    public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String protocol = "http://";
        String keyResponse = null;
        String proveedores = null;
        String forward = null;
        DatosIAVE_VO datosIAVE = null;
        Calendar cal = null;
        String nuevoLogin = null;
        try {
            log.info("Entro a ComplementariosUnoAction inicio");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (keyResponse == null || keyResponse.length() == 0) {
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
                log.info(datosIAVE);
                urlBase = bundle.getString("urlbase");
                servicio = bundle.getString("servicioCatalogoProvidersURL");
                cadenaOriginal = consumeServiciosService.getCatalogo(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), null);
                if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                    log.info("cadenaOriginal : " + cadenaOriginal);
                    cadenaDesencriptada = Crypto.aesDecrypt(Constantes.PASS_ADDCEL, cadenaOriginal);
                    log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                    proveedores = AesCtr.encrypt(cadenaDesencriptada, keyResponse, 256);
                    nuevoLogin = datosIAVE.getEmail().substring(0, datosIAVE.getEmail().indexOf("@"));
                    if (consultasService.existeUsuario(nuevoLogin) > 0) {
                        nuevoLogin = "";
                    }
                    cal = Calendar.getInstance();
                    request.setAttribute("email", AesCtr.encrypt(datosIAVE.getEmail(), keyResponse, 256));
                    request.setAttribute("dn", AesCtr.encrypt(datosIAVE.getDn(), keyResponse, 256));
                    request.setAttribute("login", AesCtr.encrypt(nuevoLogin, keyResponse, 256));
                    request.setAttribute("yearFrom", AesCtr.encrypt(Integer.toString(cal.get(Calendar.YEAR) - 90), keyResponse, 256));
                    request.setAttribute("yearTo", AesCtr.encrypt(Integer.toString(cal.get(Calendar.YEAR) - 6), keyResponse, 256));
                    request.setAttribute("defaultYear", AesCtr.encrypt(Integer.toString(cal.get(Calendar.YEAR) - 30), keyResponse, 256));
                    request.setAttribute("proveedores", proveedores);
                    request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                    forward = "datosComplementarios";
                } else {
                    request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
                    forward = "error";
                }
            }
        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al completar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);

    }

     public ActionForward datosComplementarios(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String protocol = "http://";
        String keyRequest = null;
        String keyResponse = null;
        String forward = null;
        UsuarioVO usuarioVO = null;
        DatosComplementariosUnoForm datos = null;
        try {
            log.info("Entro a ComplementariosUnoAction datosComplementarios");
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyRequest == null || keyRequest.length() == 0 || keyResponse == null || keyResponse.length() == 0) {
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                datos = (DatosComplementariosUnoForm) form;
                usuarioVO = new UsuarioVO();
                usuarioVO.setNombreUsuario(datos.getNombreUsuario());
                usuarioVO.setNumCelular(datos.getNumCelular());
                usuarioVO.setProveedor(datos.getProveedor());
                usuarioVO.setNombre(datos.getNombre());
                usuarioVO.setApellidoP(datos.getApellidoP());
                usuarioVO.setApellidoM(datos.getApellidoM());
                usuarioVO.setFechaNac(datos.getFechaNac());
                usuarioVO.setSexo(datos.getSexo());
                usuarioVO.setTelefonoCasa(datos.getTelefonoCasa());
                usuarioVO.setTelefonoOficina(datos.getTelefonoOficina());
                usuarioVO.setEmail(datos.getEmail());
                log.info(usuarioVO);
                request.getSession().setAttribute("usuarioVO", usuarioVO);
                forward="datosComplementarios2";
                request.setAttribute("nextPage","goToDatosComplementariosDos");
            }
        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("errorMensaje", "Error al completar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }
    /**
     * @param consumeServiciosService the consumeServiciosService to set
     */
    public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
        this.consumeServiciosService = consumeServiciosService;
    }

    /**
     * @param consultasService the consultasService to set
     */
    public void setConsultasService(ConsultasService consultasService) {
        this.consultasService = consultasService;
    }
}
