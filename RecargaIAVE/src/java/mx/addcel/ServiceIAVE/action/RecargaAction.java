/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.action;

import com.ironbit.mc.system.crypto.Crypto;
import java.util.HashMap;
import java.util.ResourceBundle;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mx.addcel.ServiceIAVE.form.RecargaForm;
import mx.addcel.ServiceIAVE.service.ConsultasService;
import mx.addcel.ServiceIAVE.service.ConsumeServiciosService;
import mx.addcel.ServiceIAVE.service.utils.Utils;
import mx.addcel.ServiceIAVE.service.vo.DatosIAVE_VO;
import mx.addcel.ServiceIAVE.service.vo.UsuarioVO;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.json.JSONObject;

/**
 *
 * @author victor
 */
public class RecargaAction extends DispatchAction {

    private ConsumeServiciosService consumeServiciosService;
    private ConsultasService consultasService;
    private Logger log = Logger.getLogger(RecargaAction.class);
    private ResourceBundle bundle = ResourceBundle.getBundle("servicios");

    public ActionForward inicio(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String forward = null;
        String keyResponse = null;
        log.info("Entro RecargaAction inicio");
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (keyResponse == null || keyResponse.length() == 0 ) {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                forward = Utils.obtenDatosRecargaIAVE(bundle, consumeServiciosService, keyResponse, request);
            }
        } catch (Exception e) {
            log.fatal("Error al validar login : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al realizar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }
    
    public ActionForward realizaRecarga(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String urlBase = null;
        String servicio = null;
        String cadenaOriginal = null;
        String cadenaDesencriptada = null;
        String protocol = "http://";
        String keyResponse = null;
        String keyRequest = null;
        String forward = null;
        String respuesta = null;
        UsuarioVO usuarioVO = null;
        DatosIAVE_VO datosIAVE = null;
        RecargaForm datos = null;
        JSONObject jsonObj = null;
        String jEnc = null;
        String newEnc = null;
        HashMap parametros = null;
        String resultado[] = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            if (keyResponse == null || keyResponse.length() == 0 || keyRequest == null || keyRequest.length() == 0) {
                request.setAttribute("titulo", "Error");
                request.setAttribute("errorMensaje", "Error al validar session");
                forward = "error";
            } else {
                if (request.isSecure()) {
                    protocol = "https://";
                }
                datos = (RecargaForm) form;
                usuarioVO = (UsuarioVO) request.getSession().getAttribute("usuarioVO");
                if (usuarioVO == null) {
                    usuarioVO = new UsuarioVO();
                }
                datosIAVE = (DatosIAVE_VO) request.getSession().getAttribute("datosIAVE");
                usuarioVO.setPassMobileCard(datos.getPassMobileCard());
                usuarioVO.setPlataforma(datos.getPlataforma());
                usuarioVO.setCvv2(datos.getCvv2());
                usuarioVO.setMonto(datos.getMonto());
                usuarioVO.setNombreUsuario((String) request.getSession().getAttribute("loginMobileCard"));
                log.info(usuarioVO);
                resultado = Utils.validacionLogin(bundle,consumeServiciosService,datosIAVE.getImei(), usuarioVO.getNombreUsuario(), usuarioVO.getPassMobileCard(), request.isSecure());
                if (resultado != null && resultado[0].equals("1")) {
                    urlBase = bundle.getString("urlbase");
                    servicio = bundle.getString("servicioCompraIAVE");
                    jsonObj = new JSONObject();
                    jsonObj.put("login", usuarioVO.getNombreUsuario());
                    jsonObj.put("password", usuarioVO.getPassMobileCard());
                    jsonObj.put("cvv2", usuarioVO.getCvv2());
                    jsonObj.put("pin", datosIAVE.getPin());
                    jsonObj.put("tarjeta", datosIAVE.getTag());
                    jsonObj.put("vigencia", consultasService.tdcVigencia(usuarioVO.getNombreUsuario()));
                    jsonObj.put("producto", usuarioVO.getMonto());
                    jsonObj.put("imei", "IAVE" + datosIAVE.getImei());
                    jsonObj.put("cx", "");
                    jsonObj.put("cy", "");
                    jsonObj.put("tipo", usuarioVO.getPlataforma());
                    jsonObj.put("software", "1.1.1");
                    jsonObj.put("modelo", "web");
                    jsonObj.put("key", datosIAVE.getImei());
                    log.info(jsonObj);

                    jEnc = Crypto.aesEncrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), jsonObj.toString());
                    newEnc = Utils.mergeStr(jEnc, usuarioVO.getPassMobileCard());
                    parametros = new HashMap();
                    parametros.put("json", newEnc);
                    cadenaOriginal = consumeServiciosService.compraIAVE(new StringBuilder(protocol).append(urlBase).append(servicio).toString(), parametros);
                    if (cadenaOriginal != null && cadenaOriginal.length() > 0) {
                        log.info("cadenaOriginal : " + cadenaOriginal);
                        cadenaDesencriptada = Crypto.aesDecrypt(Utils.parsePass(usuarioVO.getPassMobileCard()), cadenaOriginal);
                        log.info("cadenaDesencriptada : " + cadenaDesencriptada);
                        jsonObj = new JSONObject(cadenaDesencriptada);
                        if (Integer.toString((Integer) jsonObj.get("resultado")).equals("1")) {
                            log.info("Pago exitoso");
                            request.setAttribute("llavePublica",  request.getSession().getAttribute("publicKey"));
                            request.setAttribute("aliasTag", AesCtr.encrypt(datosIAVE.getNombreTag(), keyResponse, 256));
                            request.setAttribute("tag", AesCtr.encrypt(datosIAVE.getTag(), keyResponse, 256));
                            request.setAttribute("mensage", AesCtr.encrypt((String) jsonObj.get("resultado"), keyResponse, 256));
                            request.setAttribute("autorizacion", AesCtr.encrypt((String) jsonObj.get("folio"), keyResponse, 256));
                            request.setAttribute("nextPage","goToConfirmacionPago");
                            forward = "pagoExitoso";
                        } else {
                            log.info("Error al realizar pago");
                            request.setAttribute("mensajeError", AesCtr.encrypt((String) jsonObj.get("mensaje"), keyResponse, 256));
                            request.setAttribute("nextPage","goToRecargaIAVE");
                            forward = "errorRecarga";
                        }
                    } else {
                        request.setAttribute("errorMensaje", "Error de comunicaci&oacute;n, intentelo nuevamente.");
                        forward = "error";
                    }
                } else {
                    log.info("Entro pass no valido");
                    request.setAttribute("mensajeError", AesCtr.encrypt("El password ingresado no coincide con el usuario MobileCard", keyResponse, 256));
                    request.setAttribute("nextPage","goToRecargaIAVE");
                    forward = "errorRecarga";
                }
            }
        } catch (Exception e) {
            log.fatal("Excepcion : ", e);
            request.setAttribute("titulo", "Error");
            request.setAttribute("errorMensaje", "Error al completar las operaciones");
            forward = "error";
        }
        return mapping.findForward(forward);
    }

    /**
     * @param consumeServiciosService the consumeServiciosService to set
     */
    public void setConsumeServiciosService(ConsumeServiciosService consumeServiciosService) {
        this.consumeServiciosService = consumeServiciosService;
    }

    /**
     * @param consultasService the consultasService to set
     */
    public void setConsultasService(ConsultasService consultasService) {
        this.consultasService = consultasService;
    }
}
