/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.dao;

import java.util.HashMap;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author victor
 */
public class ConsultasDAO extends SqlMapClientDaoSupport {

    public String existeUsuarioAsociado(String loginIAVE) {
        return (String) this.getSqlMapClientTemplate().queryForObject("obtenMapeoIAVE", loginIAVE);
    }

    public Integer existeUsuario(String login) {
        return (Integer) this.getSqlMapClientTemplate().queryForObject("existeUsuario", login);
    }

    public void insertaMapeoIAVE(HashMap parametros) {
        this.getSqlMapClientTemplate().insert("insertaMapeoIAVE", parametros);
    }

    public String existeTDC(String tdc) {
        return (String) this.getSqlMapClientTemplate().queryForObject("existeTDC", tdc);
    }

    public String existeEmail(String email) {
        return (String) this.getSqlMapClientTemplate().queryForObject("existeEmail", email);
    }

    public String existeTelefono(String dn) {
        return (String) this.getSqlMapClientTemplate().queryForObject("existeDN", dn);
    }

    public String tdcVigencia(String login) {
        return (String) this.getSqlMapClientTemplate().queryForObject("tdcVigencia", login);
    }
    public String existeIMEI(String imei){
        return (String) this.getSqlMapClientTemplate().queryForObject("existeIMEI", imei);
    }
}
