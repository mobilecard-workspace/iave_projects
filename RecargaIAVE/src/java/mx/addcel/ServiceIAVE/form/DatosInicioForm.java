/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author victor
 */
public class DatosInicioForm extends ActionForm {

    private String iduserEnc;
    private String dnEnc;
    private String emailEnc;
    private String imeiEnc;
    private String tagEnc;
    private String nombreTagEnc;
    private String nombreUsuarioEnc;
    private String numCelularEnc;
    private String proveedorEnc;
    private String nombreEnc;
    private String apellidoPEnc;
    private String apellidoMEnc;
    private String fechaNacEnc;
    private String sexoEnc;
    private String telefonoCasaEnc;
    private String telefonoOficinaEnc;
    private String estadoEnc;
    private String ciudadEnc;
    private String calleEnc;
    private String numExtEnc;
    private String numIntEnc;
    private String coloniaEnc;
    private String codigoPostalEnc;
    private String numTarjetaEnc;
    private String tipoTarjetaEnc;
    private String codigoPostalAMEXEnc;
    private String domicilioAMEXEnc;
    private String vigenciaTarjetaEnc;
    private String terminosEnc;
    private String loginEnc;
    private String passwordEnc;
    private String plataformaEnc;
    private String montoEnc;
    private String passMobileCardEnc;
    private String cvv2Enc;
    private String pinEnc;

    /**
     * @return the iduserEnc
     */
    public String getIduserEnc() {
        return iduserEnc;
    }

    /**
     * @param iduserEnc the iduserEnc to set
     */
    public void setIduserEnc(String iduserEnc) {
        this.iduserEnc = iduserEnc;
    }

    /**
     * @return the dnEnc
     */
    public String getDnEnc() {
        return dnEnc;
    }

    /**
     * @param dnEnc the dnEnc to set
     */
    public void setDnEnc(String dnEnc) {
        this.dnEnc = dnEnc;
    }

    /**
     * @return the emailEnc
     */
    public String getEmailEnc() {
        return emailEnc;
    }

    /**
     * @param emailEnc the emailEnc to set
     */
    public void setEmailEnc(String emailEnc) {
        this.emailEnc = emailEnc;
    }

    /**
     * @return the imeiEnc
     */
    public String getImeiEnc() {
        return imeiEnc;
    }

    /**
     * @param imeiEnc the imeiEnc to set
     */
    public void setImeiEnc(String imeiEnc) {
        this.imeiEnc = imeiEnc;
    }

    /**
     * @return the tagEnc
     */
    public String getTagEnc() {
        return tagEnc;
    }

    /**
     * @param tagEnc the tagEnc to set
     */
    public void setTagEnc(String tagEnc) {
        this.tagEnc = tagEnc;
    }

    /**
     * @return the nombreTagEnc
     */
    public String getNombreTagEnc() {
        return nombreTagEnc;
    }

    /**
     * @param nombreTagEnc the nombreTagEnc to set
     */
    public void setNombreTagEnc(String nombreTagEnc) {
        this.nombreTagEnc = nombreTagEnc;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setIduserEnc(null);
        setDnEnc(null);
        setEmailEnc(null);
        setImeiEnc(null);
        setTagEnc(null);
        setNombreTagEnc(null);
        setNombreUsuarioEnc(null);
        setNumCelularEnc(null);
        setProveedorEnc(null);
        setNombreEnc(null);
        setApellidoPEnc(null);
        setApellidoMEnc(null);
        setFechaNacEnc(null);
        setSexoEnc(null);
        setTelefonoCasaEnc(null);
        setTelefonoOficinaEnc(null);
        setEstadoEnc(null);
        setCiudadEnc(null);
        setCalleEnc(null);
        setNumExtEnc(null);
        setNumIntEnc(null);
        setColoniaEnc(null);
        setCodigoPostalEnc(null);
        setNumTarjetaEnc(null);
        setTipoTarjetaEnc(null);
        setCodigoPostalAMEXEnc(null);
        setDomicilioAMEXEnc(null);
        setVigenciaTarjetaEnc(null);
        setTerminosEnc(null);
        setLoginEnc(null);
        setPasswordEnc(null);
        setPlataformaEnc(null);
        setMontoEnc(null);
        setPassMobileCardEnc(null);
        setCvv2Enc(null);
    }

    /**
     * @return the nombreUsuarioEnc
     */
    public String getNombreUsuarioEnc() {
        return nombreUsuarioEnc;
    }

    /**
     * @param nombreUsuarioEnc the nombreUsuarioEnc to set
     */
    public void setNombreUsuarioEnc(String nombreUsuarioEnc) {
        this.nombreUsuarioEnc = nombreUsuarioEnc;
    }

    /**
     * @return the numCelularEnc
     */
    public String getNumCelularEnc() {
        return numCelularEnc;
    }

    /**
     * @param numCelularEnc the numCelularEnc to set
     */
    public void setNumCelularEnc(String numCelularEnc) {
        this.numCelularEnc = numCelularEnc;
    }

    /**
     * @return the proveedorEnc
     */
    public String getProveedorEnc() {
        return proveedorEnc;
    }

    /**
     * @param proveedorEnc the proveedorEnc to set
     */
    public void setProveedorEnc(String proveedorEnc) {
        this.proveedorEnc = proveedorEnc;
    }

    /**
     * @return the nombreEnc
     */
    public String getNombreEnc() {
        return nombreEnc;
    }

    /**
     * @param nombreEnc the nombreEnc to set
     */
    public void setNombreEnc(String nombreEnc) {
        this.nombreEnc = nombreEnc;
    }

    /**
     * @return the apellidoPEnc
     */
    public String getApellidoPEnc() {
        return apellidoPEnc;
    }

    /**
     * @param apellidoPEnc the apellidoPEnc to set
     */
    public void setApellidoPEnc(String apellidoPEnc) {
        this.apellidoPEnc = apellidoPEnc;
    }

    /**
     * @return the apellidoMEnc
     */
    public String getApellidoMEnc() {
        return apellidoMEnc;
    }

    /**
     * @param apellidoMEnc the apellidoMEnc to set
     */
    public void setApellidoMEnc(String apellidoMEnc) {
        this.apellidoMEnc = apellidoMEnc;
    }

    /**
     * @return the fechaNacEnc
     */
    public String getFechaNacEnc() {
        return fechaNacEnc;
    }

    /**
     * @param fechaNacEnc the fechaNacEnc to set
     */
    public void setFechaNacEnc(String fechaNacEnc) {
        this.fechaNacEnc = fechaNacEnc;
    }

    /**
     * @return the sexoEnc
     */
    public String getSexoEnc() {
        return sexoEnc;
    }

    /**
     * @param sexoEnc the sexoEnc to set
     */
    public void setSexoEnc(String sexoEnc) {
        this.sexoEnc = sexoEnc;
    }

    /**
     * @return the telefonoCasaEnc
     */
    public String getTelefonoCasaEnc() {
        return telefonoCasaEnc;
    }

    /**
     * @param telefonoCasaEnc the telefonoCasaEnc to set
     */
    public void setTelefonoCasaEnc(String telefonoCasaEnc) {
        this.telefonoCasaEnc = telefonoCasaEnc;
    }

    /**
     * @return the telefonoOficinaEnc
     */
    public String getTelefonoOficinaEnc() {
        return telefonoOficinaEnc;
    }

    /**
     * @param telefonoOficinaEnc the telefonoOficinaEnc to set
     */
    public void setTelefonoOficinaEnc(String telefonoOficinaEnc) {
        this.telefonoOficinaEnc = telefonoOficinaEnc;
    }

    /**
     * @return the estadoEnc
     */
    public String getEstadoEnc() {
        return estadoEnc;
    }

    /**
     * @param estadoEnc the estadoEnc to set
     */
    public void setEstadoEnc(String estadoEnc) {
        this.estadoEnc = estadoEnc;
    }

    /**
     * @return the ciudadEnc
     */
    public String getCiudadEnc() {
        return ciudadEnc;
    }

    /**
     * @param ciudadEnc the ciudadEnc to set
     */
    public void setCiudadEnc(String ciudadEnc) {
        this.ciudadEnc = ciudadEnc;
    }

    /**
     * @return the calleEnc
     */
    public String getCalleEnc() {
        return calleEnc;
    }

    /**
     * @param calleEnc the calleEnc to set
     */
    public void setCalleEnc(String calleEnc) {
        this.calleEnc = calleEnc;
    }

    /**
     * @return the numExtEnc
     */
    public String getNumExtEnc() {
        return numExtEnc;
    }

    /**
     * @param numExtEnc the numExtEnc to set
     */
    public void setNumExtEnc(String numExtEnc) {
        this.numExtEnc = numExtEnc;
    }

    /**
     * @return the numIntEnc
     */
    public String getNumIntEnc() {
        return numIntEnc;
    }

    /**
     * @param numIntEnc the numIntEnc to set
     */
    public void setNumIntEnc(String numIntEnc) {
        this.numIntEnc = numIntEnc;
    }

    /**
     * @return the coloniaEnc
     */
    public String getColoniaEnc() {
        return coloniaEnc;
    }

    /**
     * @param coloniaEnc the coloniaEnc to set
     */
    public void setColoniaEnc(String coloniaEnc) {
        this.coloniaEnc = coloniaEnc;
    }

    /**
     * @return the codigoPostalEnc
     */
    public String getCodigoPostalEnc() {
        return codigoPostalEnc;
    }

    /**
     * @param codigoPostalEnc the codigoPostalEnc to set
     */
    public void setCodigoPostalEnc(String codigoPostalEnc) {
        this.codigoPostalEnc = codigoPostalEnc;
    }

    /**
     * @return the numTarjetaEnc
     */
    public String getNumTarjetaEnc() {
        return numTarjetaEnc;
    }

    /**
     * @param numTarjetaEnc the numTarjetaEnc to set
     */
    public void setNumTarjetaEnc(String numTarjetaEnc) {
        this.numTarjetaEnc = numTarjetaEnc;
    }

    /**
     * @return the tipoTarjetaEnc
     */
    public String getTipoTarjetaEnc() {
        return tipoTarjetaEnc;
    }

    /**
     * @param tipoTarjetaEnc the tipoTarjetaEnc to set
     */
    public void setTipoTarjetaEnc(String tipoTarjetaEnc) {
        this.tipoTarjetaEnc = tipoTarjetaEnc;
    }

    /**
     * @return the codigoPostalAMEXEnc
     */
    public String getCodigoPostalAMEXEnc() {
        return codigoPostalAMEXEnc;
    }

    /**
     * @param codigoPostalAMEXEnc the codigoPostalAMEXEnc to set
     */
    public void setCodigoPostalAMEXEnc(String codigoPostalAMEXEnc) {
        this.codigoPostalAMEXEnc = codigoPostalAMEXEnc;
    }

    /**
     * @return the domicilioAMEXEnc
     */
    public String getDomicilioAMEXEnc() {
        return domicilioAMEXEnc;
    }

    /**
     * @param domicilioAMEXEnc the domicilioAMEXEnc to set
     */
    public void setDomicilioAMEXEnc(String domicilioAMEXEnc) {
        this.domicilioAMEXEnc = domicilioAMEXEnc;
    }

    /**
     * @return the vigenciaTarjetaEnc
     */
    public String getVigenciaTarjetaEnc() {
        return vigenciaTarjetaEnc;
    }

    /**
     * @param vigenciaTarjetaEnc the vigenciaTarjetaEnc to set
     */
    public void setVigenciaTarjetaEnc(String vigenciaTarjetaEnc) {
        this.vigenciaTarjetaEnc = vigenciaTarjetaEnc;
    }

    /**
     * @return the terminosEnc
     */
    public String getTerminosEnc() {
        return terminosEnc;
    }

    /**
     * @param terminosEnc the terminosEnc to set
     */
    public void setTerminosEnc(String terminosEnc) {
        this.terminosEnc = terminosEnc;
    }

    /**
     * @return the loginEnc
     */
    public String getLoginEnc() {
        return loginEnc;
    }

    /**
     * @param loginEnc the loginEnc to set
     */
    public void setLoginEnc(String loginEnc) {
        this.loginEnc = loginEnc;
    }

    /**
     * @return the passwordEnc
     */
    public String getPasswordEnc() {
        return passwordEnc;
    }

    /**
     * @param passwordEnc the passwordEnc to set
     */
    public void setPasswordEnc(String passwordEnc) {
        this.passwordEnc = passwordEnc;
    }

    /**
     * @return the plataformaEnc
     */
    public String getPlataformaEnc() {
        return plataformaEnc;
    }

    /**
     * @param plataformaEnc the plataformaEnc to set
     */
    public void setPlataformaEnc(String plataformaEnc) {
        this.plataformaEnc = plataformaEnc;
    }

    /**
     * @return the montoEnc
     */
    public String getMontoEnc() {
        return montoEnc;
    }

    /**
     * @param montoEnc the montoEnc to set
     */
    public void setMontoEnc(String montoEnc) {
        this.montoEnc = montoEnc;
    }

    /**
     * @return the passMobileCardEnc
     */
    public String getPassMobileCardEnc() {
        return passMobileCardEnc;
    }

    /**
     * @param passMobileCardEnc the passMobileCardEnc to set
     */
    public void setPassMobileCardEnc(String passMobileCardEnc) {
        this.passMobileCardEnc = passMobileCardEnc;
    }

    /**
     * @return the cvv2Enc
     */
    public String getCvv2Enc() {
        return cvv2Enc;
    }

    /**
     * @param cvv2Enc the cvv2Enc to set
     */
    public void setCvv2Enc(String cvv2Enc) {
        this.cvv2Enc = cvv2Enc;
    }

    /**
     * @return the pinEnc
     */
    public String getPinEnc() {
        return pinEnc;
    }

    /**
     * @param pinEnc the pinEnc to set
     */
    public void setPinEnc(String pinEnc) {
        this.pinEnc = pinEnc;
    }
}
