/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

/**
 *
 * @author victor
 */
public class DatosComplementariosDosForm extends ActionForm {

    private Logger log = Logger.getLogger(DatosComplementariosDosForm.class);
    private String estadoEnc;
    private String ciudadEnc;
    private String calleEnc;
    private String numExtEnc;
    private String numIntEnc;
    private String coloniaEnc;
    private String codigoPostalEnc;
    private String numTarjetaEnc;
    private String tipoTarjetaEnc;
    private String codigoPostalAMEXEnc;
    private String domicilioAMEXEnc;
    private String vigenciaTarjetaEnc;
    private String terminosEnc;
    private String plataformaEnc;
    private String estado;
    private String ciudad;
    private String calle;
    private String numExt;
    private String numInt;
    private String colonia;
    private String codigoPostal;
    private String numTarjeta;
    private String tipoTarjeta;
    private String codigoPostalAMEX;
    private String domicilioAMEX;
    private String vigenciaTarjeta;
    private String terminos;
    private String plataforma;

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setEstadoEnc(null);
        setCiudadEnc(null);
        setCalleEnc(null);
        setNumExtEnc(null);
        setNumIntEnc(null);
        setColoniaEnc(null);
        setCodigoPostalEnc(null);
        setNumTarjetaEnc(null);
        setTipoTarjetaEnc(null);
        setCodigoPostalAMEXEnc(null);
        setDomicilioAMEXEnc(null);
        setVigenciaTarjetaEnc(null);
        setTerminosEnc(null);
        setPlataformaEnc(null);
        setEstado(null);
        setCiudad(null);
        setCalle(null);
        setNumExt(null);
        setNumInt(null);
        setColonia(null);
        setCodigoPostal(null);
        setNumTarjeta(null);
        setTipoTarjeta(null);
        setCodigoPostalAMEX(null);
        setDomicilioAMEX(null);
        setVigenciaTarjeta(null);
        setTerminos(null);
        setPlataforma(null);
    }

    /**
     * @return the estadoEnc
     */
    public String getEstadoEnc() {
        return estadoEnc;
    }

    /**
     * @param estadoEnc the estadoEnc to set
     */
    public void setEstadoEnc(String estadoEnc) {
        this.estadoEnc = estadoEnc;
    }

    /**
     * @return the ciudadEnc
     */
    public String getCiudadEnc() {
        return ciudadEnc;
    }

    /**
     * @param ciudadEnc the ciudadEnc to set
     */
    public void setCiudadEnc(String ciudadEnc) {
        this.ciudadEnc = ciudadEnc;
    }

    /**
     * @return the calleEnc
     */
    public String getCalleEnc() {
        return calleEnc;
    }

    /**
     * @param calleEnc the calleEnc to set
     */
    public void setCalleEnc(String calleEnc) {
        this.calleEnc = calleEnc;
    }

    /**
     * @return the numExtEnc
     */
    public String getNumExtEnc() {
        return numExtEnc;
    }

    /**
     * @param numExtEnc the numExtEnc to set
     */
    public void setNumExtEnc(String numExtEnc) {
        this.numExtEnc = numExtEnc;
    }

    /**
     * @return the numIntEnc
     */
    public String getNumIntEnc() {
        return numIntEnc;
    }

    /**
     * @param numIntEnc the numIntEnc to set
     */
    public void setNumIntEnc(String numIntEnc) {
        this.numIntEnc = numIntEnc;
    }

    /**
     * @return the coloniaEnc
     */
    public String getColoniaEnc() {
        return coloniaEnc;
    }

    /**
     * @param coloniaEnc the coloniaEnc to set
     */
    public void setColoniaEnc(String coloniaEnc) {
        this.coloniaEnc = coloniaEnc;
    }

    /**
     * @return the codigoPostalEnc
     */
    public String getCodigoPostalEnc() {
        return codigoPostalEnc;
    }

    /**
     * @param codigoPostalEnc the codigoPostalEnc to set
     */
    public void setCodigoPostalEnc(String codigoPostalEnc) {
        this.codigoPostalEnc = codigoPostalEnc;
    }

    /**
     * @return the numTarjetaEnc
     */
    public String getNumTarjetaEnc() {
        return numTarjetaEnc;
    }

    /**
     * @param numTarjetaEnc the numTarjetaEnc to set
     */
    public void setNumTarjetaEnc(String numTarjetaEnc) {
        this.numTarjetaEnc = numTarjetaEnc;
    }

    /**
     * @return the tipoTarjetaEnc
     */
    public String getTipoTarjetaEnc() {
        return tipoTarjetaEnc;
    }

    /**
     * @param tipoTarjetaEnc the tipoTarjetaEnc to set
     */
    public void setTipoTarjetaEnc(String tipoTarjetaEnc) {
        this.tipoTarjetaEnc = tipoTarjetaEnc;
    }

    /**
     * @return the codigoPostalAMEXEnc
     */
    public String getCodigoPostalAMEXEnc() {
        return codigoPostalAMEXEnc;
    }

    /**
     * @param codigoPostalAMEXEnc the codigoPostalAMEXEnc to set
     */
    public void setCodigoPostalAMEXEnc(String codigoPostalAMEXEnc) {
        this.codigoPostalAMEXEnc = codigoPostalAMEXEnc;
    }

    /**
     * @return the domicilioAMEXEnc
     */
    public String getDomicilioAMEXEnc() {
        return domicilioAMEXEnc;
    }

    /**
     * @param domicilioAMEXEnc the domicilioAMEXEnc to set
     */
    public void setDomicilioAMEXEnc(String domicilioAMEXEnc) {
        this.domicilioAMEXEnc = domicilioAMEXEnc;
    }

    /**
     * @return the vigenciaTarjetaEnc
     */
    public String getVigenciaTarjetaEnc() {
        return vigenciaTarjetaEnc;
    }

    /**
     * @param vigenciaTarjetaEnc the vigenciaTarjetaEnc to set
     */
    public void setVigenciaTarjetaEnc(String vigenciaTarjetaEnc) {
        this.vigenciaTarjetaEnc = vigenciaTarjetaEnc;
    }

    /**
     * @return the terminosEnc
     */
    public String getTerminosEnc() {
        return terminosEnc;
    }

    /**
     * @param terminosEnc the terminosEnc to set
     */
    public void setTerminosEnc(String terminosEnc) {
        this.terminosEnc = terminosEnc;
    }

    /**
     * @return the plataformaEnc
     */
    public String getPlataformaEnc() {
        return plataformaEnc;
    }

    /**
     * @param plataformaEnc the plataformaEnc to set
     */
    public void setPlataformaEnc(String plataformaEnc) {
        this.plataformaEnc = plataformaEnc;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the calle
     */
    public String getCalle() {
        return calle;
    }

    /**
     * @param calle the calle to set
     */
    public void setCalle(String calle) {
        this.calle = calle;
    }

    /**
     * @return the numExt
     */
    public String getNumExt() {
        return numExt;
    }

    /**
     * @param numExt the numExt to set
     */
    public void setNumExt(String numExt) {
        this.numExt = numExt;
    }

    /**
     * @return the numInt
     */
    public String getNumInt() {
        return numInt;
    }

    /**
     * @param numInt the numInt to set
     */
    public void setNumInt(String numInt) {
        this.numInt = numInt;
    }

    /**
     * @return the colonia
     */
    public String getColonia() {
        return colonia;
    }

    /**
     * @param colonia the colonia to set
     */
    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    /**
     * @return the codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * @param codigoPostal the codigoPostal to set
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * @return the numTarjeta
     */
    public String getNumTarjeta() {
        return numTarjeta;
    }

    /**
     * @param numTarjeta the numTarjeta to set
     */
    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    /**
     * @return the tipoTarjeta
     */
    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    /**
     * @param tipoTarjeta the tipoTarjeta to set
     */
    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    /**
     * @return the codigoPostalAMEX
     */
    public String getCodigoPostalAMEX() {
        return codigoPostalAMEX;
    }

    /**
     * @param codigoPostalAMEX the codigoPostalAMEX to set
     */
    public void setCodigoPostalAMEX(String codigoPostalAMEX) {
        this.codigoPostalAMEX = codigoPostalAMEX;
    }

    /**
     * @return the domicilioAMEX
     */
    public String getDomicilioAMEX() {
        return domicilioAMEX;
    }

    /**
     * @param domicilioAMEX the domicilioAMEX to set
     */
    public void setDomicilioAMEX(String domicilioAMEX) {
        this.domicilioAMEX = domicilioAMEX;
    }

    /**
     * @return the vigenciaTarjeta
     */
    public String getVigenciaTarjeta() {
        return vigenciaTarjeta;
    }

    /**
     * @param vigenciaTarjeta the vigenciaTarjeta to set
     */
    public void setVigenciaTarjeta(String vigenciaTarjeta) {
        this.vigenciaTarjeta = vigenciaTarjeta;
    }

    /**
     * @return the terminos
     */
    public String getTerminos() {
        return terminos;
    }

    /**
     * @param terminos the terminos to set
     */
    public void setTerminos(String terminos) {
        this.terminos = terminos;
    }

    /**
     * @return the plataforma
     */
    public String getPlataforma() {
        return plataforma;
    }

    /**
     * @param plataforma the plataforma to set
     */
    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        String keyResponse = null;
        String keyRequest = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            String nextPage = (String) request.getAttribute("nextPage");
            log.info("DatosComplementariosDosForm validate");
            if (nextPage == null || !nextPage.equals("goToDatosComplementariosDos")) {
                if (GenericValidator.isBlankOrNull(keyResponse)||GenericValidator.isBlankOrNull(keyRequest)) {
                    errors.add(null, new ActionMessage("Application.llaveInexistente"));
                } else {
                    if (GenericValidator.isBlankOrNull(getEstadoEnc())) {
                        errors.add("estadoEnc", new ActionMessage("ComplementariosDosAction.estadoEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getCiudadEnc())) {
                        errors.add("ciudadEnc", new ActionMessage("ComplementariosDosAction.ciudadEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getCalleEnc())) {
                        errors.add("calleEnc", new ActionMessage("ComplementariosDosAction.calleEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getNumExtEnc())) {
                        errors.add("numExtEnc", new ActionMessage("ComplementariosDosAction.numExtEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getColoniaEnc())) {
                        errors.add("coloniaEnc", new ActionMessage("ComplementariosDosAction.coloniaEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getNumTarjetaEnc())) {
                        errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getTipoTarjetaEnc())) {
                        errors.add("tipoTarjetaEnc", new ActionMessage("ComplementariosDosAction.tipoTarjetaEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getVigenciaTarjetaEnc())) {
                        errors.add("vigenciaTarjetaEnc", new ActionMessage("ComplementariosDosAction.vigenciaTarjetaEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getTerminosEnc())) {
                        errors.add("terminosEnc", new ActionMessage("ComplementariosDosAction.terminosEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getPlataformaEnc())) {
                        errors.add("plataformaEnc", new ActionMessage("ComplementariosDosAction.plataformaEnc"));
                    }
                    if (errors.size() == 0) {
                        setEstado(AesCtr.decrypt(getEstadoEnc(), keyRequest, 256));
                        setCiudad(AesCtr.decrypt(getCiudadEnc(), keyRequest, 256));
                        setCalle(AesCtr.decrypt(getCalleEnc(), keyRequest, 256));
                        setNumExt(AesCtr.decrypt(getNumExtEnc(), keyRequest, 256));
                        setNumInt(AesCtr.decrypt(getNumIntEnc(), keyRequest, 256));
                        setColonia(AesCtr.decrypt(getColoniaEnc(), keyRequest, 256));
                        setCodigoPostal(AesCtr.decrypt(getCodigoPostalEnc(), keyRequest, 256));
                        setNumTarjeta(AesCtr.decrypt(getNumTarjetaEnc(), keyRequest, 256));
                        setTipoTarjeta(AesCtr.decrypt(getTipoTarjetaEnc(), keyRequest, 256));
                        setDomicilioAMEX(AesCtr.decrypt(getDomicilioAMEXEnc(), keyRequest, 256));
                        setVigenciaTarjeta(AesCtr.decrypt(getVigenciaTarjetaEnc(), keyRequest, 256));
                        setTerminos(AesCtr.decrypt(getTerminosEnc(), keyRequest, 256));
                        setPlataforma(AesCtr.decrypt(getPlataformaEnc(), keyRequest, 256));
                        if (GenericValidator.isBlankOrNull(getEstado())) {
                            errors.add("estadoEnc", new ActionMessage("ComplementariosDosAction.estadoEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getCiudad())) {
                            errors.add("ciudadEnc", new ActionMessage("ComplementariosDosAction.ciudadEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getCalle())) {
                            errors.add("calleEnc", new ActionMessage("ComplementariosDosAction.calleEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getNumExt())) {
                            errors.add("numExtEnc", new ActionMessage("ComplementariosDosAction.numExtEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getColonia())) {
                            errors.add("coloniaEnc", new ActionMessage("ComplementariosDosAction.coloniaEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getNumTarjeta())) {
                            errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getTipoTarjeta())) {
                            errors.add("tipoTarjetaEnc", new ActionMessage("ComplementariosDosAction.tipoTarjetaEnc"));
                        }
                        if (getTipoTarjeta() != null && getTipoTarjeta().equals("3")) {
                            if (GenericValidator.isBlankOrNull(getDomicilioAMEX())) {
                                errors.add("domicilioAMEXEnc", new ActionMessage("ComplementariosDosAction.domicilioAMEXEnc"));
                            }
                        }
                        if (getTipoTarjeta() != null && getTipoTarjeta().equals("3")) {
                            if (GenericValidator.isBlankOrNull(getCodigoPostalAMEX())) {
                                errors.add("codigoPostalAMEXEnc", new ActionMessage("ComplementariosDosAction.codigoPostalAMEXEnc"));
                            }
                        }
                        if (GenericValidator.isBlankOrNull(getVigenciaTarjeta())) {
                            errors.add("vigenciaTarjetaEnc", new ActionMessage("ComplementariosDosAction.vigenciaTarjetaEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getTerminos())) {
                            errors.add("terminosEnc", new ActionMessage("ComplementariosDosAction.terminosEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getPlataforma())) {
                            errors.add("plataformaEnc", new ActionMessage("ComplementariosDosAction.plataformaEnc"));
                        }
                    }
                    if (errors.size() == 0) {
                        if (!getEstado().matches("^\\d{1,2}$")) {
                            errors.add("estadoEnc", new ActionMessage("ComplementariosDosAction.estadoEnc.format"));
                        }
                        if (!getTipoTarjeta().matches("^\\d{1}$")) {
                            errors.add("tipoTarjetaEnc", new ActionMessage("ComplementariosDosAction.tipoTarjetaEnc.format"));
                        }
                        if (getTipoTarjeta() != null && getTipoTarjeta().equals("3")) {
                            if (!getNumTarjeta().matches("^\\d{15}$")) {
                                errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc.format"));
                            }
                        }else{
                            if (!getNumTarjeta().matches("^\\d{16}$")) {
                                errors.add("numTarjetaEnc", new ActionMessage("ComplementariosDosAction.numTarjetaEnc.format"));
                            }
                        }
                        if (!getVigenciaTarjeta().matches("^\\d{2}/\\d{2}$")) {
                            errors.add("vigenciaTarjetaEnc", new ActionMessage("ComplementariosDosAction.vigenciaTarjetaEnc.format"));
                        }
                        if (!getTerminos().matches("^[S]$")) {
                            errors.add("terminosEnc", new ActionMessage("ComplementariosDosAction.terminosEnc.format"));
                        }
                    }
                    if (errors.size() != 0) {
                        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                        StringBuilder sb = new StringBuilder();
                        Iterator<ActionMessage> iter = errors.get();
                        PropertyMessageResources p = (PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
                        while (iter.hasNext()) {
                            ActionMessage msg = iter.next();
                            sb.append(StrutsMensajes.getErrorMessage(p, msg));
                            sb.append("<br/>");
                        }
                        System.out.println(sb);
                        request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exeption en validacion DatosComplementariosDosForm : ", e);
            if (request.getAttribute("llavePublica") != null) {
                request.removeAttribute("llavePublica");
            }
            request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
        }
        return errors;
    }
}
