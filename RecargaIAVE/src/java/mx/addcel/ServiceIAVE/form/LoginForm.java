/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

/**
 *
 * @author victor
 */
public class LoginForm extends ActionForm {

    private Logger log = Logger.getLogger(ActionForm.class);
    private String loginEnc;
    private String passwordEnc;
    private String login;
    private String password;

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setLoginEnc(null);
        setPasswordEnc(null);
        setLogin(null);
        setPassword(null);
    }

    /**
     * @return the loginEnc
     */
    public String getLoginEnc() {
        return loginEnc;
    }

    /**
     * @param loginEnc the loginEnc to set
     */
    public void setLoginEnc(String loginEnc) {
        this.loginEnc = loginEnc;
    }

    /**
     * @return the passwordEnc
     */
    public String getPasswordEnc() {
        return passwordEnc;
    }

    /**
     * @param passwordEnc the passwordEnc to set
     */
    public void setPasswordEnc(String passwordEnc) {
        this.passwordEnc = passwordEnc;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        String keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
        String keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
        log.info("validate LoginForm");
        try {
            String nextPage = (String) request.getAttribute("nextPage");
            log.info("LoginForm validate");
            if (nextPage == null || !nextPage.equals("goToLogin")) {
                if (GenericValidator.isBlankOrNull(keyResponse) || GenericValidator.isBlankOrNull(keyRequest)){
                    errors.add(null, new ActionMessage("Application.llaveInexistente"));
                } else {
                    if (GenericValidator.isBlankOrNull(getLoginEnc())) {
                        errors.add("loginEnc", new ActionMessage("LoginAction.loginEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getPasswordEnc())) {
                        errors.add("passwordEnc", new ActionMessage("LoginAction.passwordEnc"));
                    }
                    if (errors.size() == 0) {
                        setLogin(AesCtr.decrypt(getLoginEnc(), keyRequest, 256));
                        setPassword(AesCtr.decrypt(getPasswordEnc(), keyRequest, 256));
                        log.info("validate LoginForm " + getLogin());
                        log.info("validate LoginForm " + getPassword());
                        if (GenericValidator.isBlankOrNull(getLogin())) {
                            errors.add("loginEnc", new ActionMessage("LoginAction.loginEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getPassword())) {
                            errors.add("passwordEnc", new ActionMessage("LoginAction.passwordEnc"));
                        }
                    }
                    if (errors.size() != 0) {
                        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                        StringBuilder sb = new StringBuilder();
                        Iterator<ActionMessage> iter = errors.get();
                        PropertyMessageResources p = (PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
                        while (iter.hasNext()) {
                            ActionMessage msg = iter.next();
                            sb.append(StrutsMensajes.getErrorMessage(p, msg));
                            sb.append("<br/>");
                        }
                        System.out.println(sb);
                        request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exeption en validacion LoginForm : ", e);
            if (request.getAttribute("llavePublica") != null) {
                request.removeAttribute("llavePublica");
            }
            request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
        }
        return errors;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
