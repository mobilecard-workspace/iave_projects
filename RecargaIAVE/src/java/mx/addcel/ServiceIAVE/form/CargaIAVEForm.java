/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

/**
 *
 * @author victor
 */
public class CargaIAVEForm extends ActionForm {

    private Logger log = Logger.getLogger(CargaIAVEForm.class);
    private String iduserEnc;
    private String dnEnc;
    private String emailEnc;
    private String imeiEnc;
    private String tagEnc;
    private String nombreTagEnc;
    private String pinEnc;
    private String iduser;
    private String dn;
    private String email;
    private String imei;
    private String tag;
    private String nombreTag;
    private String pin;

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setIduserEnc(null);
        setDnEnc(null);
        setEmailEnc(null);
        setImeiEnc(null);
        setTagEnc(null);
        setNombreTagEnc(null);
        setPinEnc(null);
        setIduser(null);
        setDn(null);
        setEmail(null);
        setImei(null);
        setTag(null);
        setNombreTag(null);
        setPin(null);
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        String keyResponse = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            if (GenericValidator.isBlankOrNull(keyResponse)) {
                errors.add(null, new ActionMessage("Application.llaveInexistente"));
            } else {
                if (GenericValidator.isBlankOrNull(getIduserEnc())) {
                    errors.add("idUserEnc", new ActionMessage("CargaIAVEAction.idUserEnc"));
                }
                if (GenericValidator.isBlankOrNull(getDnEnc())) {
                    errors.add("dnEnc", new ActionMessage("CargaIAVEAction.dnEnc"));
                }
                if (GenericValidator.isBlankOrNull(getEmailEnc())) {
                    errors.add("emailEnc", new ActionMessage("CargaIAVEAction.emailEnc"));
                }
                if (GenericValidator.isBlankOrNull(getImeiEnc())) {
                    errors.add("imeiEnc", new ActionMessage("CargaIAVEAction.imeiEnc"));
                }
                if (GenericValidator.isBlankOrNull(getTagEnc())) {
                    errors.add("tagEnc", new ActionMessage("CargaIAVEAction.tagEnc"));
                }
                if (GenericValidator.isBlankOrNull(getNombreTagEnc())) {
                    errors.add("nombreTagEnc", new ActionMessage("CargaIAVEAction.nombreTagEnc"));
                }
                if (GenericValidator.isBlankOrNull(getPinEnc())) {
                    errors.add("pinEnc", new ActionMessage("CargaIAVEAction.pinEnc"));
                }
                if (errors.size() == 0) {
                    setIduser(AesCtr.decrypt(getIduserEnc(), keyResponse, 256));
                    setDn(AesCtr.decrypt(getDnEnc(), keyResponse, 256));
                    setEmail(AesCtr.decrypt(getEmailEnc(), keyResponse, 256));
                    setImei(AesCtr.decrypt(getImeiEnc(), keyResponse, 256));
                    setTag(AesCtr.decrypt(getTagEnc(), keyResponse, 256));
                    setNombreTag(AesCtr.decrypt(getNombreTagEnc(), keyResponse, 256));
                    setPin(AesCtr.decrypt(getPinEnc(), keyResponse, 256));
                    if (GenericValidator.isBlankOrNull(getIduser())) {
                        errors.add("idUserEnc", new ActionMessage("CargaIAVEAction.idUserEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getDn())) {
                        errors.add("dnEnc", new ActionMessage("CargaIAVEAction.dnEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getEmail())) {
                        errors.add("emailEnc", new ActionMessage("CargaIAVEAction.emailEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getImei())) {
                        errors.add("imeiEnc", new ActionMessage("CargaIAVEAction.imeiEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getTag())) {
                        errors.add("tagEnc", new ActionMessage("CargaIAVEAction.tagEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getNombreTag())) {
                        errors.add("nombreTagEnc", new ActionMessage("CargaIAVEAction.nombreTagEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getPin())) {
                        errors.add("pinEnc", new ActionMessage("CargaIAVEAction.pinEnc"));
                    }
                    if (errors.size() == 0) {
                        if (!getIduser().matches("^[0-9]*$")) {
                            errors.add("idUserEnc", new ActionMessage("CargaIAVEAction.pinEnc.format"));
                        }
                        if (!getDn().matches("^\\d{10}$")) {
                            errors.add("dnEnc", new ActionMessage("CargaIAVEAction.dnEnc.format"));
                        }
                        if (!GenericValidator.isEmail(getEmail())) {
                            errors.add("emailEnc", new ActionMessage("CargaIAVEAction.emailEnc.format"));
                        }
                        if (!getTag().matches("^[0-9]*$")) {
                            errors.add("tagEnc", new ActionMessage("CargaIAVEAction.tagEnc.format"));
                        }
                        if (!getPin().matches("^\\d{1}$")) {
                            errors.add("pinEnc", new ActionMessage("CargaIAVEAction.pinEnc.format"));
                        }
                    }
                }
            }
            if (errors.size() != 0) {
                request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                StringBuilder sb = new StringBuilder();
                Iterator<ActionMessage> iter = errors.get();
                PropertyMessageResources p = (PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
                while (iter.hasNext()) {
                    ActionMessage msg = iter.next();
                    sb.append(StrutsMensajes.getErrorMessage(p, msg));
                    sb.append("<br/>");
                }
                System.out.println(sb);
                request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
            }
        } catch (Exception e) {
            log.error("Exeption en validacion CargaIAVEForm : ",e);
            if(request.getAttribute("llavePublica")!=null){
                request.removeAttribute("llavePublica");
            }
            request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
        }
        return errors;
    }

    /**
     * @return the iduserEnc
     */
    /**
     * @return the iduserEnc
     */
    public String getIduserEnc() {
        return iduserEnc;
    }

    /**
     * @param iduserEnc the iduserEnc to set
     */
    public void setIduserEnc(String iduserEnc) {
        this.iduserEnc = iduserEnc;
    }

    /**
     * @return the dnEnc
     */
    public String getDnEnc() {
        return dnEnc;
    }

    /**
     * @param dnEnc the dnEnc to set
     */
    public void setDnEnc(String dnEnc) {
        this.dnEnc = dnEnc;
    }

    /**
     * @return the emailEnc
     */
    public String getEmailEnc() {
        return emailEnc;
    }

    /**
     * @param emailEnc the emailEnc to set
     */
    public void setEmailEnc(String emailEnc) {
        this.emailEnc = emailEnc;
    }

    /**
     * @return the imeiEnc
     */
    public String getImeiEnc() {
        return imeiEnc;
    }

    /**
     * @param imeiEnc the imeiEnc to set
     */
    public void setImeiEnc(String imeiEnc) {
        this.imeiEnc = imeiEnc;
    }

    /**
     * @return the tagEnc
     */
    public String getTagEnc() {
        return tagEnc;
    }

    /**
     * @param tagEnc the tagEnc to set
     */
    public void setTagEnc(String tagEnc) {
        this.tagEnc = tagEnc;
    }

    /**
     * @return the nombreTagEnc
     */
    public String getNombreTagEnc() {
        return nombreTagEnc;
    }

    /**
     * @param nombreTagEnc the nombreTagEnc to set
     */
    public void setNombreTagEnc(String nombreTagEnc) {
        this.nombreTagEnc = nombreTagEnc;
    }

    /**
     * @return the pinEnc
     */
    public String getPinEnc() {
        return pinEnc;
    }

    /**
     * @param pinEnc the pinEnc to set
     */
    public void setPinEnc(String pinEnc) {
        this.pinEnc = pinEnc;
    }

    /**
     * @return the iduser
     */
    public String getIduser() {
        return iduser;
    }

    /**
     * @param iduser the iduser to set
     */
    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    /**
     * @return the dn
     */
    public String getDn() {
        return dn;
    }

    /**
     * @param dn the dn to set
     */
    public void setDn(String dn) {
        this.dn = dn;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the imei
     */
    public String getImei() {
        return imei;
    }

    /**
     * @param imei the imei to set
     */
    public void setImei(String imei) {
        this.imei = imei;
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return the nombreTag
     */
    public String getNombreTag() {
        return nombreTag;
    }

    /**
     * @param nombreTag the nombreTag to set
     */
    public void setNombreTag(String nombreTag) {
        this.nombreTag = nombreTag;
    }

    /**
     * @return the pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * @param pin the pin to set
     */
    public void setPin(String pin) {
        this.pin = pin;
    }
}
