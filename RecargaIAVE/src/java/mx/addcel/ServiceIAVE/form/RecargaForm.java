/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

/**
 *
 * @author victor
 */
public class RecargaForm extends ActionForm {

    private Logger log = Logger.getLogger(RecargaForm.class);
    private String tagAliasEnc;
    private String tagEnc;
    private String montoEnc;
    private String passMobileCardEnc;
    private String cvv2Enc;
    private String plataformaEnc;
    private String tagAlias;
    private String tag;
    private String monto;
    private String passMobileCard;
    private String cvv2;
    private String plataforma;

    
    
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setMontoEnc(null);
        setPassMobileCardEnc(null);
        setCvv2Enc(null);
        setTagAliasEnc(null);
        setTagEnc(null);
        setMonto(null);
        setPassMobileCard(null);
        setCvv2(null);
        setTagAlias(null);
        setTag(null);
        setPlataformaEnc(null);
        setPlataforma(null);
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        String keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
        String keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
        String nextPage = (String) request.getAttribute("nextPage");
        try {
            if (nextPage == null || !nextPage.equals("goToRecargaIAVE")) {
                if (GenericValidator.isBlankOrNull(keyResponse) || GenericValidator.isBlankOrNull(keyRequest)) {
                    errors.add(null, new ActionMessage("Application.llaveInexistente"));
                } else {
                    if (GenericValidator.isBlankOrNull(getTagAliasEnc())) {
                        errors.add("tagAliasEnc", new ActionMessage("RecargaAction.tagAliasEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getTagEnc())) {
                        errors.add("tagEnc", new ActionMessage("RecargaAction.tagEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getMontoEnc())) {
                        errors.add("montoEnc", new ActionMessage("RecargaAction.montoEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getPassMobileCardEnc())) {
                        errors.add("passMobileCardEnc", new ActionMessage("RecargaAction.passMobileCardEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getCvv2Enc())) {
                        errors.add("cvv2Enc", new ActionMessage("RecargaAction.cvv2Enc"));
                    }
                    log.info("Termino validacion 1");
                    if (errors.size() == 0) {
                        setTag(AesCtr.decrypt(getTagEnc(), keyRequest, 256));
                        setTagAlias(AesCtr.decrypt(getTagAliasEnc(), keyRequest, 256));
                        setMonto(AesCtr.decrypt(getMontoEnc(), keyRequest, 256));
                        setCvv2(AesCtr.decrypt(getCvv2Enc(), keyRequest, 256));
                        setPassMobileCard(AesCtr.decrypt(getPassMobileCardEnc(), keyRequest, 256));
                        setPlataforma(AesCtr.decrypt(getPlataformaEnc(), keyRequest, 256));
                        log.info("Inicio validacion 2");
                        if (GenericValidator.isBlankOrNull(getTagAlias())) {
                            errors.add("tagAliasEnc", new ActionMessage("RecargaAction.tagAliasEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getTag())) {
                            errors.add("tagEnc", new ActionMessage("RecargaAction.tagEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getMonto())) {
                            errors.add("montoEnc", new ActionMessage("RecargaAction.montoEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getPassMobileCard())) {
                            errors.add("passMobileCardEnc", new ActionMessage("RecargaAction.passMobileCardEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getCvv2())) {
                            errors.add("cvv2Enc", new ActionMessage("RecargaAction.cvv2Enc"));
                        }
                        if (errors.size() == 0) {
                            if (!getTag().matches("^\\d+$")) {
                                errors.add("tagEnc", new ActionMessage("RecargaAction.tagEnc.format"));
                            }
                            if (!getMonto().matches("^\\d{3}$")) {
                                errors.add("montoEnc", new ActionMessage("RecargaAction.montoEnc.format"));
                            }
                            if (!getCvv2().matches("^\\d{3,4}$")) {
                                errors.add("cvv2Enc", new ActionMessage("RecargaAction.cvv2Enc.format"));
                            }
                        }
                    }
                    if (errors.size() != 0) {
                        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                        StringBuilder sb = new StringBuilder();
                        Iterator<ActionMessage> iter = errors.get();
                        PropertyMessageResources p = (PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
                        while (iter.hasNext()) {
                            ActionMessage msg = iter.next();
                            sb.append(StrutsMensajes.getErrorMessage(p, msg));
                            sb.append("<br/>");
                        }
                        System.out.println(sb);
                        request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exeption en validacion CargaIAVEForm : ", e);
            if (request.getAttribute("llavePublica") != null) {
                request.removeAttribute("llavePublica");
            }
            request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
        }
        return errors;

    }

    /**
     * @return the tagAliasEnc
     */
    public String getTagAliasEnc() {
        return tagAliasEnc;
    }

    /**
     * @param tagAliasEnc the tagAliasEnc to set
     */
    public void setTagAliasEnc(String tagAliasEnc) {
        this.tagAliasEnc = tagAliasEnc;
    }

    /**
     * @return the tagEnc
     */
    public String getTagEnc() {
        return tagEnc;
    }

    /**
     * @param tagEnc the tagEnc to set
     */
    public void setTagEnc(String tagEnc) {
        this.tagEnc = tagEnc;
    }

    /**
     * @return the montoEnc
     */
    public String getMontoEnc() {
        return montoEnc;
    }

    /**
     * @param montoEnc the montoEnc to set
     */
    public void setMontoEnc(String montoEnc) {
        this.montoEnc = montoEnc;
    }

    /**
     * @return the passMobileCardEnc
     */
    public String getPassMobileCardEnc() {
        return passMobileCardEnc;
    }

    /**
     * @param passMobileCardEnc the passMobileCardEnc to set
     */
    public void setPassMobileCardEnc(String passMobileCardEnc) {
        this.passMobileCardEnc = passMobileCardEnc;
    }

    /**
     * @return the cvv2Enc
     */
    public String getCvv2Enc() {
        return cvv2Enc;
    }

    /**
     * @param cvv2Enc the cvv2Enc to set
     */
    public void setCvv2Enc(String cvv2Enc) {
        this.cvv2Enc = cvv2Enc;
    }

    /**
     * @return the plataformaEnc
     */
    public String getPlataformaEnc() {
        return plataformaEnc;
    }

    /**
     * @param plataformaEnc the plataformaEnc to set
     */
    public void setPlataformaEnc(String plataformaEnc) {
        this.plataformaEnc = plataformaEnc;
    }

    /**
     * @return the tagAlias
     */
    public String getTagAlias() {
        return tagAlias;
    }

    /**
     * @param tagAlias the tagAlias to set
     */
    public void setTagAlias(String tagAlias) {
        this.tagAlias = tagAlias;
    }

    /**
     * @return the tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tag the tag to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }

    /**
     * @return the monto
     */
    public String getMonto() {
        return monto;
    }

    /**
     * @param monto the monto to set
     */
    public void setMonto(String monto) {
        this.monto = monto;
    }

    /**
     * @return the passMobileCard
     */
    public String getPassMobileCard() {
        return passMobileCard;
    }

    /**
     * @param passMobileCard the passMobileCard to set
     */
    public void setPassMobileCard(String passMobileCard) {
        this.passMobileCard = passMobileCard;
    }

    /**
     * @return the cvv2
     */
    public String getCvv2() {
        return cvv2;
    }

    /**
     * @param cvv2 the cvv2 to set
     */
    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    /**
     * @return the plataforma
     */
    public String getPlataforma() {
        return plataforma;
    }

    /**
     * @param plataforma the plataforma to set
     */
    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }
}
