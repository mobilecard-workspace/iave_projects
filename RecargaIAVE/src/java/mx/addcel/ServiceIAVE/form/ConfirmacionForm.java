/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author victor
 */
public class ConfirmacionForm extends ActionForm{
    private Logger log = Logger.getLogger(ConfirmacionForm.class);
    public void reset(ActionMapping mapping, HttpServletRequest request) {        
    }
     public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        String keyResponse = null;
        String keyRequest = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            String nextPage = (String) request.getAttribute("nextPage");
            log.info("ConfirmacionForm validate");
            if (nextPage == null || !nextPage.equals("goToConfirmacionPago")) {
                if (GenericValidator.isBlankOrNull(keyResponse)||GenericValidator.isBlankOrNull(keyRequest)) {
                    errors.add(null, new ActionMessage("Application.llaveInexistente"));
                } else {
                    
                }
            }
        } catch (Exception e) {
            log.error("Exeption en validacion ConfirmacionForm : ", e);
            if (request.getAttribute("llavePublica") != null) {
                request.removeAttribute("llavePublica");
            }
            request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
        }
        return errors;
    }
}
