/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.addcel.ServiceIAVE.form;

import java.util.Iterator;
import javacryption.aes.AesCtr;
import javax.servlet.http.HttpServletRequest;
import mx.addcel.ServiceIAVE.service.utils.StrutsMensajes;
import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.Globals;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.util.PropertyMessageResources;

/**
 *
 * @author victor
 */
public class DatosComplementariosUnoForm extends ActionForm {

    private Logger log = Logger.getLogger(DatosComplementariosUnoForm.class);
    private String nombreUsuarioEnc;
    private String numCelularEnc;
    private String proveedorEnc;
    private String nombreEnc;
    private String apellidoPEnc;
    private String apellidoMEnc;
    private String fechaNacEnc;
    private String sexoEnc;
    private String emailEnc;
    private String telefonoCasaEnc;
    private String telefonoOficinaEnc;
    private String nombreUsuario;
    private String numCelular;
    private String proveedor;
    private String nombre;
    private String apellidoP;
    private String apellidoM;
    private String fechaNac;
    private String sexo;
    private String email;
    private String telefonoCasa;
    private String telefonoOficina;

    /**
     * @return the numCelularEnc
     */
    public String getNumCelularEnc() {
        return numCelularEnc;
    }

    /**
     * @param numCelularEnc the numCelularEnc to set
     */
    public void setNumCelularEnc(String numCelularEnc) {
        this.numCelularEnc = numCelularEnc;
    }

    /**
     * @return the proveedorEnc
     */
    public String getProveedorEnc() {
        return proveedorEnc;
    }

    /**
     * @param proveedorEnc the proveedorEnc to set
     */
    public void setProveedorEnc(String proveedorEnc) {
        this.proveedorEnc = proveedorEnc;
    }

    /**
     * @return the nombreEnc
     */
    public String getNombreEnc() {
        return nombreEnc;
    }

    /**
     * @param nombreEnc the nombreEnc to set
     */
    public void setNombreEnc(String nombreEnc) {
        this.nombreEnc = nombreEnc;
    }

    /**
     * @return the apellidoPEnc
     */
    public String getApellidoPEnc() {
        return apellidoPEnc;
    }

    /**
     * @param apellidoPEnc the apellidoPEnc to set
     */
    public void setApellidoPEnc(String apellidoPEnc) {
        this.apellidoPEnc = apellidoPEnc;
    }

    /**
     * @return the apellidoMEnc
     */
    public String getApellidoMEnc() {
        return apellidoMEnc;
    }

    /**
     * @param apellidoMEnc the apellidoMEnc to set
     */
    public void setApellidoMEnc(String apellidoMEnc) {
        this.apellidoMEnc = apellidoMEnc;
    }

    /**
     * @return the fechaNacEnc
     */
    public String getFechaNacEnc() {
        return fechaNacEnc;
    }

    /**
     * @param fechaNacEnc the fechaNacEnc to set
     */
    public void setFechaNacEnc(String fechaNacEnc) {
        this.fechaNacEnc = fechaNacEnc;
    }

    /**
     * @return the sexoEnc
     */
    public String getSexoEnc() {
        return sexoEnc;
    }

    /**
     * @param sexoEnc the sexoEnc to set
     */
    public void setSexoEnc(String sexoEnc) {
        this.sexoEnc = sexoEnc;
    }

    /**
     * @return the numCelular
     */
    public String getNumCelular() {
        return numCelular;
    }

    /**
     * @param numCelular the numCelular to set
     */
    public void setNumCelular(String numCelular) {
        this.numCelular = numCelular;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the apellidoP
     */
    public String getApellidoP() {
        return apellidoP;
    }

    /**
     * @param apellidoP the apellidoP to set
     */
    public void setApellidoP(String apellidoP) {
        this.apellidoP = apellidoP;
    }

    /**
     * @return the apellidoM
     */
    public String getApellidoM() {
        return apellidoM;
    }

    /**
     * @param apellidoM the apellidoM to set
     */
    public void setApellidoM(String apellidoM) {
        this.apellidoM = apellidoM;
    }

    /**
     * @return the fechaNac
     */
    public String getFechaNac() {
        return fechaNac;
    }

    /**
     * @param fechaNac the fechaNac to set
     */
    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        setNombreUsuarioEnc(null);
        setNumCelularEnc(null);
        setProveedorEnc(null);
        setNombreEnc(null);
        setApellidoPEnc(null);
        setApellidoMEnc(null);
        setFechaNacEnc(null);
        setSexoEnc(null);
        setEmailEnc(null);
        setTelefonoCasaEnc(null);
        setTelefonoOficinaEnc(null);
        setNombreUsuario(null);
        setNumCelular(null);
        setProveedor(null);
        setNombre(null);
        setApellidoP(null);
        setApellidoM(null);
        setFechaNac(null);
        setSexo(null);
        setEmail(null);
        setTelefonoCasa(null);
        setTelefonoOficina(null);
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        String keyResponse = null;
        String keyRequest = null;
        try {
            keyResponse = (String) request.getSession().getAttribute("jCryptionKey");
            keyRequest = (String) request.getSession().getAttribute("jCryptionKey_");
            String nextPage = (String) request.getAttribute("nextPage");
            log.info("DatosComplementariosUnoForm validate");
            if (nextPage == null || !nextPage.equals("goToDatosComplementariosUno")) {
                if (GenericValidator.isBlankOrNull(keyResponse)||GenericValidator.isBlankOrNull(keyRequest)) {
                    errors.add(null, new ActionMessage("Application.llaveInexistente"));
                } else {
                    if (GenericValidator.isBlankOrNull(getNombreUsuarioEnc())) {
                        errors.add("nombreUsuarioEnc", new ActionMessage("ComplementariosUnoAction.nombreUsuarioEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getNumCelularEnc())) {
                        errors.add("numCelularEnc", new ActionMessage("ComplementariosUnoAction.numCelularEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getProveedorEnc())) {
                        errors.add("proveedorEnc", new ActionMessage("ComplementariosUnoAction.proveedorEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getNombreEnc())) {
                        errors.add("nombreEnc", new ActionMessage("ComplementariosUnoAction.nombreEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getApellidoPEnc())) {
                        errors.add("apellidoPEnc", new ActionMessage("ComplementariosUnoAction.apellidoPEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getFechaNacEnc())) {
                        errors.add("fechaNacEnc", new ActionMessage("ComplementariosUnoAction.fechaNacEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getSexoEnc())) {
                        errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.sexoEnc"));
                    }
                    if (GenericValidator.isBlankOrNull(getEmailEnc())) {
                        errors.add("emailEnc", new ActionMessage("ComplementariosUnoAction.emailEnc"));
                    }
                    if (errors.size() == 0) {
                        setNombreUsuario(AesCtr.decrypt(getNombreUsuarioEnc(), keyRequest, 256));
                        setNumCelular(AesCtr.decrypt(getNumCelularEnc(), keyRequest, 256));
                        setProveedor(AesCtr.decrypt(getProveedorEnc(), keyRequest, 256));
                        setNombre(AesCtr.decrypt(getNombreEnc(), keyRequest, 256));
                        setApellidoP(AesCtr.decrypt(getApellidoPEnc(), keyRequest, 256));
                        setApellidoM(AesCtr.decrypt(getApellidoMEnc(), keyRequest, 256));
                        setFechaNac(AesCtr.decrypt(getFechaNacEnc(), keyRequest, 256));
                        setSexo(AesCtr.decrypt(getSexoEnc(), keyRequest, 256));
                        setTelefonoCasa(AesCtr.decrypt(getTelefonoCasaEnc(), keyRequest, 256));
                        setTelefonoOficina(AesCtr.decrypt(getTelefonoOficinaEnc(), keyRequest, 256));
                        setEmail(AesCtr.decrypt(getEmailEnc(), keyRequest, 256));
                        if (GenericValidator.isBlankOrNull(getNombreUsuario())) {
                            errors.add("nombreUsuarioEnc", new ActionMessage("ComplementariosUnoAction.nombreUsuarioEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getNumCelular())) {
                            errors.add("numCelularEnc", new ActionMessage("ComplementariosUnoAction.numCelularEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getProveedor())) {
                            errors.add("proveedorEnc", new ActionMessage("ComplementariosUnoAction.proveedorEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getNombre())) {
                            errors.add("nombreEnc", new ActionMessage("ComplementariosUnoAction.nombreEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getApellidoP())) {
                            errors.add("apellidoPEnc", new ActionMessage("ComplementariosUnoAction.apellidoPEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getFechaNac())) {
                            errors.add("fechaNacEnc", new ActionMessage("ComplementariosUnoAction.fechaNacEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getSexo())) {
                            errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.sexoEnc"));
                        }
                        if (GenericValidator.isBlankOrNull(getEmail())) {
                            errors.add("emailEnc", new ActionMessage("ComplementariosUnoAction.emailEnc"));
                        }
                    }
                    if (errors.size() == 0) {
                        if (!getNombreUsuario().matches("^[0-9a-zA-Z_.-]{0,35}$")) {
                            errors.add("nombreUsuarioEnc", new ActionMessage("ComplementariosUnoAction.nombreUsuarioEnc.format"));
                        }
                        if (!getNumCelular().matches("^\\d{10}$")) {
                            errors.add("numCelularEnc", new ActionMessage("ComplementariosUnoAction.numCelularEnc.format"));
                        }
                        if (!getProveedor().matches("^\\d$")) {
                            errors.add("proveedorEnc", new ActionMessage("ComplementariosUnoAction.proveedorEnc.format"));
                        }
                        if (!getNombre().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,35})$")) {
                            errors.add("nombreEnc", new ActionMessage("ComplementariosUnoAction.nombreEnc.format"));
                        }
                        if (!getApellidoP().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,45})$")) {
                            errors.add("apellidoP", new ActionMessage("ComplementariosUnoAction.apellidoPEnc.format"));
                        }
                        if (!GenericValidator.isBlankOrNull(getApellidoM()) && !getApellidoM().matches("^([a-zA-Z ÑÁÉÍÓÚñáéíóú]{2,45})$")) {
                            errors.add("apellidoM", new ActionMessage("ComplementariosUnoAction.apellidoMEnc.format"));
                        }
                        if (!getFechaNac().matches("^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$")) {
                            errors.add("fechaNac", new ActionMessage("ComplementariosUnoAction.fechaNac.format"));
                        }
                        if (!getSexo().matches("^[FM]{1}$")) {
                            errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.sexoEnc.format"));
                        }
                        if (!GenericValidator.isEmail(getEmail())) {
                            errors.add("sexoEnc", new ActionMessage("ComplementariosUnoAction.emailEnc.format"));
                        }
                    }
                    if (errors.size() != 0) {
                        request.setAttribute("llavePublica", request.getSession().getAttribute("publicKey"));
                        StringBuilder sb = new StringBuilder();
                        Iterator<ActionMessage> iter = errors.get();
                        PropertyMessageResources p = (PropertyMessageResources) request.getAttribute(Globals.MESSAGES_KEY);
                        while (iter.hasNext()) {
                            ActionMessage msg = iter.next();
                            sb.append(StrutsMensajes.getErrorMessage(p, msg));
                            sb.append("<br/>");
                        }
                        System.out.println(sb);
                        request.setAttribute("errorMensaje", AesCtr.encrypt(sb.toString(), keyResponse, 256));
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exeption en validacion DatosComplementariosUnoForm : ", e);
            if (request.getAttribute("llavePublica") != null) {
                request.removeAttribute("llavePublica");
            }
            request.setAttribute("errorMensaje", "Error inesperado en la validaci&oacute;n");
        }
        return errors;

    }

    /**
     * @return the log
     */
    public Logger getLog() {
        return log;
    }

    /**
     * @param log the log to set
     */
    public void setLog(Logger log) {
        this.log = log;
    }

    /**
     * @return the telefonoCasaEnc
     */
    public String getTelefonoCasaEnc() {
        return telefonoCasaEnc;
    }

    /**
     * @param telefonoCasaEnc the telefonoCasaEnc to set
     */
    public void setTelefonoCasaEnc(String telefonoCasaEnc) {
        this.telefonoCasaEnc = telefonoCasaEnc;
    }

    /**
     * @return the telefonoOficinaEnc
     */
    public String getTelefonoOficinaEnc() {
        return telefonoOficinaEnc;
    }

    /**
     * @param telefonoOficinaEnc the telefonoOficinaEnc to set
     */
    public void setTelefonoOficinaEnc(String telefonoOficinaEnc) {
        this.telefonoOficinaEnc = telefonoOficinaEnc;
    }

    /**
     * @return the telefonoCasa
     */
    public String getTelefonoCasa() {
        return telefonoCasa;
    }

    /**
     * @param telefonoCasa the telefonoCasa to set
     */
    public void setTelefonoCasa(String telefonoCasa) {
        this.telefonoCasa = telefonoCasa;
    }

    /**
     * @return the telefonoOficina
     */
    public String getTelefonoOficina() {
        return telefonoOficina;
    }

    /**
     * @param telefonoOficina the telefonoOficina to set
     */
    public void setTelefonoOficina(String telefonoOficina) {
        this.telefonoOficina = telefonoOficina;
    }

    /**
     * @return the nombreUsuarioEnc
     */
    public String getNombreUsuarioEnc() {
        return nombreUsuarioEnc;
    }

    /**
     * @param nombreUsuarioEnc the nombreUsuarioEnc to set
     */
    public void setNombreUsuarioEnc(String nombreUsuarioEnc) {
        this.nombreUsuarioEnc = nombreUsuarioEnc;
    }

    /**
     * @return the nombreUsuario
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    /**
     * @param nombreUsuario the nombreUsuario to set
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    /**
     * @return the emailEnc
     */
    public String getEmailEnc() {
        return emailEnc;
    }

    /**
     * @param emailEnc the emailEnc to set
     */
    public void setEmailEnc(String emailEnc) {
        this.emailEnc = emailEnc;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
